require_relative 'boot'

require 'rails/all'

require 'rake'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MediaChoice
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.available_locales = [:en, :"pt-BR"]
    config.i18n.default_locale = :"pt-BR"

    # Include settings folder
    config.autoload_paths += %W( #{config.root}/lib/settings #{config.root}/lib/custom_devise #{config.root}/lib/evaluation_system )

    config.active_job.queue_adapter = :delayed_job

    config.time_zone = 'Brasilia'
  end
end
