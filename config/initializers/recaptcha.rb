Recaptcha.configure do |config|
  config.site_key  = RecaptchaSettings.public_key
  config.secret_key = RecaptchaSettings.private_key
end
