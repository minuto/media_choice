Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  scope '(:locale)', locale: /en|pt-BR/ do
    devise_for :users, controllers: { confirmations: 'users/confirmations' }
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    resources :profiles, except: [:index]
    resources :contents, only: [:new, :create, :show]
    resources :contests, only: [:show, :index]

    scope format: true, constraints: { format: :json } do
      get '/contents', to: 'contents#index', as: :all_contents
      put '/contents/:id/evaluation', to: 'contents#evaluation', as: :content_evaluation
      get '/videos', to: 'contents#index'
      get '/videos/:id', to: 'contents#video', as: :get_video
      get '/contents/:id/related', to: 'contents#related_contents', as: :related_contents
    end

    scope :contents do
      post '/zencoder_callback', to: 'contents#zencoder_callback', as: :content_zencoder_callback
      get '/:id/map_view', to: 'contents#map_view', as: :map_view
      get '/:id/map_player', to: 'contents#map_player', as: :map_player
      get '/:id/share_button', to: 'contents#share_button', as: :share_button
      get '/:id/share_box', to: 'contents#share_box', as: :share_box
    end

    post '/public_rating', to: 'public_ratings#public_rating', as: :public_rating

    get 'states' => 'profiles#states', as: 'states'

    # static views' routes
    get 'terms_of_use' => 'static_views#terms_of_use', as: 'terms_of_use'
    get 'creative_commons' => 'static_views#creative_commons', as: 'creative_commons'
    get 'sponsorship' => 'static_views#sponsorship', as: 'sponsorship'
    get 'contact_us' => 'static_views#contact_us', as: 'contact_us'
    get 'soundtrack_copyright' => 'static_views#soundtrack_copyright', as: 'soundtrack_copyright'
    get 'faq' => 'static_views#faq', as: 'faq'
    get 'education' => 'static_views#education', as: 'education'
    get 'exhibit_on_your_institution' => 'static_views#exhibit_on_your_institution', as: 'exhibit_on_your_institution'
    get 'about_us' => 'static_views#about_us', as: 'about_us'
    get 'image_copyright' => 'static_views#image_copyright', as: 'image_copyright'
    get 'exhibition_network' => 'static_views#exhibition_network', as: 'exhibition_network'

    root to: 'home#index'
  end
end
