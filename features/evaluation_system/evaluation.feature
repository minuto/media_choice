Feature: Evaluation request
  In order to label a video as evaluated
  As the evaluation system
  I should be able to send a notification about the evaluated video

  @javascript
  Scenario: Evaluating a content
    Given I have a registered and confirmed user
    And the user has a complete profile
    And the user has a stored video
    And the video has been sent to evaluation
    When I send an evaluation request
    Then the stored video should have been evaluated
