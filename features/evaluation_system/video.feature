Feature: Video request
  In order to be able to retrieve video information
  As the evaluation system
  I should be able to ask for a specific video

  @javascript
  Scenario: Asking for an existent video
    Given I have a registered and confirmed user
    And the user has a complete profile
    And the user has a stored video with state "evaluating"
    When I send a request for the stored video
    Then I should receive a json with the video information
