Feature: Index request
  In order to be able to retrieve contents
  As the evaluation system
  I should be able to ask for the pending evaluation contents

  @javascript
  Scenario: Index with a list of ids
    Given I have a registered and confirmed user
    And the user has a complete profile
    And the user has stored contents
    When I send a request for the contents
    Then I should receive a json with the contents information

  @javascript
  Scenario: Index with user_id
    Given I have a registered and confirmed user
    And the user has a complete profile
    And the user has stored contents
    When I send a request for one of the user's contents
    Then I should receive a json with the content information
