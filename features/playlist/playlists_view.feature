Feature: Playlists view
  In order to see the created playlists
  As a regular, non registered user
  I should be able to see the playlists

  @javascript
  Scenario: Seeing the playlists on the Home Page
    Given there is a created playlist on the database
    And the created playlist has translations on both locales
    And the created playlist is not empty
    When I am at the Home Page on the locale "en"
    Then I should see the playlist's "en" name
    And I should see every video on the playlist
    When I am at the Home Page on the locale "pt-BR"
    Then I should see the playlist's "pt-BR" name
