Given(/^I have a MassMail$/) do
  create(:mass_mail)
end

When(/^I fill the required MassMail fields$/) do
  mass_mail_attrs = attributes_for(:mass_mail).with_indifferent_access

  step "I fill the \"mass_mail_subject\" field with \"#{mass_mail_attrs['subject']}\""
  step "I fill the \"mass_mail_body\" field with \"#{mass_mail_attrs['body']}\""
  step "I select \"#{mass_mail_attrs['locale']}\" from \"mass_mail_locale\""
end

When(/^I fill the required MassMail fields with other information$/) do
  mass_mail_attrs = attributes_for(:other_mass_mail).with_indifferent_access

  step "I fill the \"mass_mail_subject\" field with \"#{mass_mail_attrs['subject']}\""
  step "I fill the \"mass_mail_body\" field with \"#{mass_mail_attrs['body']}\""
  step "I select \"#{mass_mail_attrs['locale']}\" from \"mass_mail_locale\""
end

When(/^I click on the send mass mail link$/) do
  step "I click on the \"#{I18n.t('active_admin.send_mass_mail')}\" link"
end

Then(/^I should see the new mass_mail's details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: MassMail.model_name.human)}\""
  step "I should see \"#{@new_model.subject}\""
  step "I should see \"#{@new_model.status}\""
  step "I should see \"#{@new_model.locale}\""
end

Then(/^I should not see the status field$/) do
  step "I should not see \"#{MassMail.human_attribute_name(:status)}\""
end

Then(/^I should see the Mass Mail's index attributes$/) do
  step "I should see \"#{MassMail.human_attribute_name(:status)}\""
  step "I should see \"#{MassMail.human_attribute_name(:subject)}\""
  step "I should see \"#{MassMail.human_attribute_name(:locale)}\""
end

Then(/^I should see the edited mass_mail's details for the other information$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: MassMail.model_name.human)}\""
  step "I should see \"#{@edited_model.subject}\""
  step "I should see \"#{@edited_model.status}\""
  step "I should see \"#{@edited_model.locale}\""
end

Then(/^the edited MassMail should not be listed$/) do
  step "I should not see \"#{@new_model.subject}\""
end

Then(/^I should see the Mass Mail's filter options$/) do
  within 'div#filters_sidebar_section' do
    step "I should not see \"#{User.model_name.human}\""
    step "I should see \"#{MassMail.human_attribute_name(:subject)}\""
    step "I should see \"#{MassMail.human_attribute_name(:body)}\""
    step "I should see \"#{MassMail.human_attribute_name(:locale)}\""
    step "I should not see \"#{MassMail.human_attribute_name(:created_at)}\""
    step "I should not see \"#{MassMail.human_attribute_name(:updated_at)}\""
  end
end

Then(/^I should be only able to view the mass mail index actions$/) do
  step "I should see \"#{I18n.t('active_admin.view')}\""
  step "I should not see \"#{I18n.t('active_admin.edit')}\""
  step "I should not see \"#{I18n.t('active_admin.delete')}\""
  step "I should not see \"#{I18n.t('active_admin.send_mass_mail')}\""
end

Then(/^the show page should prohibit modifications$/) do
  step "I should not see \"#{I18n.t('active_admin.edit_model', model: MassMail.model_name.human)}\""
  step "I should not see \"#{I18n.t('active_admin.delete_model', model: MassMail.model_name.human)}\""
  step "I should not see \"#{I18n.t('active_admin.send_mass_mail')}\""
end
