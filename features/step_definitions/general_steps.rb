Given(/^I am at the home page$/) do
  visit root_path
end

Given(/^I should see "(.*?)"$/) do |content|
  expect(page).to have_content content
end

When(/^I take a picture of the page$/) do
  page.save_screenshot('/tmp/picture.png')
end

When(/^I click on the "(.*?)" link$/) do |link|
  click_link link
end

When(/^I fill the "(.*?)" field with "(.*?)"$/) do |field, input|
  fill_in field, with: input
end

When(/^I click on the "(.*?)" button$/) do |button|
  click_button button
end

When(/^I select "(.*?)" from "(.*?)"$/) do |option, select_identifier|
  select option, from: select_identifier
end

When(/^I check the "(.*?)" box$/) do |checkbox|
  check checkbox
end

When(/^I clear the cache$/) do
  Rails.cache.clear
end

When(/^I choose "(.*?)"$/) do |option|
  choose option
end

# rubocop:disable Lint/Debugger
Then(/^I stop to debug$/) do
  byebug
end
# rubocop:enable Lint/Debugger

Given(/^I should not see "(.*?)"$/) do |content|
  expect(page).to_not have_content content
end

Then(/^I should see a "(.*?)" selector with value "(.*?)"$/) do |selector, value|
  expect(page).to have_selector("#{selector}[value='#{value}']")
end

Then(/^I should not see a "(.*?)" selector with value "(.*?)"$/) do |selector, value|
  expect(page).to_not have_selector("#{selector}[value='#{value}']")
end

Then(/^I should see the option "(.*?)" of selector "(.*?)" selected$/) do |option, selector|
  expect(find("##{selector} option[value='#{option}']")).to be_selected
end

Then(/^I should see the checkbox (.*?) checked$/) do |selector|
  expect(find("##{selector}")).to be_checked
end
