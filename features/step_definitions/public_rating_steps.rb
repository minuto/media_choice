Given(/^the video has a grade of (\d+)$/) do |grade|
  user_who_voted_on_the_video = create :user
  create :public_rating, grade: grade, user_id: user_who_voted_on_the_video.id, content_id: @video.id
end

When(/^I grade the video as (\d+)$/) do |grade|
  execute_script("$('input#content-rating-val').val(#{grade})")
  step "I click on the \"#{I18n.t('contents.content_details.evaluate')}\" button"
end

When(/^I try to vote again$/) do
  step 'I grade the video as 4'
end

Then(/^I should see the video's grade as (.+)$/) do |grade|
  expect(find('.rateit-range')[:'aria-valuenow'].to_f).to eq grade.to_f
end

Then(/^I should see the successful vote message$/) do
  step "I should see \"#{I18n.t('successful_public_rating')}\""
end

Then(/^I should see the unsuccessful vote message$/) do
  step "I should see \"#{I18n.t('unsuccessful_public_rating')}\""
end
