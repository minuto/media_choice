Given(/^I have a complete profile$/) do
  @profile = create(:profile, user_id: @user.id)
end

Given(/^I am at the send content page$/) do
  visit new_content_path
end

Given(/^I see my name on the Author field$/) do
  expect(find('#content_author').value).to eq @profile.full_name
end

Given(/^I have a video to be uploaded$/) do
  @content_attributes = attributes_for :video
  @video_attributes = attributes_for :original_media, :with_file_name
end

When(/^I fill the title field with the video's title$/) do
  step "I fill the \"video-title\" field with \"#{@content_attributes[:title]}\""
end

When(/^I fill the video's soundtrack$/) do
  step "I fill the \"video-soundtrack\" field with \"#{@content_attributes[:soundtrack]}\""
end

When(/^I fill the director field with my name$/) do
  step "I fill the \"video-director\" field with \"#{@profile.full_name}\""
end

When(/^I mark the specific location checkbox$/) do
  step 'I check the "video-has-location" box'
end

When(/^I fill the zip code field with my zip code$/) do
  step "I fill the \"video-location-postal-code\" field with \"#{@content_attributes[:zip_code]}\""
end

When(/^I send a video file$/) do
  attach_file 'video-upload', @video_attributes[:media_file_name]
end

When(/^I click on the register button$/) do
  step "I click on the \"#{I18n.t 'register'}\" button"
end

When(/^I try to access the send content page$/) do
  visit new_content_path
end

Then(/^I should see the successful upload message$/) do
  step "I should see \"#{I18n.t 'successfully_created_video'}\""
end

Then(/^I should be redirected to the home page$/) do
  expect(current_path).to eq root_path(I18n.locale)
end

Then(/^I should see the you need to be logged in to send contents message$/) do
  step "I should see \"#{I18n.t 'needs_login'}\""
end

Then(/^I should see the content is not evaluated message$/) do
  step "I should see \"#{I18n.t 'content_not_yet_evaluated'}\""
end

Then(/^the label of the file field should be the same as the file name$/) do
  expect(find_by_id('file-label').text).to eq @video_attributes[:media_file_name].split('/').last
end

Then(/^the last encoded content state should be "([^"]*)"$/) do |state|
  expect(EncodedContent.last.state).to eq(state)
end

Then(/^the sent content should have a correctly assigned path$/) do
  video = Content.last.original_medias.last
  path = video.media.url
  expect(path).to match(%r{videos/#{video.id}/#{video.media_file_name}})
end
