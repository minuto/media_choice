require 'English'

Given(/^the user has a complete profile$/) do
  @profile = create(:profile, user_id: @user.id)
end

When(/^the user creates a video content$/) do
  @content = create(:content_with_original_medias, user_id: @user.id)
  @original_content_url = @content.original_medias.last.media.url
end

When(/^the zencoder job finishes$/) do
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
end

Then(/^the video should have an url$/) do
  expect(@content.original_medias.last.media.url).not_to be_nil
end

Then(/^a zencoder job should have been created$/) do
  expect(Delayed::Job.any? { |job| job.queue == 'zencoder' }).to eq true
end

Then(/^an encoded content should have been created$/) do
  expect(EncodedContent.where(content_id: @content.id).size).to eq 1
end

Then(/^I should receive a successful notification$/) do
  `zencoder_fetcher -u #{ZencoderSettings.notifications.url} #{ZencoderSettings.api_key} -n 1 -x`
  expect($CHILD_STATUS.success?).to eq true
end

Then(/^the encoded content should have been updated$/) do
  expect(EncodedContent.find_by(content_id: @content.id).output_id).to_not be_nil
end

Then(/^the original media file is still accessible$/) do
  encoded_content = @content.encoded_contents.first
  expect(@content.original_medias.last.media.url).to eq @original_content_url
  expect(@original_content_url).to_not eq(encoded_content.video.url)
  expect(@content.original_medias.last.media.hash).to_not eq(encoded_content.video.hash)
end
