Given(/^I fill the required Sponsor fields$/) do
  @sponsor_attrs = attributes_for(:sponsor, :with_logo_filename).with_indifferent_access
  step "I fill the \"sponsor_address\" field with \"#{@sponsor_attrs['address']}\""
  step "I fill the \"sponsor_name\" field with \"#{@sponsor_attrs['name']}\""
  attach_file 'sponsor_logo', @sponsor_attrs['logo_file_name']
end

Then(/^I should see the new sponsor's details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: Sponsor.model_name.human)}\""
  step "I should see \"#{@sponsor_attrs['address']}\""
end

When(/^I change some Sponsor fields$/) do
  @sponsor_attrs['address'] = 'www.anotherexample.com'
  step "I fill the \"sponsor_address\" field with \"#{@sponsor_attrs['address']}\""
end

Then(/^I should see the sponsor's new details$/) do
  step "I should see the new sponsor's details"
end

Then(/^the new sponsor should be listed$/) do
  expect(page).to have_content @sponsor_attrs['address']
end
