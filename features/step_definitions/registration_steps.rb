Given(/^I have a registered and confirmed user$/) do
  @user_information = attributes_for(:user).with_indifferent_access
  @user = User.create(@user_information)
  @user.confirm
end

Given(/^I click on the sign up link$/) do
  step "I click on the \"#{I18n.t 'account.signup'}\" link"
end

Given(/^I click on the sign up button$/) do
  step "I click on the \"#{I18n.t 'register'}\" button"
end

Given(/^I confirm the user's registration$/) do
  email_body = ActionMailer::Base.deliveries.last.body.raw_source
  confirmation_link = Nokogiri::HTML(email_body).at_css('a').attribute('href').value
  visit(confirmation_link)
end

Given(/^I fill the password confirmation field with the user's password$/) do
  step "I fill the \"user-password-confirmation\" field with \"#{@user_information[:password]}\""
end

Given(/^I have the information for a new registration$/) do
  @user_information = { email: 'email@email.com', password: '12345678' }.with_indifferent_access
end

Given(/^There is a registered user$/) do
  @registered_user = create :user
end

Given(/^I have the information for a duplicate registration$/) do
  @user_information = { email: @registered_user.email, password: '12345678' }.with_indifferent_access
end

When(/^I fill the email field of the registration form$/) do
  fill_in 'user-email', with: @user_information[:email]
end

When(/^I confirm the email$/) do
  fill_in 'user-email-confirmation', with: @user_information[:email]
end

When(/^I fill the password field of the registration form$/) do
  fill_in 'user-password', with: @user_information[:password]
end

Then(/^the user should be confirmed$/) do
  user = User.find_by_email(@user_information[:email])
  expect(user.confirmed?).to be true
  expect(user.confirmed_at).to_not be_nil
end

Then(/^I should see the email is already taken message$/) do
  step "I should see \"#{I18n.t('errors.messages.taken')}\""
end

Then(/^the user should be on the home page$/) do
  expect(current_path).to eq(root_path(locale: I18n.locale))
end
