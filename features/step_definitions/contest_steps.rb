Given(/^there is an ongoing contest$/) do
  @contest = create(:contest)
  @translation = create(:contest_translation, :with_file, locale: I18n.locale, contest: @contest)
end

Given(/^I( should)? see the contest's name$/) do |_|
  step "I should see \"#{@contest.name}\""
end

Given(/^there is another ongoing contest$/) do
  @another_contest = create(:contest)
  @another_translation = create(:contest_translation, :with_file, locale: I18n.locale, contest: @another_contest)
end

Given(/^the contest has associated contents$/) do
  @participating_user = create(:user, profile: create(:profile))
  @contents = create_list(:content_with_original_medias, 2, user: @participating_user, contests: [@contest])
  Delayed::Job.where(queue: 'zencoder').first.invoke_job
  step 'I should receive a successful notification'
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
  step 'I should receive a successful notification'
end

Given(/^the associated contents have been evaluated$/) do
  @contents.map(&:send_to_evaluation)
  @contents.map(&:label_evaluated)
end

Given(/^some of the associated contents have been evaluated$/) do
  @contents.first.send_to_evaluation
  @contents.first.label_evaluated
end

Given(/^the contest has associated sponsors$/) do
  @sponsors = Array.new(2, create(:sponsor, :with_logo, contests: [@contest]))
end

When(/^I mark the contest's checkbox$/) do
  step "I check the \"contest_#{@contest.id}\" box"
end

When(/^I visit the contest page$/) do
  visit(contest_path(id: @contest))
end

When(/^I mark all the contests' checkboxes$/) do
  find_all('input[id^=checkbox]').map(&:check)
end

When(/^the other contest gets deleted$/) do
  @another_contest.delete
end

Then(/^I should see the contest's description$/) do
  step "I should see \"#{@contest.description}\""
end

Then(/^I should see the share link$/) do
  step "I should see \"#{I18n.t('contests.show.share')}\""
end

Then(/^The selected contest should have been associated with the content$/) do
  expect(@contest.contents).to include(Content.last)
end

Then(/^I should see the contest not found error message$/) do
  step "I should see \"#{I18n.t('contest_not_found')}\""
end

Then(/^I should see the contest's logo$/) do
  expect(has_css?("img[src=\"#{@contest.contest_translations.first.logo.url}\"]")).to be true
end

Then(/^I should see the associated contents$/) do
  @contents.each do |content|
    expect(has_css?("img[src=\"#{content.thumbnails.last.url}\"]")).to be true
  end
end

Then(/^I should see only the evaluated contents$/) do
  @contents.each do |content|
    expect(has_css?("img[src=\"#{content.thumbnails.last.url}\"]")).to eq content.evaluated?
  end
end

Then(/^I should see the associated sponsors$/) do
  @sponsors.each do |sponsor|
    expect(has_css?("img[src=\"#{sponsor.logo.url(:thumb)}\"]")).to be true
  end
end
