When(/^I click to view a content$/) do
  view_translation = I18n.t('active_admin.view')
  element = page.first(:xpath, "//a[text()='#{view_translation}']")
  # Seek the id of the chosen content so we can assert we are seeing the correct one later
  # The element[:href] will be somenthing like "http://127.0.0.1:3000/admin/contents/1?locale=pt-BR"
  element_id = element[:href].split('?').first.split('/').last.to_i
  @chosen_content = Content.find(element_id)
  element.click
end

When(/^I click on the deleted scope$/) do
  step "I click on the \"#{I18n.t('deleted')}\" link"
end

Then(/^I should see a list with the contents$/) do
  Content.all.each do |content|
    expect(page).to have_content content.title
    expect(page).to have_content content.director
    expect(page).to have_content content.view_count
    expect(page).to have_content content.soundtrack
    expect(page).to have_content content.evaluation_state
    expect(page).to have_content content.judge_rating
    expect(page).to have_content content.user.email
  end
end

Then(/^I should see the content's details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: Content.model_name.human)}\""
  step "I should see \"#{@chosen_content.title}\""
  step "I should see \"#{@chosen_content.director}\""
  step "I should see \"#{@chosen_content.view_count}\""
  step "I should see \"#{@chosen_content.evaluation_state}\""
  step "I should see \"#{@chosen_content.zip_code}\""
  step "I should see \"#{@chosen_content.judge_rating}\""
  step "I should see \"#{@chosen_content.soundtrack}\""
  step "I should see \"#{@chosen_content.co_director}\""
  step "I should see \"#{@chosen_content.team}\""
  step "I should see \"#{@chosen_content.user.email}\""
end

Then(/^I should see the content's encoded contents details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: EncodedContent.model_name.human)}\""
  @chosen_content.encoded_contents.each do |encoded_content|
    expect(page.first(:xpath, "//video[@src='#{encoded_content.video.url}']")).to_not be_nil
  end
end

Then(/^I should be at the content management page$/) do
  expect(current_path).to eq(admin_contents_path)
end

Then(/^the content should be successfully deleted$/) do
  step "I should see \"#{I18n.t('flash.actions.destroy.notice', resource_name: Content.model_name.human)}\""
end

Then(/^I should see the deleted content$/) do
  step "I should see \"#{@chosen_content.title}\""
  step "I should see \"#{@chosen_content.user.email}\""
  step "I should see \"#{@chosen_content.evaluation_state}\""
end
