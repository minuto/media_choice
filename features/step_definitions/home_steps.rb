def create_content_list
  [
    create(:content_with_original_medias, user: @participating_user, contests: [@contest, @another_contest], created_at: Time.zone.tomorrow),
    create(:content_with_original_medias, user: @participating_user, contests: [@contest, @another_contest], created_at: Time.zone.today),
    create(:content_with_original_medias, user: @participating_user, contests: [@contest, @another_contest], created_at: Time.zone.yesterday),
    create(:content_with_original_medias, user: @participating_user, contests: [@contest, @another_contest], created_at: Time.zone.today)
  ]
end

Given(/^there are contests on the database$/) do
  step 'there is an ongoing contest'
  step 'there is another ongoing contest'
  @contests = [@contest, @another_contest]
end

Given(/^there are contents on the database$/) do
  @participating_user = create(:user, profile: create(:profile))
  @contents = create_content_list
  Delayed::Job.where(queue: 'zencoder').each do |job|
    job.invoke_job
    step 'I should receive a successful notification'
  end
end

Given(/^some of the contents have been evaluated$/) do
  @evaluated_contents = []
  @contents.each_with_index do |content, index|
    next if index == 1
    content.send_to_evaluation
    content.label_evaluated
    @evaluated_contents << content
  end
end

Given(/^there are home highlights on the database$/) do
  @home_highlights = Array.new(2, create(:home_highlight, :with_file))
end

Given(/^some of the evaluated contents have been given featured judge ratings$/) do
  @evaluated_contents.each_with_index do |content, index|
    if index == 1
      content.judge_evaluation.update(judge_rating: 0, evaluation_state: 'evaluated') # not featured content
    else
      content.judge_evaluation.update(judge_rating: 4, evaluation_state: 'evaluated') # featured content
    end
  end
end

When(/^a new started contest is added$/) do
  @new_started_contest = create(:contest)
  @translation = create(:contest_translation, :with_file, locale: I18n.locale, contest: @new_started_contest)
end

When(/^I click on the Recent tab$/) do
  step "I click on the \"#{I18n.t('home.index.recent')}\" link"
end

Then(/^I should see the contests$/) do
  @contests.each do |contest|
    expect(has_css?("img[src=\"#{contest.logo_url}\"]")).to be true
  end
end

Then(/^I should see the home highlights$/) do
  @home_highlights.each do |highlight|
    expect(has_css?("img[src=\"#{highlight.banner.url}\"]")).to be true
    expect(has_css?("a[href=\"#{highlight.link}\"]")).to be true
  end
end

Then(/^I should see the new contest$/) do
  expect(has_css?("img[src=\"#{@new_started_contest.logo_url}\"]")).to be true
end

When(/^I visit the home page with the locale (.+)?$/) do |locale|
  I18n.locale = locale.to_sym
  visit(root_path(locale: locale))
end

Then(/^I should see the evaluated contents ordered by creation date$/) do
  sleep(15)
  content_ids = page.all(:xpath, '//*[@class="thumb content-id"]').map { |div| div[:'data-id'].to_i }
  expect(Content.finished.evaluated.order(created_at: :desc).pluck(:id)).to eq content_ids
  step 'I should see featured icons on the recent featured contents'
end

Then(/^I should see featured icons on the recent featured contents$/) do
  @evaluated_contents.each do |content|
    found_icon = true
    begin
      # raises exception if element is not found
      find("#recent-featured-#{content.id}", visible: :all)[:class].include?('has-tip top ic-destaque')
    rescue
      found_icon = false
    end
    expect(found_icon).to eq content.featured?
  end
end

Then(/^I should see only the featured contents$/) do
  @evaluated_contents.each do |content|
    expect(has_css?("img[src=\"#{content.thumbnails.last.url}\"]")).to eq content.featured?
  end
end
