def expect_response_to_include_json_content(content)
  content_json = JSON.parse(EvaluationSystem.content_json_for_evaluation(content).to_json)
  expect(@json_parsed_response).to include(content_json)
end

def build_request_uri(path, params = {})
  uri = URI("http:\/\/#{Capybara.current_session.server.host}:#{Capybara.current_session.server.port}\/#{path}")
  uri.query = URI.encode_www_form(params)
  uri
end

Given(/^the user has a stored video$/) do
  @stored_video = create :content_with_original_medias, user: @user
  Delayed::Job.where(queue: 'zencoder').first.invoke_job
  step 'I should receive a successful notification'
end

Given(/^the user has stored contents$/) do
  @stored_contents = create_list(:content_with_original_medias, 2, user: @user)
  @stored_contents.each { |c| c.judge_evaluation.update(evaluation_state: 'evaluating') }
  Delayed::Job.where(queue: 'zencoder').first.invoke_job
  step 'I should receive a successful notification'
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
  step 'I should receive a successful notification'
end

Given(/^the video has been sent to evaluation$/) do
  @stored_video.send_to_evaluation
end

Given(/^the user has a stored video with state "(.*?)"$/) do |state|
  step 'the user has a stored video'
  @stored_video.judge_evaluation.update!(evaluation_state: state)
end

When(/^I send an evaluation request$/) do
  uri = build_request_uri("contents/#{@stored_video.id}/evaluation.json")
  request = Net::HTTP::Put.new(uri, 'Content-Type' => 'application/json')
  request.body = { content: { evaluation_state: 'evaluated' } }.to_json
  Net::HTTP.start(uri.host, uri.port) do |http|
    @response = http.request request
  end
end

When(/^I send a request for the contents$/) do
  @response = Net::HTTP.get_response(build_request_uri('contents.json', ids: Content.all.map(&:id).join(',')))
  @json_parsed_response = JSON.parse(@response.body)
end

When(/^I send a request for one of the user's contents$/) do
  @response = Net::HTTP.get_response(build_request_uri('contents.json', user_id: @user.id, orig_id: Content.first.id))
  @json_parsed_response = JSON.parse(@response.body)
end

When(/^I send a request for the stored video$/) do
  @response = Net::HTTP.get_response(build_request_uri("videos/#{@stored_video.id}.json"))
  @json_parsed_response = JSON.parse(@response.body)
end

Then(/^the stored video should have been evaluated$/) do
  @stored_video.reload
  expect(@stored_video.evaluation_state).to eq 'evaluated'
end

Then(/^I should receive a json with the contents information$/) do
  @stored_contents.each do |content|
    expect_response_to_include_json_content content
  end
end

Then(/^I should receive a json with the content information$/) do
  expect_response_to_include_json_content Content.first
end

Then(/^I should receive a json with the video information$/) do
  expect_response_to_include_json_content @stored_video
end
