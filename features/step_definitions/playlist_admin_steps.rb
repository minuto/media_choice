Given(/^the user has multiple stored videos$/) do
  @video1 = create(:content_with_original_medias, user: @user, title: 'Video 1')
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
  step 'I should receive a successful notification'
  @video1.send_to_evaluation
  @video1.label_evaluated
  @video2 = create(:content_with_original_medias, user: @user, title: 'Video 2')
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
  step 'I should receive a successful notification'
  @video2.send_to_evaluation
  @video2.label_evaluated
  @video3 = create(:content_with_original_medias, user: @user, title: 'Video 3')
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
  step 'I should receive a successful notification'
  @video3.send_to_evaluation
  @video3.label_evaluated
end

Given(/^the created playlist has a video in it$/) do
  Playlist.last.contents << Content.last
end

Then(/^the playlist should have been created$/) do
  playlist = Playlist.last
  expect(playlist.name(@locale)).to eq @name
  @videos.each do |video|
    expect(playlist.contents).to include video
  end
end

When(/^I add all the videos to the playlist$/) do
  Content.all.pluck(:title).each do |title|
    step "I select \"#{title}\" from #{Content.model_name.human.pluralize}"
  end
end

When(/^I remove one of the videos from the playlist$/) do
  @playlist.reload
  unselect @playlist.contents.first.title, from: 'playlist_content_ids'
end

When(/^I check the visible checkbox$/) do
  step "I check the \"#{I18n.t 'activerecord.attributes.playlist.visible'}\" box"
end

When(/^I click on the add playlist translation button$/) do
  step "I click on the \"#{I18n.t 'active_admin.has_many_new', model: PlaylistTranslation.model_name.human}\" link"
end

When(/^I select the first two contents$/) do
  @videos = Content.first(2)
  @videos.map(&:title).each do |title|
    step "I select \"#{title}\" from \"playlist_content_ids\""
  end
end

When(/^I fill the name field with "([^"]*)"$/) do |name|
  @name = name
  step "I fill the \"#{I18n.t('activerecord.attributes.playlist_translation.name')}\" field with \"#{name}\""
end

When(/^I select the "([^"]*)" locale$/) do |locale|
  @locale = locale
  step "I select \"#{locale}\" from \"#{I18n.t 'activerecord.attributes.playlist_translation.locale'}\""
end

When(/^I click on the edit playlist link$/) do
  click_link I18n.t 'active_admin.edit'
end

Then(/^the playlist should have been updated$/) do
  playlist = Playlist.last
  expect(playlist.name(@locale)).to eq @name
end

Then(/^the playlist should be empty$/) do
  playlist = Playlist.last
  expect(playlist.contents.size).to eq 0
end

Given(/^the created playlist has a translation$/) do
  Playlist.last.playlist_translations.destroy_all
  Playlist.last.playlist_translations << create(:playlist_translation)
end
