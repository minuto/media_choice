Given(/^I click on the Sign in link$/) do
  find('a#login_link').click
end

Given(/^I click on the Sign in button$/) do
  step "I click on the \"#{I18n.t 'enter'}\" button"
end

When(/^I click on the Sign out link$/) do
  step "I click on the \"#{I18n.t 'account.signout'}\" link"
end

When(/^I click on the account drop down link$/) do
  step "I click on the \"#{I18n.t 'account.my'}\" link"
end

Then(/^I should see the successful login message$/) do
  step "I should see \"#{I18n.t 'devise.sessions.signed_in', kind: I18n.t('Media Choice')}\""
end

Then(/^I should see the successful logout message$/) do
  step "I should see \"#{I18n.t 'devise.sessions.signed_out'}\""
end

Given(/^I fill the email field with the user's email$/) do
  step "I fill the \"user_email\" field with \"#{@user_information['email']}\""
end

Given(/^I fill the password field with the user's password$/) do
  step "I fill the \"user_password\" field with \"#{@user_information['password']}\""
end

Given(/^I am logged in$/) do
  step 'I am at the home page'
  step 'I click on the Sign in link'
  step "I fill the email field with the user's email"
  step "I fill the password field with the user's password"
  step 'I click on the Sign in button'
end
