When(/^I fill the required User fields$/) do
  @new_user_information = attributes_for(:user).with_indifferent_access
  step "I fill the \"user_email\" field with \"#{@new_user_information['email']}\""
  step "I fill the \"user_password\" field with \"#{@new_user_information['password']}\""
  step "I fill the \"user_password_confirmation\" field with \"#{@new_user_information['password']}\""
end

Then(/^I should see all the stored users$/) do
  User.all.each do |user|
    step "I should see \"#{user.email}\""
  end
end

Then(/^I should see the new user's details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: User.model_name.human)}\""
  step "I should see \"#{User.last.email}\""
end

Then(/^the new user should not be listed$/) do
  expect(page).to_not have_content @new_model.email
end
