Given(/^there is a video author user$/) do
  @video_author = create(:user)
  create(:profile, user_id: @video_author.id)
end

Given(/^there is a video author user without some location information$/) do
  @video_author = create(:user)
  profile = create(:profile, user_id: @video_author.id)
  profile.update_columns(city: nil, state: nil)
end

Given(/^the video author has a stored video$/) do
  @video = create(:content_with_original_medias, user_id: @video_author.id)
  Delayed::Job.where(queue: 'zencoder').last.invoke_job
end

Given(/^the video author has a stored but not encoded video$/) do
  @video = create(:content_with_original_medias, user_id: @video_author.id)
  create(:encoded_video, state: nil, content_id: @video.id)
end

Given(/^the video author has a stored and encoded video$/) do
  step 'the video author has a stored video'
  step 'I should receive a successful notification'
end

Given(/^the video author has a stored, encoded and evaluated video$/) do
  step 'the video author has a stored and encoded video'
  step 'the stored video has been evaluated'
end

Given(/^the stored video has been evaluated$/) do
  @video.send_to_evaluation
  @video.label_evaluated
end

Given(/^the video is on a contest$/) do
  @contest = create(:contest, contents: [@video])
  @contest_translation = create(:contest_translation, :with_file_name, contest: @contest, locale: I18n.locale)
end

When(/^I visit the video's page$/) do
  visit content_path(id: @video.id)
end

When(/^I click on the "([^"]*)" player link$/) do |id|
  find("a[id^='#{id}']").click
end

When(/^I close the share box$/) do
  find('button.close-button').click
end

When(/^the video has a featured judge rating$/) do
  @video.judge_evaluation.update(judge_rating: 5)
end

When(/^the video does not have a fetured judge rating$/) do
  @video.judge_evaluation.update(judge_rating: 0)
end

Then(/^I should see the video player$/) do
  # By using div on the selector, it is forcing the page to have the videojs loaded
  expect(has_css?('div.video-js')).to eq true
end

Then(/^I should be able to see the video's author$/) do
  step "I should see \"#{@video_author.profile.full_name}\""
  step "I should see \"#{@video_author.profile.city}\""
  step "I should see \"#{@video_author.profile.state}\""
  step "I should see \"#{Carmen::Country.numeric_coded(@video_author.profile.country_numeric_code).name}\""
end

Then(/^I should be able to see the video's author with placeholders replacing the missing information$/) do
  step "I should see \"#{@video_author.profile.full_name}\""
  step "I should see \"#{Carmen::Country.numeric_coded(@video_author.profile.country_numeric_code).name}\""
  step "I should see \"#{I18n.t('city')} #{I18n.t('not_informed')}\""
  step "I should see \"#{I18n.t('state')} #{I18n.t('not_informed')}\""
end

Then(/^I should see the content is not ready message$/) do
  step "I should see \"#{I18n.t('video_has_not_finished_encoding')}\""
end

Then(/^I should be able to see the "([^"]*)" player button$/) do |id|
  expect(has_css?("a[id^='#{id}']")).to eq true
end

Then(/^I should see "([^"]*)" icon$/) do |icon_class|
  expect(page).to have_selector("i.#{icon_class}", visible: true)
end

Then(/^I should see the contest name$/) do
  step "I should see \"#{@contest_translation.name}\""
end

Then(/^I should be at the map page$/) do
  expect(current_path).to eq(map_view_path(locale: I18n.locale, id: @video.id))
end

Then(/^I should see the featured icon$/) do
  expect(has_css?("span[class^='has-tip top ic-destaque']")).to eq true
end

Then(/^I should not see the featured icon$/) do
  expect(has_css?("span[class^='has-tip top ic-destaque']")).to eq false
end
