# rubocop:disable Lint/Eval
# Using eval is safe within this context as it is not part of production code
Given(/^I am at the "([^"]*)" management page$/) do |model|
  visit eval("admin_#{model}s_path")
end

Given(/^I have a logged in admin user$/) do
  step 'I have a registered and confirmed user'
  step 'the user has a complete profile'
  step 'I become an admin user'
  step 'I am at the home page'
  step 'I click on the Sign in link'
  step "I fill the email field with the user's email"
  step "I fill the password field with the user's password"
  step 'I click on the Sign in button'
end

Given(/^I am at the admin dashboard$/) do
  step "I click on the \"#{I18n.t('control_panel')}\" link"
end

When(/^I click on the New "([^"]*)" admin link$/) do |model|
  step "I click on the \"#{I18n.t('active_admin.new_model', model: eval(model).model_name.human)}\" link"
end

When(/^I click on the Create "([^"]*)" admin button$/) do |model|
  step "I click on the \"#{I18n.t('helpers.submit.create', model: eval(model).model_name.human)}\" button"
  @new_model = eval(model).order(:created_at).last
end

When(/^I click on the Delete "([^"]*)" admin link$/) do |model|
  step "I click on the \"#{I18n.t('active_admin.delete_model', model: eval(model).model_name.human)}\" link"
end

When(/^I click on the "([^"]*)" admin menu link$/) do |model|
  step "I click on the \"#{eval(model).model_name.human}\" link"
end

Then(/^I should not see the control panel link$/) do
  expect(page).to_not have_content I18n.t('control_panel')
end

When(/^I try to access the admin control panel$/) do
  visit admin_root_path
end

When(/^I click on the Edit "([^"]*)" admin link$/) do |model|
  step "I click on the \"#{I18n.t('active_admin.edit_model', model: eval(model).model_name.human)}\" link"
end

When(/^I click on the Save "([^"]*)" admin button$/) do |model|
  step "I click on the \"#{I18n.t('helpers.submit.update', model: eval(model).model_name.human)}\" button"
  @edited_model = eval(model).order(:updated_at).last
end

When(/^I click on the Update "([^"]*)" admin button$/) do |model|
  step "I click on the \"#{I18n.t('helpers.submit.update', model: eval(model).model_name.human)}\" button"
end

Then(/^I should see the you need to sign in message$/) do
  step "I should see \"#{I18n.t(:unauthenticated, scope: [:devise, :failure])}\""
end

When(/^I become an admin user$/) do
  @user.update(admin: true)
end

Then(/^I should see the control panel link$/) do
  step "I should see \"#{I18n.t('control_panel')}\""
end

Then(/^I should be at the admin dashboard$/) do
  expect(current_path).to eq(admin_root_path)
end

Then(/^the new "([^"]*)" should be successfully created on admin$/) do |model|
  step "I should see \"#{I18n.t('flash.actions.create.notice', resource_name: eval(model).model_name.human)}\""
end

Then(/^the new "([^"]*)" should be successfully updated on admin$/) do |model|
  step "I should see \"#{I18n.t('flash.actions.update.notice', resource_name: eval(model).model_name.human)}\""
end

Then(/^the new "([^"]*)" should be successfully destroyed on admin$/) do |model|
  step "I should see \"#{I18n.t('flash.actions.destroy.notice', resource_name: eval(model).model_name.human)}\""
end

Then(/^I should be at the "([^"]*)" admin index$/) do |model|
  expect(current_path).to eq(eval("admin_#{model}_path"))
end
# rubocop:enable Lint/Eval
