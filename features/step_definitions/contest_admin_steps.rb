When(/^I fill the required Contest fields$/) do
  contest_attrs = attributes_for(:contest).with_indifferent_access
  step "I fill the \"contest_start_date\" field with \"#{contest_attrs['start_date']}\""
end

When(/^I fill the required Contest Translation fields$/) do
  contest_translation_attrs = attributes_for(:contest_translation, :with_file_name).with_indifferent_access
  step "I fill the \"contest_contest_translations_attributes_0_name\" field with \"#{contest_translation_attrs['name']}\""
  attach_file 'contest_contest_translations_attributes_0_logo', contest_translation_attrs['logo_file_name']
end

Then(/^I should see the new contest's details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: Contest.model_name.human)}\""
  step "I should see \"#{I18n.l(Contest.last.start_date.to_date, format: :long)}\""
end

Then(/^I should see the new contest translation's details$/) do
  step "I should see \"#{I18n.t('active_admin.details', model: ContestTranslation.model_name.human)}\""
  step "I should see \"#{ContestTranslation.last.name}\""
end

Then(/^the new contest should be listed$/) do
  step "I should see \"#{I18n.l(@new_model.start_date.to_date, format: :long)}\""
end
