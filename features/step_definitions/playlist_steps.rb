Given(/^there is a created playlist on the database$/) do
  @playlist = create :playlist
end

Given(/^the created playlist is not empty$/) do
  @user = create(:user)
  @user.confirm
  @profile = create(:profile, user: @user)
  step 'the user has multiple stored videos'
  @playlist.contents << [@video1, @video2]
  @playlist.save
end

Given(/^the created playlist has translations on both locales$/) do
  @playlist.playlist_translations = [create(:playlist_translation, name: 'Name on locale en', locale: 'en'), create(:playlist_translation, name: 'Name on locale pt-BR', locale: 'pt-BR')]
  @playlist.save
end

When(/^I am at the Home Page on the locale "([^"]*)"$/) do |locale|
  visit(root_path(locale: locale))
end

Then(/^I should see the playlist's "([^"]*)" name$/) do |locale|
  step "I should see \"#{@playlist.name(locale)}\""
end

Then(/^I should see every video on the playlist$/) do
  @playlist.contents.each do |video|
    expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to be true
  end
end

Then(/^I should not see the playlist$/) do
  step "I should not see \"#{@playlist.name}\""
end
