def create_contents_for(user)
  @not_finished_encoding_video = create(:content_with_original_medias, user: user)
  @not_evaluated_video = create(:content_with_original_medias, user: user)
  @evaluated_video = create(:content_with_original_medias, user: user)
  @evaluated_video.judge_evaluation.update(judge_rating: 0)
  @featured_video = create(:content_with_original_medias, user: user)
  @featured_video.judge_evaluation.update(judge_rating: JudgeEvaluation::FEATURED_THRESHOLD)
  @videos = [@not_finished_encoding_video, @not_evaluated_video, @evaluated_video, @featured_video]
  @videos_to_be_shown = [@not_evaluated_video, @evaluated_video, @featured_video]
  @videos_to_be_encoded = [@not_evaluated_video, @evaluated_video, @featured_video]
  @featured_videos_to_be_shown = [@featured_video]
  @not_finished_encoding_videos = [@not_finished_encoding_video]
  @not_evaluated_videos = @videos_to_be_shown.select { |video| video.evaluation_state != 'evaluated' }
end

Given(/^I click the my account link$/) do
  step "I click on the \"#{I18n.t 'account.my'}\" link"
end

Given(/^I click the new profile link$/) do
  step "I click on the \"#{I18n.t 'profile_links.new'}\" link"
end

When(/^I click on the button to send the profile information$/) do
  step "I click on the \"#{I18n.t 'save'}\" button"
end

Then(/^I should see the successfully created profile message$/) do
  step "I should see \"#{I18n.t 'successfully_created_profile'}\""
end

Given(/^I select any occupation$/) do
  # We have to drop the first element because it is the placeholder element of the dropdown
  option = all('#profile_occupation option').drop(1).sample.text
  step "I select \"#{option}\" from \"profile_occupation\""
end

Given(/^I try to see a nonexistent profile$/) do
  visit(profile_path(id: 1))
end

Given(/^I am at the user's profile page$/) do
  visit profile_path(id: @profile.id)
end

Given(/^I have sent videos$/) do
  create_contents_for(@user)
end

Given(/^the other user has sent videos$/) do
  create_contents_for(@another_user)
  @videos_to_be_shown = [@evaluated_video, @featured_video]
  @not_evaluated_videos = [@not_evaluated_video]
end

Given(/^some videos have finished encodings$/) do
  waiting_encode = Delayed::Job.where(queue: 'zencoder')

  waiting_encode[1..@videos_to_be_encoded.size].each do |job|
    job.invoke_job
    step 'I should receive a successful notification'
  end
end

Given(/^some videos have been evaluated$/) do
  @evaluated_video.send_to_evaluation
  @evaluated_video.label_evaluated
  @evaluated_video.judge_evaluation.update!(judge_rating: 3)

  @featured_video.send_to_evaluation
  @featured_video.label_evaluated
  @featured_video.judge_evaluation.update!(judge_rating: 4)
end

When(/^I click on the my page link$/) do
  click_link I18n.t 'account.my'
  click_link I18n.t 'account.my_page'
end

When(/^I mouseover my account menu item$/) do
  step "I should see \"#{I18n.t('account.my')}\""
  find('a#my-account').hover
end

When(/^I click on the edit link$/) do
  step "I click on the \"#{I18n.t('profiles.show.edit_profile')}\" link"
end

Then(/^I should see the error message for occupation can't be blank$/) do
  step "I should see \"#{I18n.t 'errors.format', attribute: Profile.human_attribute_name('occupation'), message: I18n.t('errors.messages.blank')}\""
end

Then(/^I should see the not found page$/) do
  expect(status_code).to eq 404
end

Then(/^I should be at my page$/) do
  expect(current_path).to eq(profile_path(locale: I18n.locale, id: @user.profile.id))
end

Then(/^I should see my full name$/) do
  step "I should see \"#{@user.profile.full_name}\""
end

Then(/^I should see the edit link$/) do
  step "I should see \"#{I18n.t 'profiles.show.edit_profile'}\""
end

Then(/^I should see the amount of videos I sent$/) do
  @videos_to_be_shown.each(&:reload)
  step "I should see \"#{@videos_to_be_shown.size} #{I18n.t 'profiles.show.videos'}\""
end

Then(/^I should see the videos I sent that finished encoding$/) do
  @videos_to_be_shown.each do |video|
    expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to be true
  end
end

Then(/^I should see the amount of featured videos I sent$/) do
  step "I should see \"#{@featured_videos_to_be_shown.size} #{I18n.t 'profiles.show.featured_contents'}\""
end

Then(/^I should see the featured videos I sent that finished encoding$/) do
  within 'div#list-featured' do
    @featured_videos_to_be_shown.each do |video|
      expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to be true
    end
  end
end

Then(/^I should see the profile's navbar management links$/) do
  step "I should see \"#{I18n.t('account.my_page')}\""
  step "I should see \"#{I18n.t('profile_links.edit')}\""
end

Then(/^I should be at the profile's edit page$/) do
  expect(current_path).to eq(edit_profile_path(locale: I18n.locale, id: @profile.id))
  step "I should see \"#{I18n.t('edit_your_registration')}\""
end

Then(/^I should see my information correctly loaded$/) do
  step "I should see a \"input\" selector with value \"#{@profile.user.email}\""
  step "I should see a \"input\" selector with value \"#{@profile.first_name}\""
  step "I should see a \"input\" selector with value \"#{@profile.last_name}\""
  step "I should see a \"input\" selector with value \"#{@profile.address}\""
  step "I should see a \"input\" selector with value \"#{@profile.city}\""
  step "I should see a \"input\" selector with value \"#{@profile.zip_code}\""
  step "I should see a \"input\" selector with value \"#{@profile.phone}\""
  step "I should see the option \"#{@profile.school_type}\" of selector \"profile_school_type\" selected"
  step "I should see a \"input\" selector with value \"#{@profile.school}\""
  step "I should see the option \"#{@profile.occupation}\" of selector \"profile_occupation\" selected"
  expect(find("#profile_gender_#{@profile.gender}").value.to_i).to eq(@profile.gender)
  step "I should see the option \"#{@profile.country_numeric_code}\" of selector \"profile_country_numeric_code\" selected"
  step "I should see the option \"#{@profile.state}\" of selector \"state\" selected"
  step "I should see the option \"#{@profile.birthdate.day}\" of selector \"profile_birthdate_3i\" selected"
  step "I should see the option \"#{@profile.birthdate.month}\" of selector \"profile_birthdate_2i\" selected"
  step "I should see the option \"#{@profile.birthdate.year}\" of selector \"profile_birthdate_1i\" selected"
  step "I should see the option \"#{@profile.preferred_language}\" of selector \"profile_preferred_language\" selected"
  step 'I should see the checkbox profile_receive_mass_email checked'
end

Then(/^I should not see the recaptcha input$/) do
  expect(page.has_css?('div.recaptcha')).to eq false
end

Then(/^I should not see the terms acceptance checkbox$/) do
  step "I should not see \"#{I18n.t('terms_of_service')}\""
end

Then(/^I should see the successfully updated profile message$/) do
  step "I should see \"#{I18n.t 'successfully_updated_profile'}\""
end

Then(/^I should see the videos I sent that haven't been evaluated$/) do
  @not_evaluated_videos.each do |video|
    expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to be true
  end
end

Then(/^I should see the titles of the video I sent that didn't finish encoding$/) do
  @not_finished_encoding_videos.each do |video|
    step "I should see \"#{video.title}\""
  end
end

Given(/^there is another user with a profile$/) do
  @another_user = create(:user)
  @another_users_profile = create(:profile, user: @another_user, first_name: 'Another', last_name: 'User')
end

Given(/^I visit the other user's profile page$/) do
  visit profile_path(id: @another_users_profile.id)
end

Then(/^I should see the other user's full name$/) do
  step "I should see \"#{@another_users_profile.full_name}\""
end

Then(/^I should not see the edit link$/) do
  step "I should not see \"#{I18n.t 'profiles.show.edit_profile'}\""
end

Then(/^I should see the amount of videos the other user's sent$/) do
  step "I should see \"#{@videos_to_be_encoded.size} #{I18n.t 'profiles.show.videos'}\""
end

Then(/^I should see the amount of featured videos the other user's sent$/) do
  step "I should see \"#{@featured_videos_to_be_shown.size} #{I18n.t 'profiles.show.featured_contents'}\""
end

Then(/^I should see the other user's videos that finished encoding$/) do
  @videos_to_be_shown.each do |video|
    expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to be true
  end
end

Then(/^I should see the other user's featured videos that finished encoding$/) do
  @featured_videos_to_be_shown.each do |video|
    expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to be true
  end
end

Then(/^I should not see the other user's videos that haven't been evaluated$/) do
  @not_evaluated_videos.each do |video|
    expect(has_css?("img[src=\"#{video.thumbnails.last.url}\"]")).to eq false
  end
end

Then(/^I should not see the titles of the other user's videos that didn't finish encoding$/) do
  @not_finished_encoding_videos.each do |video|
    step "I should not see \"#{video.title}\""
  end
end
