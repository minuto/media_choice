Feature: Signing in and out
  In order to control access to the site
  As a regular user
  I should be able to sign in and out of the system

  Background:
    Given I have a registered and confirmed user

  @javascript
  Scenario: Signing in and out
    Given I am at the home page
    When I click on the Sign in link
    And I fill the email field with the user's email
    And I fill the password field with the user's password
    And I click on the Sign in button
    Then I should see the successful login message
    When I click on the account drop down link
    And I click on the Sign out link
    Then I should see the successful logout message

  @javascript
  Scenario: Logging in as admin
    Given I am at the home page
    Then I should not see the control panel link
    When I try to access the admin control panel
    Then I should see the you need to sign in message
    When I am at the home page
    And I click on the Sign in link
    And I fill the email field with the user's email
    And I fill the password field with the user's password
    And I click on the Sign in button
    Then I should not see the control panel link
    When I try to access the admin control panel
    Then I should see the you need to sign in message
    When I become an admin user
    And I am at the home page
    Then I should see the control panel link
    And I try to access the admin control panel
    Then I should be at the admin dashboard
