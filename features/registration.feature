Feature: Registration
  In order to be able to login to the site
  As a regular user
  I should be able to create an account on the remote service

  Scenario: Creating an account with valid credentials
  Given I am at the home page
  And I have the information for a new registration
  When I click on the sign up link
  And I fill the email field of the registration form
  And I confirm the email
  And I fill the password field of the registration form
  And I fill the password confirmation field with the user's password
  When I click on the sign up button
  And I confirm the user's registration
  Then the user should be confirmed
  And the user should be on the home page

  Scenario: Invalid credentials - Email already taken
  Given There is a registered user
  And I am at the home page
  And I have the information for a duplicate registration
  When I click on the sign up link
  And I fill the email field of the registration form
  And I confirm the email
  And I fill the password field of the registration form
  And I fill the password confirmation field with the user's password
  And I click on the sign up button
  Then I should see the email is already taken message
