Feature: Seeing the Home page
  In order to see the content of the site
  As a regular, non registered user
  I should be able to see the home page

  @javascript
  Scenario: Seeing the home page
    Given there are contests on the database
    And there are contents on the database
    And there are home highlights on the database
    And some of the contents have been evaluated
    And some of the evaluated contents have been given featured judge ratings
    When I am at the home page
    Then I should see the contests
    And I should see only the featured contents
    And I should see the home highlights
    When I click on the Recent tab
    Then I should see the evaluated contents ordered by creation date

  @javascript
  Scenario: Home page cache update
    Given there are contests on the database
    When I visit the home page with the locale en
    Then I should see the contests
    When I visit the home page with the locale pt-BR
    Then I should see the contests
    When a new started contest is added
    And I clear the cache
    When I visit the home page with the locale en
    Then I should see the new contest
    And I should see the contests
    When I visit the home page with the locale pt-BR
    Then I should see the new contest
    And I should see the contests
