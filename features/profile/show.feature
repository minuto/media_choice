Feature: Seeing a Profile's page
  In order to check my registration
  As a regular user
  I should be able to manage my profile

  Scenario: Seeing a nonexistent profile
    Given I try to see a nonexistent profile
    Then I should see the not found page

  @javascript
  Scenario: Seeing the profile page
    Given I have a registered and confirmed user
    And I have a complete profile
    And I am logged in
    And I am at the home page
    When I mouseover my account menu item
    Then I should see the profile's navbar management links
    Given I have sent videos
    And some videos have finished encodings
    And some videos have been evaluated
    When I click on the my page link
    Then I should be at my page
    And I should see my full name
    And I should see the edit link
    And I should see the amount of videos I sent
    And I should see the amount of featured videos I sent
    And I should see the videos I sent that finished encoding
    And I should see the featured videos I sent that finished encoding
    And I should see the videos I sent that haven't been evaluated
    And I should see the titles of the video I sent that didn't finish encoding

  @javascript
  Scenario: Seeing another user's profile page
    Given I have a registered and confirmed user
    And there is another user with a profile
    And the other user has sent videos
    And some videos have finished encodings
    And some videos have been evaluated
    And I am logged in
    And I visit the other user's profile page
    Then I should see the other user's full name
    And I should not see the edit link
    And I should see the amount of videos the other user's sent
    And I should see the amount of featured videos the other user's sent
    And I should see the other user's videos that finished encoding
    And I should see the other user's featured videos that finished encoding
    And I should not see the other user's videos that haven't been evaluated
    And I should not see the titles of the other user's videos that didn't finish encoding
