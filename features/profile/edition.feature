Feature: Editing a Profile
  In order to change personal data
  As a regular user
  I should be able to edit my profile

  Background:
    Given I have a registered and confirmed user
    And I am logged in
    And I have a complete profile

  @javascript
  Scenario: Correct data loading
    Given I am at the user's profile page
    When I click on the edit link
    Then I should be at the profile's edit page
    And I should see my information correctly loaded
    And I should not see the recaptcha input
    And I should not see the terms acceptance checkbox

  @javascript
  Scenario: Updating profile with valid data
    Given I am at the user's profile page
    When I click on the edit link
    And I fill the "user-first-name" field with "New"
    And I fill the "user-last-name" field with "Name"
    And I fill the "user-city" field with "New City"
    And I fill the "user-postal-code" field with "00212-222"
    And I select "06" from "profile_birthdate_3i"
    And I select "11" from "profile_birthdate_2i"
    And I select "1991" from "profile_birthdate_1i"
    When I click on the button to send the profile information
    Then I should see the successfully updated profile message
    And I should be at my page
    And I should see "New Name"
