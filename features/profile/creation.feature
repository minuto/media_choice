Feature: Creating a Profile
  In order to complete my registration
  As a regular user
  I should be able to create a profile

  Background:
    Given I have a registered and confirmed user

  @javascript
  Scenario: Filling valid data
    Given I am at the home page
    And I am logged in
    And I click on the account drop down link
    And I click the new profile link
    And I fill the "user-first-name" field with "Media"
    And I fill the "user-last-name" field with "Choice User"
    And I fill the "user-city" field with "São Paulo"
    And I select "Brasil" from "profile[country_numeric_code]"
    And I select "São Paulo" from "profile[state]"
    And I select any occupation
    And I fill the "user-postal-code" field with "00981-009"
    And I select "05" from "profile_birthdate_3i"
    And I select "10" from "profile_birthdate_2i"
    And I select "1990" from "profile_birthdate_1i"
    And I choose "profile_gender_2"
    And I select "pt-BR" from "profile[preferred_language]"
    And I select "pt-BR" from "profile[preferred_language]"
    And I check the "user-terms" box
    When I click on the button to send the profile information
    Then I should see the successfully created profile message

  @javascript
  Scenario: Filling invalid data
    Given I am at the home page
    And I am logged in
    And I click on the account drop down link
    And I click the new profile link
    And I fill the "user-first-name" field with "Media"
    And I fill the "user-last-name" field with "Choice User"
    And I fill the "user-city" field with "São Paulo"
    And I select "Brasil" from "profile[country_numeric_code]"
    And I select "São Paulo" from "profile[state]"
    And I fill the "user-postal-code" field with "00981-009"
    And I select "05" from "profile_birthdate_3i"
    And I select "10" from "profile_birthdate_2i"
    And I select "1990" from "profile_birthdate_1i"
    And I check the "user-terms" box
    When I click on the button to send the profile information
    Then I should see the error message for occupation can't be blank
