Feature: Video player
  In order to see video contents
  As a regular user
  I should be able to see the finished video contents

  Background:
    Given I have a registered and confirmed user
    And I am logged in

  @javascript @wip
  Scenario: Accessing a finished video page
    Given there is a video author user
    And the video author has a stored, encoded and evaluated video
    And the video is on a contest
    When I visit the video's page
    Then I should see the video player
    And I should see the contest name
    And I should be able to see the video's author
    And I should be able to see the "vjs-share-button" player button
    And I should be able to see the "vjs-map-button-image-destination" player button
    When I click on the "vjs-share-button" player link
    Then I should see "fa-facebook" icon
    And I should see "fa-twitter" icon
    And I should see "fa-google-plus" icon
    And I should see "fa-link" icon
    When I close the share box
    And I click on the "vjs-map-button-image-destination" player link
    Then I should be at the map page

  @javascript
  Scenario: Accessing a finished video page when the author lacks location information
    Given there is a video author user without some location information
    And the video author has a stored, encoded and evaluated video
    And the video is on a contest
    When I visit the video's page
    Then I should see the video player
    And I should see the contest name
    And I should be able to see the video's author with placeholders replacing the missing information

  @javascript
  Scenario: Accessing video page when the encoding process is not finished
    Given there is a video author user
    And the video author has a stored but not encoded video
    And the stored video has been evaluated
    When I visit the video's page
    Then I should see the content is not ready message

  @javascript
  Scenario: Accessing video page when the content wasn't evaluated
    Given there is a video author user
    And the video author has a stored and encoded video
    When I visit the video's page
    Then I should see the content is not evaluated message

  @javascript
  Scenario: Rating-dependent icons
    Given there is a video author user
    And the video author has a stored, encoded and evaluated video
    When the video has a featured judge rating
    And I visit the video's page
    Then I should see the featured icon
    When the video does not have a fetured judge rating
    And I visit the video's page
    Then I should not see the featured icon
