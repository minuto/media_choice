Feature: Public Rating
  In order to evaluate videos
  As a regular user
  I should be able to rate and see the average grade of videos

  Background:
    Given I have a registered and confirmed user
    And I am logged in

  @javascript
  Scenario: Rating a video
    Given there is a video author user
    And the video author has a stored, encoded and evaluated video
    And the video has a grade of 5
    When I visit the video's page
    Then I should see the video's grade as 5
    When I grade the video as 1
    Then I should see the successful vote message
    And I should see the video's grade as 1
    When I try to vote again
    Then I should see the unsuccessful vote message
    When I visit the video's page
    Then I should see the video's grade as 5
    When I clear the cache
    And I visit the video's page
    Then I should see the video's grade as 3
