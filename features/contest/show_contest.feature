Feature: Showing Contests
  In order to see all contests
  As a regular user
  I should be able to see the contest page

  Background:
    Given I have a registered and confirmed user
    And there is an ongoing contest
    And the contest has associated contents
    And the contest has associated sponsors

  @javascript
  Scenario: Seeing the contest page
    Given I am logged in
    And the associated contents have been evaluated
    When I visit the contest page
    Then I should see the contest's name
    And I should see the contest's description
    And I should see the share link
    And I should see the contest's logo
    And I should see the associated contents
    And I should see the associated sponsors

  @javascript
  Scenario: Seeing the contest page when the contents have not been evaluated
    Given I am logged in
    And some of the associated contents have been evaluated
    When I visit the contest page
    Then I should see the contest's name
    And I should see only the evaluated contents
