Feature: Managing users
  In order to manage the site's users
  As an admin user
  I should be able to see, create and delete other users

  Background:
    Given I have a logged in admin user
    And I am at the admin dashboard

  Scenario: Complete user management
    When I click on the "User" admin menu link
    Then I should see all the stored users
    When I click on the New "User" admin link
    And I fill the required User fields
    And I click on the Create "User" admin button
    Then the new "User" should be successfully created on admin
    And I should see the new user's details
    When I click on the Delete "User" admin link
    Then the new "User" should be successfully destroyed on admin
    And I should be at the "users" admin index
    And the new user should not be listed
