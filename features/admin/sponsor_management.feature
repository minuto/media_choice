Feature: Sponsor Management
  In order to manage the site's sponsors
  As an admin user
  I should be able to create, edit and show sponsors

  Background:
    Given I have a logged in admin user
    And I am at the "sponsor" management page

  @javascript
  Scenario: Complete sponsor management
    Given I click on the New "Sponsor" admin link
    And I fill the required Sponsor fields
    When I click on the Create "Sponsor" admin button
    Then the new "Sponsor" should be successfully created on admin
    And I should see the new sponsor's details
    When I click on the Edit "Sponsor" admin link
    And I change some Sponsor fields
    When I click on the Update "Sponsor" admin button
    Then the new "Sponsor" should be successfully updated on admin
    And I should see the sponsor's new details
    When I am at the "sponsor" management page
    Then the new sponsor should be listed
