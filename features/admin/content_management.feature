Feature: Content Management
  In order to manage the site's contents
  As an admin user
  I should be able to see and destroy contents

  Background:
    Given I have a logged in admin user
    And the user has a stored video

  @javascript
  Scenario: Complete content management
    When I am at the "content" management page
    Then I should see a list with the contents
    When I click to view a content
    Then I should see the content's details
    And I should see the content's encoded contents details
    When I click on the Delete "Content" admin link
    Then I should be at the content management page
    And the content should be successfully deleted
    When I click on the deleted scope
    Then I should see the deleted content
