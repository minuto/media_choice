Feature: Mass Mail Management
  In order to send mass mails
  As an admin user
  I should be able to view, search, create, edit and delete mass mails

  Background:
    Given I have a logged in admin user
    And I am at the "mass_mail" management page

  Scenario: Complete mass_mail management
    Then I should see the Mass Mail's index attributes
    And I should see the Mass Mail's filter options
    When I click on the New "MassMail" admin link
    Then I should not see the status field
    When I fill the required MassMail fields
    And I click on the Create "MassMail" admin button
    Then the new "MassMail" should be successfully created on admin
    And I should see the new mass_mail's details
    When I click on the Edit "MassMail" admin link
    When I fill the required MassMail fields with other information
    And I click on the Save "MassMail" admin button
    Then I should see the edited mass_mail's details for the other information
    When I click on the Delete "MassMail" admin link
    Then the new "MassMail" should be successfully destroyed on admin
    And I should be at the "mass_mails" admin index
    And the edited MassMail should not be listed

  Scenario: Sending the mass mail
    Given I have a MassMail
    And I am at the "mass_mail" management page
    When I click on the send mass mail link
    Then I should see "sending"
    And the show page should prohibit modifications
    Given I am at the "mass_mail" management page
    Then I should be only able to view the mass mail index actions
