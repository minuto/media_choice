Feature: Contest Management
  In order to manage the site's contests
  As an admin user
  I should be able to see, create and delete contests and its translations

  Background:
    Given I have a logged in admin user
    And I am at the "contest" management page

  @javascript
  Scenario: Complete contest management
    When I click on the New "Contest" admin link
    And I click on the New "ContestTranslation" admin link
    And I fill the required Contest fields
    And I fill the required Contest Translation fields
    And I click on the Create "Contest" admin button
    Then the new "Contest" should be successfully created on admin
    And I should see the new contest's details
    And I should see the new contest translation's details
    When I am at the "contest" management page
    Then the new contest should be listed
