Feature: Playlist management
  In order to create new playlists
  As an admin user
  I should be able to create and edit playlists

  Background:
    Given I have a logged in admin user
    And the user has multiple stored videos

  @javascript
  Scenario: Creating a playlist
    And I am at the "playlist" management page
    When I click on the New "Playlist" admin link
    And I check the visible checkbox
    And I click on the add playlist translation button
    And I fill the name field with "Playlist"
    And I select the "en" locale
    And I select the first two contents
    And I click on the Create "Playlist" admin button
    Then the playlist should have been created

  @javascript
  Scenario: Editing a playlist
    Given there is a created playlist on the database
    And the created playlist has a video in it
    And the created playlist has a translation
    And I am at the "playlist" management page
    When I click on the edit playlist link
    And I fill the name field with "New Playlist name"
    And I select the "pt-BR" locale
    And I remove one of the videos from the playlist
    And I click on the Update "Playlist" admin button
    Then the playlist should have been updated
    And the playlist should be empty
