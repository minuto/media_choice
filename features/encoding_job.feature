Feature: Send uploaded video to zencoder
  In order to be able to see video contents
  As the zencoder client
  I should be able to send a job for encoding and receive notifications

  @javascript
  Scenario: Sending a video to zencoder
    Given I have a registered and confirmed user
    And the user has a complete profile
    When the user creates a video content
    Then the video should have an url
    And a zencoder job should have been created
    When the zencoder job finishes
    Then an encoded content should have been created
    And I should receive a successful notification
    And the encoded content should have been updated
    And the original media file is still accessible

