Feature: Send Content
  In order to be able to see video contents
  As a regular user
  I should be able to send my own video

  Background:
    Given I have a video to be uploaded
    And there is an ongoing contest

  @javascript
  Scenario: Uploading a valid file
    Given I have a registered and confirmed user
    And I am logged in
    And I have a complete profile
    And I am at the send content page
    And I see my name on the Author field
    And I see the contest's name
    When I fill the title field with the video's title
    And I check the "user-terms" box
    And I fill the video's soundtrack
    And I fill the director field with my name
    And I mark the specific location checkbox
    And I fill the zip code field with my zip code
    And I send a video file
    Then the label of the file field should be the same as the file name
    When I mark the contest's checkbox
    And I click on the register button
    Then I should see the successful upload message
    And The selected contest should have been associated with the content
    And the zencoder job finishes
    And I should receive a successful notification
    And the last encoded content state should be "finished"
    And the sent content should have a correctly assigned path

  @javascript
  Scenario: Removing contest before upload
    Given I have a registered and confirmed user
    And I am logged in
    And I have a complete profile
    And I am at the send content page
    And there is another ongoing contest
    When I fill the title field with the video's title
    And I check the "user-terms" box
    And I fill the video's soundtrack
    And I fill the director field with my name
    And I mark the specific location checkbox
    And I fill the zip code field with my zip code
    And I send a video file
    And I mark all the contests' checkboxes
    And the other contest gets deleted
    And I click on the register button
    Then I should see the contest not found error message

  Scenario: Trying to send a video without logging in
    When I try to access the send content page
    Then I should be redirected to the home page
    And I should see the you need to be logged in to send contents message
