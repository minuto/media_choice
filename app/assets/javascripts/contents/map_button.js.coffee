class @MapButtonPluginBuilder
  constructor: (@map_button_html) ->
    videojs.plugin('mapButton', @mapButton)

  mapButton: (options) ->
    map_button_wrapper = document.createElement('div')
    map_button_wrapper.innerHTML = options['plugin_builder'].map_button_html

    @.el().appendChild(map_button_wrapper)
    @
