#= require infobubble
#= require jquery.rateit
#= require contents/public_rating
#= require contents/share_button
#= require contents/related_contents

class @VideoPopup
  constructor: ->
    @videojs_list = []
    @infoBubble = new InfoBubble({
      maxWidth: 480,
      minHeight: 300,
      maxHeight: 425,
      shadowStyle: 0,
      padding: 0,
      borderRadius: 0,
      arrowSize: 10,
      borderColor: '#2c2c2c',
      disableAnimation: true,
      backgroundClassName: 'player-geo',
    })

  reload_video_scripts: ->
    videos = $('video')
    if videos.length > 0
      video_id = videos[0].id.split('-').pop().split('_')[0]
      video_tag_id = 'video-' + video_id
      if !@videojs_list.includes(video_tag_id)
        $('#content_loading_' + video_id).find('div.rateit, span.rateit').rateit()
        video = videojs(video_tag_id)
        @videojs_list.push video.id()
        share_button = ''
        share_box = ''
        $.ajax(
          async: false
          method: 'GET'
          url: $('#share_box_' + video_id + '_link').val()
          success: (data) ->
            share_box = data
          dataType: 'html'
        )

        $.ajax(
          async: false
          method: 'GET'
          url: $('#share_button_' + video_id + '_link').val()
          success: (data) ->
            share_button = data
          dataType: 'html'
        )

        share_plugin_builder = new ShareButtonPluginBuilder(share_button, share_box)
        video.shareButton
          plugin_builder: share_plugin_builder

        add_related_videos()

  dispose_last_video: ->
    if @videojs_list.length > 0
      videojs(document.getElementById(@videojs_list.pop())).dispose()

  add_infobubble_listeners: ->
    @infoBubble.addListener 'domready', =>
      @reload_video_scripts()
