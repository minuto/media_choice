class @Markers
  constructor: (@marker_normal, @marker_featured) ->
    @markers_list = []

  retrieve_details: (div_section, center_video_id) ->
    details = []
    $(div_section).each (index, element) ->
      detail = {}
      latLng = {}
      latLng.lat = parseFloat($(this).children('input[name="latitude"]').first().val())
      latLng.lng = parseFloat($(this).children('input[name="longitude"]').first().val())
      detail.coordinates = latLng
      detail.featured_marker = $(this).children('input[name="markerType"]').first().val() == 'markerFeatured'
      detail.url = $(this).children('input[name="map_player_link"]').first().val()
      detail.center_marker = $(this).children('input[name="latitude"]').first().attr('id').split('-').pop() == center_video_id
      details.push(detail)
    details

  create_marker: (map, position, featured_marker, map_player_link) ->
    new (google.maps.Marker)(
      map: map
      position: position
      title: ''
      icon: if featured_marker then @marker_featured else @marker_normal
      map_player_link: map_player_link
    )

  load: (details, map, oms) ->
    latlngbounds = new (google.maps.LatLngBounds)
    for detail in details
      marker = this.create_marker(map, detail.coordinates, detail.featured_marker, detail.url)
      @markers_list.push(marker)
      oms.addMarker(marker)
      latlngbounds.extend detail.coordinates

      # Open center video information
      if detail.center_marker
        $(window).load ->
          google.maps.event.trigger marker, 'click'
          google.maps.event.trigger marker, 'click'
