#= require oms.min
#= require contents/map/video_popup

class @MapRenderer
  DEFAULT_ZOOM: 12
  MINIMUM_ZOOM: 3
  MAXIMUM_ZOOM: 25

  constructor: () ->
    @video_popup = new VideoPopup()
    @video_popup.add_infobubble_listeners()
    @center_video_id = $('#video_id').val()
    latitude_to_center_key = $("#latitude-" + @center_video_id).val()
    longitude_to_center_key = $("#longitude-" + @center_video_id).val()
    @google_map = new (google.maps.Map)($('#map-view-section')[0],
      center:
        lat: parseFloat(latitude_to_center_key)
        lng: parseFloat(longitude_to_center_key)
      zoom: @DEFAULT_ZOOM
      streetViewControl: false
      mapTypeControlOptions:
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
        position: google.maps.ControlPosition.LEFT_BOTTOM
      scrollwheel: false)
    @oms = new OverlappingMarkerSpiderfier(@google_map, {markersWontMove: true, markersWontHide: true})

  close_video_popup: ->
    @video_popup.infoBubble.close()

  load_markers: (markers) ->
    markers.load(markers.retrieve_details('.content-detailed-section', @center_video_id), @google_map, @oms)

  create_map_player_div: (data, video_id) ->
    content_details_div = document.createElement('div')
    content_details_div.className = 'content-detailed-section'
    content_details_div.innerHTML = data
    div = document.createElement('div')
    div.className = 'hidden'
    div.id = 'content_loading_' + video_id
    div.appendChild content_details_div
    div

  map_player_success_callback: (marker, map_player_div) ->
    @video_popup.dispose_last_video()
    @video_popup.infoBubble.setContent map_player_div
    @video_popup.infoBubble.open @google_map, marker

  oms_click_callback: (marker) ->
    splitted_url = marker.map_player_link.split('/')
    # this leverages the structure of the route which is of the form: /:locale/contents/:id/map_marker
    video_id = splitted_url[splitted_url.length - 2]
    if $('div#content_loading_' + video_id).length == 0
      $.get(
        marker.map_player_link
        (data) =>
          @map_player_success_callback(marker, @create_map_player_div(data, video_id))
        'html'
      )
    else
      @video_popup.infoBubble.open @google_map, marker

  add_oms_listeners: ->
    @oms.addListener 'click', (marker) =>
      @oms_click_callback(marker)

    @oms.addListener 'spiderfy', (markers) =>
      @close_video_popup()

  add_google_maps_listeners: ->
    google.maps.event.addListener @google_map, 'zoom_changed', =>
      if @google_map.getZoom() < @MINIMUM_ZOOM
        @google_map.setZoom @MINIMUM_ZOOM
      else if @google_map.getZoom() > @MAXIMUM_ZOOM
        @google_map.setZoom @MAXIMUM_ZOOM

    google.maps.event.addListener @google_map, 'click', =>
      @close_video_popup()

    google.maps.event.addListener @google_map, 'mousedown', =>
      @google_map.setOptions scrollwheel: true
