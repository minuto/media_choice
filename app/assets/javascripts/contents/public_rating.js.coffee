@changePublicRatingValueAndMessage = (e, data, status, xhr) ->
  $('.rateit-range').attr('aria-valuenow', data['grade'])
  $('#public_rating_message').text(data['message'])

@addCallbackToPublicRatingForm = ->
  $('#public_rating_form').on('ajax:success', changePublicRatingValueAndMessage)

$(document).ready @addCallbackToPublicRatingForm
