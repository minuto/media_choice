@video_zip_code = (event) ->
  if $('input[name=video-has-location]').first().is(':checked')
    $('#video-location').css('display', 'block')
    $('#video-location').prop('required', true);
  else
    $('#video-location').css('display', 'none')
    $('#video-location').prop('required', false)

$('input[name=video-has-location]').on('change', @video_zip_code)
