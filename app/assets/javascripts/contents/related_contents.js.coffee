#= require videojs-suggestedVideoEndcap

@add_related_videos = ->
  $('video').each (index, video_tag) ->
    # videojs appends '_html5_api' on the video id
    div_id = video_tag.id.split('_')[0]
    video_id = video_tag.id.split('_')[0].split('-').pop()
    url = $('#video-endcap-' + video_id)[0].value
    @success_response = (data) ->
      if data.length > 0
        videojs(div_id).suggestedVideoEndcap
          header: 'You may also like…'
          suggestions: data

    video_tag.onloadeddata = ->
      $.get url, @success_response, 'json'

$(document).ready(@add_related_videos)
