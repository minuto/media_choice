class @ShareButtonPluginBuilder
  constructor: (@share_button_html, @share_box_html) ->
    # register the plugin with video.js
    videojs.plugin('shareButton', @shareButton)

  shareButton: (options) ->
    share_button_wrapper = document.createElement('div')
    share_box_wrapper = document.createElement('div')
    share_button_wrapper.innerHTML = options['plugin_builder'].share_button_html
    share_box_wrapper.innerHTML = options['plugin_builder'].share_box_html

    @.el().appendChild(share_button_wrapper)
    @.el().appendChild(share_box_wrapper)
    @
