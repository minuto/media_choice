@responsible_fields = (event)->
  current_date = new Date()
  year_difference = current_date.getFullYear() - $('#profile_year').val()
  month_difference = current_date.getMonth() + 1 - $('#profile_month').val()
  day_difference = current_date.getUTCDate() - $('#profile_day').val()
  if year_difference <= 18
    if month_difference <= 0
      if day_difference < 0
        $('#responsible_fields').css('display', 'block')
        $('#user-responsible-name').prop('required', true)
        $('#user-responsible-id-number').prop('required', true)
        return
  $('#responsible_fields').css('display', 'none')
  $('#user-responsible-name').prop('required', false)
  $('#user-responsible-id-number').prop('required', false)

$('#profile_year').on('change', @responsible_fields)
$('#profile_month').on('change', @responsible_fields)
$('#profile_day').on('change', @responsible_fields)
