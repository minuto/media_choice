@state_select = (event) ->
  @success_response = (data) ->
    $('div#state_select').html(data)

  @request_params = ->
    state: $('#stored_state').val(),
    country_numeric_code: $('select#profile_country_numeric_code :selected').val(),
    locale: $('#locale').val()

  $.get('/states', @request_params(), @success_response)

$(document).ready(@state_select)
$('select#profile_country').on('change', @state_select)
