@school_fields = (event) ->
  if $('input[name=student]').first().is(':checked')
    $('#school_fields').css('display', 'block')
    $('#profile_school_type').prop('required', true)
    $('#user-school-name').prop('required', true)
  else
    $('#school_fields').css('display', 'none')
    $('#profile_school_type').prop('required', false)
    $('#user-school-name').prop('required', false)

$('input[name=student]').on('change', @school_fields)
$(document).ready(@school_fields)
