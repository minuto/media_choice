class Thumbnail < ApplicationRecord
  has_attached_file :image,
    storage: :s3,
    s3_credentials: AWSSettings.to_h,
    s3_region: AWSSettings.aws_region,
    styles: { small: '94x52' },
    path: ':class/:id/:style.:extension'
  validates_attachment_content_type :image, content_type: /\Aimage/

  delegate :width, to: :encoded_content
  delegate :height, to: :encoded_content

  belongs_to :encoded_content

  def url(style = nil)
    self.image.url(style)
  end
end
