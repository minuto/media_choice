class PlaylistTranslation < ApplicationRecord
  validates :name, :locale, presence: true
  belongs_to :playlist
end
