require 'validators/content_user_profile_validator'
require 'validators/content_contest_validator'

class Content < ApplicationRecord
  RELATED_CONTENTS_LIST_SIZE = 30

  validates :terms_of_service, acceptance: true

  validates :title, :user_id, :soundtrack, :director, presence: true
  validates_with ContentUserProfileValidator, unless: 'user_id.nil?'
  validates_with ContentContestValidator, unless: 'contests.blank?', on: :create

  after_create :send_to_zencoder
  after_create -> { JudgeEvaluation.create!(content: self) }
  after_save :update_coordinates, if: :zip_code_changed?

  has_many :encoded_contents
  has_many :public_ratings
  has_and_belongs_to_many :contests
  has_and_belongs_to_many :playlists
  has_one :coordinate
  has_one :judge_evaluation
  has_many :thumbnails, through: :encoded_contents
  has_many :original_medias

  belongs_to :user

  # The default_scope is applied across all queries to the model.
  # It is also applied while creating/building a record. It is not applied while updating a record. That means that Content.new will return an object with attribute 'deleted' assigned false.
  default_scope { where(deleted: false) }
  scope :finished, -> { includes(:encoded_contents).where(encoded_contents: { state: 'finished' }) }
  scope :unfinished, -> { includes(:encoded_contents).where(encoded_contents: { state: nil }).or(includes(:encoded_contents).where.not(encoded_contents: { state: 'finished' })) }
  scope :evaluated, -> { includes(:judge_evaluation).where(judge_evaluations: {evaluation_state: 'evaluated'}) }
  scope :not_evaluated, -> { includes(:judge_evaluation).where.not(judge_evaluations: {evaluation_state: 'evaluated'}) }
  scope :with_found_coordinates, -> { includes(:coordinate).where(coordinates: { found: true }) }
  scope :featured, -> { joins(:judge_evaluation).where('judge_evaluations.judge_rating >= ?', JudgeEvaluation::FEATURED_THRESHOLD) }
  scope :show_on_map, -> { finished.with_found_coordinates }
  scope :recent, -> { finished.evaluated.order(created_at: :desc) }

  accepts_nested_attributes_for :original_medias

  def view_counter_attribute
    'view_count'
  end

  include CoordinateSearcher
  include SupportForViewCounting

  delegate :evaluation_state, to: :judge_evaluation
  delegate :label_evaluated, to: :judge_evaluation
  delegate :label_corrupted, to: :judge_evaluation
  delegate :label_piracy, to: :judge_evaluation
  delegate :label_repeated, to: :judge_evaluation
  delegate :rollback_bad_state, to: :judge_evaluation
  delegate :bad_state?, to: :judge_evaluation
  delegate :featured?, to: :judge_evaluation
  delegate :evaluated?, to: :judge_evaluation
  delegate :judge_rating, to: :judge_evaluation

  def average_grade
    public_ratings.average(:grade)
  end

  def finished_encoded_contents
    encoded_contents.where(state: :finished)
  end

  def finished_encoded_contents?
    !finished_encoded_contents.empty?
  end

  def send_to_evaluation
    EvaluationSystem.request_evaluation(self)
    judge_evaluation.send_to_evaluation
  end

  def description
    nil
  end

  # FIXME: What about putting this one on the helper and adding hooks after some transactions? Would be update_coordinates(content)
  def update_coordinates
    if self.zip_code.nil?
      address = self.user.city
      type = :city
    else
      address = self.zip_code
      type = :zip_code
    end
    geolocation = search_coordinates(address, type)
    attributes = { latitude: geolocation[:lat], longitude: geolocation[:lng], found: !geolocation.empty?, content: self }
    if self.coordinate.nil?
      Coordinate.create(attributes)
    else
      self.coordinate.update(attributes)
    end
  end

  def related_contents
    Rails.cache.fetch("#{cache_key}/related_contents", expires_in: 10.hours) do
      contents_in_same_contests = Content.joins(:contents_contests).where('contents_contests.contest_id' => self.contests.pluck(:id)).where.not(id: self.id).distinct
      related_contents_list = contents_in_same_contests.featured
      related_contents_list = fill_with_additional_featured_contents(related_contents_list) if related_contents_list.size < RELATED_CONTENTS_LIST_SIZE
      related_contents_list.shuffle
    end
  end

  def fill_with_additional_featured_contents(related_contents_list)
    already_inserted_ids = (related_contents_list.pluck(:id) << self.id)
    related_contents_list + Content.where.not(id: already_inserted_ids).featured.sample(RELATED_CONTENTS_LIST_SIZE - related_contents_list.size)
  end

  def self.contents_for_map_with_logged_in_user(user)
    user_contents = user_finished_contents(user)
    user_contents + generic_contents_for_map(user.adult?)
  end

  def self.generic_contents_for_map(adult = false)
    Set.new(show_on_map.featured.evaluated.where(adult: adult))
  end

  def self.user_finished_contents(user)
    Set.new(show_on_map.where(user: user))
  end

  private

  def send_to_zencoder
    RequestZencoderJob.perform_later(self)
  end
end
