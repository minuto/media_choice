class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable,
    :confirmable, :lockable, :timeoutable

  validates :email, confirmation: true

  has_one :profile
  has_many :contents
  has_many :public_ratings

  delegate :first_name, to: :profile
  delegate :last_name, to: :profile
  delegate :birthdate, to: :profile
  delegate :city, to: :profile

  scope :confirmed, -> { where.not(confirmed_at: nil) }
  # Select users that want to receive emails and have the preferred language equal to the email locale
  scope :mailing_list, ->(locale) { includes(:profile).where(profiles: { receive_mass_email: true, preferred_language: locale }) }

  def locale
    profile.nil? ? I18n.locale.to_s : profile.preferred_language
  end

  def adult?
    profile.nil? ? false : profile.adult?
  end
end
