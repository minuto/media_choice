class Sponsor < ApplicationRecord
  has_and_belongs_to_many :contests
  validates :display_count, :name, presence: true

  has_attached_file :logo,
    storage: :s3,
    s3_credentials: AWSSettings.to_h,
    s3_region: AWSSettings.aws_region,
    styles: { thumb: 'x100' },
    path: ':class/:id/:attachment/:style.:extension' # legacy path from festivaldominuto.com.br
  validates_attachment_content_type :logo, content_type: %r{\Aimage\/.*\Z}
  validates_attachment :logo, presence: true

  def view_counter_attribute
    'display_count'
  end

  include SupportForViewCounting
end
