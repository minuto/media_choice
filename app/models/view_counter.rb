class ViewCounter < ApplicationRecord
  validates :viewed_model, :model_id, presence: true
end
