class JudgeEvaluation < ApplicationRecord
  belongs_to :content
  validates :content, presence: true, uniqueness: true

  BAD_STATES = [
    CORRUPTED = 'corrupted'.freeze,
    PIRACY = 'piracy'.freeze,
    REPEATED = 'repeated'.freeze
  ].freeze

  FEATURED_THRESHOLD = 4

  state_machine :evaluation, initial: :pending, attribute: :evaluation_state do
    event :send_to_evaluation do
      transition pending: :evaluating
    end

    event :label_evaluated do
      transition evaluating: :evaluated
    end

    event :label_corrupted do
      transition evaluating: :corrupted
    end

    event :label_piracy do
      transition evaluating: :piracy
    end

    event :label_repeated do
      transition evaluating: :repeated
    end

    event :rollback_bad_state do
      transition [:corrupted, :piracy, :repeated] => :evaluated
    end
  end

  def bad_state?
    BAD_STATES.include? self.evaluation_state
  end

  def featured?
    !(self.judge_rating.nil? || self.judge_rating < FEATURED_THRESHOLD)
  end

  def evaluated?
    evaluation_state == 'evaluated'
  end
end
