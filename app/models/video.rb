class Video < OriginalMedia
  validates_attachment_content_type :media, content_type: %r{\Avideo\/.*\Z}
end
