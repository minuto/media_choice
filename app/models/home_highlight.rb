class HomeHighlight < ApplicationRecord
  validates :visible, :link, presence: true

  has_attached_file :banner,
    storage: :s3,
    s3_credentials: AWSSettings.to_h,
    s3_region: AWSSettings.aws_region
  validates_attachment_content_type :banner, content_type: %r{\Aimage\/.*\Z}

  validates_attachment :banner, presence: true

  scope :visible, -> { where(visible: true) }
end
