module CoordinateSearcher
  def search_coordinates(address, type = :city)
    parameters = { key: GoogleMapsSettings.key }
    parameters.merge!(type == :zip_code ? { components: "postal_code: #{address}" } : { address: address })
    response_body = get_response_body_from_google_maps_api(parameters)
    return {} if response_body[:status] != 'OK'
    results = response_body['results']
    results.first['geometry']['location'].with_indifferent_access
  end

  private

  def get_response_body_from_google_maps_api(parameters)
    uri = URI("#{GoogleMapsSettings.api_address}#{GoogleMapsSettings.action_address}")
    uri.query = URI.encode_www_form(parameters)
    response = Net::HTTP.get_response(uri)
    JSON.parse(response.body).with_indifferent_access
  end
end
