# Models that want to count views need to include this module and implement #view_counter_attribute.
# See Sponsor and Content for examples.
module SupportForViewCounting
  def self.included(base)
    raise NotImplementedError.new('Classes including SupportForViewCounting module need to implement a view_counter_attribute method. It should return a string with the name of the attribute that will store view counts.') unless base.instance_methods.include?(:view_counter_attribute)
  end

  def increase_view_count_by(amount)
    self.update("#{self.view_counter_attribute}": self.send(self.view_counter_attribute) + amount)
  end
end
