class ContestTranslation < ApplicationRecord
  belongs_to :contest

  validates :locale, :name, presence: true
  validates :contest_id, presence: true, unless: proc { |record| Contest.find_by_id(record.contest_id).nil? }
  validates :locale, inclusion: { in: I18n.available_locales.map(&:to_s), message: I18n.t('contest_translation_locale_not_available') }

  has_attached_file :logo,
    storage: :s3,
    s3_credentials: AWSSettings.to_h,
    s3_region: AWSSettings.aws_region,
    styles: { thumb: '270x90>' },
    path: 'contest_logos/:id/:attachment/:style.:extension'
  validates_attachment_content_type :logo, content_type: %r{\Aimage\/.*\Z}

  validates_attachment :logo, presence: true
end
