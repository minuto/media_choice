require 'validators/numeric_code_maps_to_name_validator'

class Profile < ApplicationRecord
  belongs_to :user

  validates :terms_of_service, acceptance: true
  validates :first_name, :last_name, :country_numeric_code, :state, :city,
    :zip_code, :birthdate, :gender, :occupation, presence: true
  validates :responsible_name, :responsible_document, presence: true, if: :underaged?
  validates :country_numeric_code, format: { with: /\A\d\d\d\z/, message: I18n.t('invalid_numeric_code') }
  validates_with NumericCodeMapsToNameValidator
  validates :gender, format: { with: /\A(1|2)\z/, message: I18n.t('invalid_gender') }
  validates :occupation, format: { with: /\A([1-9]|1[0-5])\z/, message: I18n.t('invalid_occupation') }
  validates :school_type, format: { with: /\A[1-5]\z/ }, allow_nil: true

  # When migrating data from the old website, some users may not have registered birthdates.
  # That would break the migration for we enforce a 'null: false' on this attribute on the database.
  # This callback is a workaround for those cases. We simply use a default value of 12 years. It won't affect new users because we validate the birthdate field on the form.
  # Someday this may be removed.
  before_validation do
    self.birthdate = (Time.zone.today - 12.years) if birthdate.blank?
  end

  def adult?
    ((Time.zone.today - birthdate) / 365).to_i >= 18
  end

  def underaged?
    !adult?
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
