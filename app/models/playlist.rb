class Playlist < ApplicationRecord
  has_and_belongs_to_many :contents
  has_many :playlist_translations, dependent: :destroy

  scope :visible, -> { where(visible: true) }

  def name(locale = I18n.locale)
    translation = playlist_translations.find_by(locale: locale)
    return I18n.t('missing_playlist_translation', field: PlaylistTranslation.human_attribute_name(__method__), _locale: locale, id: id) if translation.nil?
    translation.name
  end
end
