class PublicRating < ApplicationRecord
  validates :user_id, :content_id, :grade, presence: true

  validates :user_id, uniqueness: { scope: :content_id, message: I18n.t('can_only_vote_once') }
  validates :grade, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 5 }

  belongs_to :content
  belongs_to :user
end
