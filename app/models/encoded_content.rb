class EncodedContent < ApplicationRecord
  has_attached_file :video,
    storage: :s3,
    s3_credentials: AWSSettings.to_h,
    s3_region: AWSSettings.aws_region,
    hash_secret: self.name,
    hash_data: ':class/outputs/:id',
    path: ':class/:id/:hash.:extension'

  validates_attachment_content_type :video, content_type: %r{\Avideo\/.*\Z}
  validates :content_id, :job_id, presence: true

  belongs_to :content
  has_one :thumbnail
end
