require 'validators/contest_time_validator'

class Contest < ApplicationRecord
  has_many :contest_translations
  has_and_belongs_to_many :contents
  has_and_belongs_to_many :sponsors
  validates :start_date, presence: true
  validates_with ContestTimeValidator, if: 'start_date && end_date'

  scope :started, -> { where('start_date <= :now AND (end_date IS NULL OR end_date >= :ending)', now: Time.zone.now, ending: Time.zone.now - 1.day) }
  scope :finished, -> { where('end_date IS NOT NULL AND end_date <= :now', now: Time.zone.now - 1.day) }

  def name(locale = I18n.locale)
    translation = contest_translations.find_by(locale: locale)
    return I18n.t('missing_contest_translation', field: ContestTranslation.human_attribute_name(__method__), _locale: locale, id: id) if translation.nil?
    translation.name
  end

  def description(locale = I18n.locale)
    translation = contest_translations.find_by(locale: locale)
    return I18n.t('missing_contest_translation', field: ContestTranslation.human_attribute_name(__method__), _locale: locale, id: id) if translation.nil?
    translation.description
  end

  def logo_url(locale = I18n.locale)
    translation = contest_translations.find_by(locale: locale)
    return ActionController::Base.helpers.image_path('logo.jpg') if translation.nil?
    translation.logo.url
  end

  def finished_contents
    contents.evaluated.finished
  end

  def contents_to_show
    finished_contents.order('judge_evaluations.judge_rating DESC', created_at: :desc)
  end

  def started?
    start_date < Time.zone.now
  end

  def finished?
    !end_date.nil? && end_date < Time.zone.now - 1.day
  end

  def ongoing?
    started? && !finished?
  end
end
