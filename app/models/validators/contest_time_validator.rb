class ContestTimeValidator < ActiveModel::Validator
  def validate(record)
    record.errors[:date] << I18n.t('invalid_contest_date') if record.start_date > record.end_date
  end
end
