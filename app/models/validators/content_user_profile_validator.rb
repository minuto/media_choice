class ContentUserProfileValidator < ActiveModel::Validator
  def validate(record)
    record.errors[:user] << I18n.t('user_needs_profile') if User.find(record.user_id).profile.nil?
  end
end
