class NumericCodeMapsToNameValidator < ActiveModel::Validator
  def validate(record)
    record.errors[:country] << I18n.t('no_numeric_code_mapping') if Carmen::Country.numeric_coded(record.country_numeric_code)&.name.nil?
  end
end
