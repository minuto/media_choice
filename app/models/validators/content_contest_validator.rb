class ContentContestValidator < ActiveModel::Validator
  def validate(record)
    record.errors[:contests] << I18n.t('no_ongoing_contest_associated') unless record.contests.all?(&:ongoing?)
  end
end
