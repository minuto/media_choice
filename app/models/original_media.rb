class OriginalMedia < ApplicationRecord
  has_attached_file :media,
    storage: :s3,
    s3_credentials: AWSSettings.to_h,
    s3_region: AWSSettings.aws_region,
    path: ':class/:id/:filename'

  validates_attachment :media, presence: true

  do_not_validate_attachment_file_type :media

  belongs_to :content
end
