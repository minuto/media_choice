class MassMail < ApplicationRecord
  validates :status, :locale, :subject, :body, presence: true

  state_machine :send_email, initial: :pending, attribute: :status do
    after_transition on: :send_email, do: :create_job

    event :send_email do
      transition pending: :sending
    end

    event :successful_send do
      transition sending: :successful
    end

    event :error_sending do
      transition sending: :sent_with_error
    end

    event :retry do
      transition sent_with_error: :sending
    end
  end

  def create_job(_transition)
    MassMailJob.perform_later(self)
  end
end
