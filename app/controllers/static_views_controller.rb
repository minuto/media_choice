class StaticViewsController < ApplicationController
  layout false

  def terms_of_use; end

  def creative_commons; end

  def sponsorship; end

  def contact_us; end

  def soundtrack_copyright; end

  def faq; end

  def education; end

  def exhibit_on_your_institution; end

  def about_us; end

  def image_copyright; end

  def exhibition_network; end
end
