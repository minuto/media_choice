class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_devise_parameters, if: :devise_controller?
  before_action :set_locale

  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  protected

  # We don't have a way to test this unless we have the Devise controllers among our code.
  # Since creating the controllers looks wronger than not testing this two
  # lines. I think we can live without 100% of coverage
  # :nocov:
  def configure_devise_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit(:email, :email_confirmation, :password, :password_confirmation)
    end
  end
  # :nocov:

  def set_locale
    I18n.locale = (
      params[:locale] ||
      http_accept_language.compatible_language_from(I18n.available_locales) ||
      I18n.default_locale
    )
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

  def authenticate_admin_user!
    if current_user.nil? || !current_user.admin
      flash[:error] = I18n.t(:unauthenticated, scope: [:devise, :failure])
      redirect_to root_path
    end
  end

  def current_admin_user
    return nil if current_user.nil? || !current_user.admin?
    current_user
  end

  private

  def render_404
    render file: File.join(Rails.root, 'public', '404.html'), layout: false, status: 404
  end
end
