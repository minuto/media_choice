class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :set_carmen_locale, only: [:show, :edit, :new, :states]
  before_action :authenticate_user!, except: [:states]
  before_action :authenticate_profile_owner, only: [:edit, :update, :destroy]

  respond_to :html

  # GET /profiles/1
  def show
    all_user_videos = Content.where(user: @profile.user)
    @finished_videos = all_user_videos.finished
    @featured_contents = @finished_videos.featured
    if current_user == @profile.user
      @unfinished_videos = all_user_videos.unfinished
      @non_evaluated_videos = @finished_videos.not_evaluated
    else
      @unfinished_videos = []
      @non_evaluated_videos = []
    end
    @evaluated_videos = @finished_videos.evaluated
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = current_user.id

    respond_to do |format|
      if verify_recaptcha(model: @profile) && @profile.save
        format.html { redirect_to root_path, notice: I18n.t('successfully_created_profile') }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /profiles/1
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: I18n.t('successfully_updated_profile') }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /profiles/1
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: I18n.t('successfully_destroyed_profile') }
    end
  end

  def states
    states = Carmen::Country.numeric_coded(states_params).subregions
    render partial: 'profiles/states', locals: { states: states, selected_state: params['state'] }
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :gender, :state, :city, :country_numeric_code, :school, :responsible_document, :responsible_name, :receive_mass_email, :phone, :user_id, :address, :zip_code, :occupation, :school_type, :birthdate)
  end

  def set_carmen_locale
    Carmen.i18n_backend.locale = I18n.locale.to_s.split('-').first
  end

  def states_params
    params.require(:country_numeric_code)
  end

  def authenticate_profile_owner
    redirect_to(root_path, flash: { warning: I18n.t('not_profile_owner') }) if current_user&.profile != @profile
  end
end
