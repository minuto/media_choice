class ContestsController < ApplicationController
  before_action :set_contest, only: [:show]
  respond_to :html
  respond_to :json, only: [:index]

  # GET /contests/1
  def show
    @sponsors = @contest.sponsors
    @sponsors.each { |sponsor| ViewCounter.create!(viewed_model: Sponsor.to_s, model_id: sponsor.id) }
    @contents_list = Rails.cache.fetch("contest_#{@contest.id}_contents_list", expires_in: 2.hours) { @contest.contents_to_show }
  end

  def index
    @started_contests = Contest.started
    @finished_contests = Contest.finished
    render json: { started_contests: @started_contests, finished_contests: @finished_contests }
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_contest
    @contest = Contest.find(params[:id])
  end
end
