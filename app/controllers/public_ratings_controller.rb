class PublicRatingsController < ApplicationController
  def public_rating
    @public_rating = PublicRating.new(content_id: public_rating_params[:content_id], user: current_user, grade: public_rating_params[:grade])
    message = @public_rating.save ? I18n.t('successful_public_rating') : I18n.t('unsuccessful_public_rating')
    render json: { grade: public_rating_params[:grade], message: message }, status: :ok
  end

  private

  def public_rating_params
    params.permit(:content_id, :grade)
  end
end
