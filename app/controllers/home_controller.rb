class HomeController < ApplicationController
  def index
    @contests = Rails.cache.fetch("started_contests/#{I18n.locale}", expires_in: 2.hours) do
      Contest.started
    end
    @recent_contents, @featured_contents, @playlists = create_content_rows
    @highlights = Rails.cache.fetch("visible_home_highlights/#{I18n.locale}", expires_in: 2.hours) do
      HomeHighlight.visible
    end
  end

  private

  def create_content_rows
    [
      Rails.cache.fetch('recent_contents', expires_in: 2.hours) { Content.recent.first(30) },
      Rails.cache.fetch('featured_contents', expires_in: 2.hours) { Content.featured.order(created_at: :desc).limit(30) },
      Playlist.visible
    ]
  end
end
