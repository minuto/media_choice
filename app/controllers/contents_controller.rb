# FIXME: https://gitlab.com/minuto/media_choice/issues/99
# rubocop:disable Metrics/ClassLength
class ContentsController < ApplicationController
  before_action :set_content, only: [:show, :evaluation, :video, :map_view, :related_contents, :map_player, :share_button, :share_box]
  before_action :check_evaluated_content, only: [:show]
  before_action :find_last_encoded_video, only: [:show, :map_player]
  before_action :authenticate_user!, only: [:new, :create]
  before_action :check_profile, only: [:new, :create]
  before_action :find_contests, only: [:create]
  protect_from_forgery with: :null_session, only: :zencoder_callback

  respond_to :json, only: [:index, :video, :evaluation, :related_contents]

  # FIXME: https://gitlab.com/minuto/media_choice/issues/98
  # rubocop:disable Metrics/AbcSize
  # GET /contents.json
  def index
    contents = []
    status = :bad_request

    if params.key? :ids
      ids = params[:ids].split(',')
      contents = Content.where(id: ids)
      status = contents.count == ids.count ? :ok : :partial_content
    elsif params.key?(:user_id) && params.key?(:orig_id)
      contents = Content.where(user_id: params[:user_id], id: params[:orig_id])
      status = :ok
    end

    render json: contents.map { |content| EvaluationSystem.content_json_for_evaluation(content) }, status: status
  end
  # rubocop:enable Metrics/AbcSize

  def video
    render json: EvaluationSystem.content_json_for_evaluation(@content)
  end

  def evaluation
    evaluation_state = params[:content]['evaluation_state']
    if evaluation_state == 'evaluated'
      if @content.bad_state?
        @content.rollback_bad_state
      end
      @content.judge_evaluation.update(judge_rating: params[:content][:rating])
      @content.label_evaluated
    else
      transition = "label_#{evaluation_state}".to_sym
      @content.send(transition) if @content.respond_to? transition
    end
  end

  # GET /contents/1
  # GET /contents/1.json
  def show
    @video = @encoded_video.video
    ViewCounter.create!(viewed_model: Content.to_s, model_id: @content.id)
  end

  # GET /contents/new
  def new
    @content = Content.new
    @content.original_medias.build
    @contests = Contest.started
  end

  # POST /contents
  # POST /contents.json
  def create
    @content = Content.new(content_params)
    @content.user_id = current_user.id
    @content.contests = @contests

    respond_to do |format|
      if @content.save
        format.html { redirect_to root_path, notice: I18n.t('successfully_created_video') }
      else
        format.html { render :new }
      end
    end
  end

  def zencoder_callback
    @encoded_content = EncodedContent.find_by(job_id: params[:job][:id])
    unless @encoded_content.nil?
      update_encoded_content @encoded_content, params[:outputs].first
      @encoded_content.content.send_to_evaluation
    end

    head :no_content
  end

  def map_view
    if @content.coordinate&.found
      if user_signed_in?
        @contents = Content.contents_for_map_with_logged_in_user(current_user) + [@content]
      else
        @contents = Content.generic_contents_for_map + [@content]
      end
    else
      redirect_to content_path(@content.id), notice: I18n.t('not_found_on_map')
    end
  end

  def related_contents
    related = @content.related_contents
    render json: build_related_videos_json(related), status: :ok
  end

  def map_player
    render partial: 'map_player'
  end

  def share_button
    render partial: 'share_button', locals: { id: @content.id }
  end

  def share_box
    render partial: 'share_box', locals: { content: @content }
  end

  private

  def update_encoded_content(encoded_content, parameters)
    encoded_content.output_id = parameters[:id]
    encoded_content.state = parameters[:state]
    encoded_content.width = parameters[:width]
    encoded_content.height = parameters[:height]
    encoded_content.duration = parameters[:duration_in_ms]
    encoded_content.file_size = parameters[:file_size_in_bytes]
    encoded_content.video = URI.parse(parameters[:url])
    if encoded_content.save
      Thumbnail.create!(
        image: URI.parse(parameters[:thumbnails].first[:images].first[:url]),
        encoded_content: encoded_content
      )
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_content
    @content = Content.find(params[:id])
  end

  def check_evaluated_content
    redirect_back(fallback_location: root_path, flash: { warning: I18n.t('content_not_yet_evaluated') }) unless @content.evaluated?
  end

  def check_profile
    redirect_to(new_profile_path, flash: { warning: I18n.t('need_complete_profile_to_send_content') }) if current_user.profile.nil?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def content_params
    params.require(:content).permit(:title, :user_id, :adult, :soundtrack, :view_count, :deleted, :zip_code, :director, :co_director, :team, :allow_comments, original_medias_attributes: [:media])
  end

  def find_last_encoded_video
    redirect_to(root_path, flash: { warning: I18n.t('video_has_not_finished_encoding') }) && return unless @content.finished_encoded_contents?
    @encoded_video = @content.encoded_contents.last
  end

  def find_contests
    params.require(:contests)
    @contests = Contest.find(params[:contests])
  rescue ActiveRecord::RecordNotFound, ActionController::ParameterMissing => error
    redirect_to new_content_path, alert: I18n.t('contest_not_found')
    logger.debug error
  end

  def build_related_videos_json(related_list)
    related_list.map do |content|
      {
        # The title and full_name must have a limit of characters. Otherwise, it breaks the related contents CSS
        title: content.title.truncate(18),
        url: content_path(content.id),
        image: content.thumbnails.last.url,
        author: content.user.profile.full_name.truncate(22)
      }
    end
  end
end
# rubocop:enable Metrics/ClassLength
