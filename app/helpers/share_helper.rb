module ShareHelper
  def facebook_share(url, title)
    link_to("https://www.facebook.com/sharer/sharer.php?u=#{url}&t=#{title}", class: 'share-facebook fa-stack fa-1x', target: '_blank', title: 'Facebook') { icon('facebook') }
  end

  def twitter_share(url, title)
    link_to("https://twitter.com/share?url=#{url}&text=#{title}", class: 'share-twitter fa-stack fa-1x', target: '_blank', title: 'Twitter') { icon('twitter') }
  end

  def google_plus_share(url)
    link_to("https://plus.google.com/share?url=#{url}", class: 'share-google-plus fa-stack fa-1x', target: '_blank', title: 'Google +') { icon('google-plus') }
  end

  def icon(sharer)
    safe_join [
      content_tag(:i, '', class: 'fa fa-square fa-stack-2x'),
      content_tag(:i, '', class: "fa fa-#{sharer} fa-stack-1x fa-inverse")
    ]
  end
end
