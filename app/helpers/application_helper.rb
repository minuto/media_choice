module ApplicationHelper
  def i18n_logo
    "logo-#{I18n.locale}.png"
  end

  def i18n_top_banner
    "top-banner-#{I18n.locale}.png"
  end

  def flash_to_callout(key)
    key = 'success' if key == 'notice'
    key
  end

  def sponsor_banner
    image_tag "sponsor-banner-#{I18n.locale}.png"
  end
end
