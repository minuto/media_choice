module DeviseHelper
  def devise_error_messages!
    return '' unless devise_error_messages?
    resource_errors = resource.errors

    messages = safe_join resource_errors.full_messages.map { |msg| content_tag(:li, msg) }
    sentence = I18n.t('errors.messages.not_saved',
      count: resource_errors.count,
      resource: resource.class.model_name.human.downcase)

    content_tag(:div, class: 'callout alert', id: 'error_explanation') do
      content_tag(:h2, sentence) + content_tag(:ul, messages)
    end
  end

  def devise_error_messages?
    !resource.errors.empty?
  end
end
