module ContentsHelper
  def contest_list_for(content)
    contents_contests = content.contests
    if contents_contests.empty?
      I18n.t('contents.content_details.none')
    else
      contest_list = contents_contests.map { |contest| link_to(contest.name, contest_path(id: contest.id)) }
      safe_join(contest_list)
    end
  end
end
