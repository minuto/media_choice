module ProfileHelper
  def country_select(profile)
    list = Carmen::Country.all.map { |country| [country.name, country.numeric_code] }
    selected_country = profile.country_numeric_code.nil? ? Carmen::Country.coded('br').numeric_code : profile.country_numeric_code
    options_for_select(list, selected_country)
  end

  def state_select(states, selected_state)
    options_for_select(states.map(&:name), selected_state)
  end

  def occupation_select(selected_occupation)
    options_for_select(I18n.t('occupations').map do |key, occupation|
      key.zero? ? [occupation, nil] : [occupation, key]
    end, selected_occupation)
  end

  def school_type_select(selected_school_type)
    options_for_select(I18n.t('school_types').map do |key, school_type|
      key.zero? ? [school_type, nil] : [school_type, key]
    end, selected_school_type)
  end

  def user_owns_profile?(profile)
    current_user&.profile == profile
  end

  def display_location_information(profile)
    city, state = ['city', 'state'].map do |attribute|
      profile.send(attribute).nil? ? "#{I18n.t(attribute)} #{I18n.t('not_informed')}" : profile.send(attribute)
    end
    country = profile.country_numeric_code.nil? ? "#{I18n.t(attribute)} #{I18n.t('not_informed')}" : country_name(profile.country_numeric_code)
    content_tag(:p, "#{city} - #{state} #{country}")
  end

  def country_name(country_numeric_code)
    Carmen::Country.numeric_coded(country_numeric_code)&.name
  end

  def gender_radios(form)
    gender_radios_html = []

    I18n.t('genders').each do |id, gender|
      gender_radios_html << form.radio_button(:gender, id)
      gender_radios_html << form.label(gender)
    end

    safe_join(gender_radios_html)
  end

  def student_radios(profile)
    student_radios_html = []

    student_radios_html << radio_button_tag(:student, t('affirmative'), !profile&.school_type.nil?)
    student_radios_html << label_tag(t('affirmative'))
    student_radios_html << radio_button_tag(:student, t('negative'), profile&.school_type.nil?)
    student_radios_html << label_tag(t('negative'))

    safe_join(student_radios_html)
  end
end
