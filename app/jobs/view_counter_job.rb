class ViewCounterJob < ApplicationJob
  queue_as :counter

  def perform(*_args)
    ViewCounter.transaction do
      ViewCounter.group(:viewed_model, :model_id).count.each do |group, count|
        viewed_model = group.first
        model_id = group.last
        begin
          record = viewed_model.constantize.find(model_id)
        rescue ActiveRecord::RecordNotFound
          next # Ignore not found errors since if a model does not exist, it was removed and do not need to be updated anyway
        end
        record.increase_view_count_by(count)
      end

      ViewCounter.destroy_all
    end
  end

  after_perform do
    ViewCounterJob.set(wait: 2.hours).perform_later
  end
end
