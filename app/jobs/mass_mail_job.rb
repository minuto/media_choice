require 'mail_sender/mail_sender'

class MassMailJob < ApplicationJob
  queue_as :mass_mail
  attr_reader :mass_mail

  def perform(mass_mail)
    @mass_mail = mass_mail

    @mass_mail.retry if @mass_mail.sent_with_error?

    MailSender.new(self.mass_mail).send
  end

  after_perform do |job|
    job.mass_mail.successful_send
  end

  rescue_from Exception do |exception|
    self.mass_mail.error_sending

    raise exception
  end
end
