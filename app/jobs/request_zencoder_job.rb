class RequestZencoderJob < ApplicationJob
  queue_as :zencoder

  def perform(*args)
    content = args.first
    response = Zencoder::Job.create(input: URI.unescape(content.original_medias.last.media.url), test: Rails.env.test?, notifications: notification_url, output: ZencoderSettings.video.to_h)
    EncodedVideo.create(job_id: response.body['id'], content_id: content.id)
  end

  private

  # rubocop:disable TernaryParentheses
  # Those parentheses improve code readability and should stay
  def notification_url
    (Rails.env.test? || Rails.env.development?) ? [url: 'http://zencoderfetcher/'] : [url: ZencoderSettings.notifications.url]
  end
  # rubocop:enable TernaryParentheses
end
