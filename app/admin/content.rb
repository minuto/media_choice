ActiveAdmin.register Content do
  class Content
    has_one :profile, through: :user
  end

  actions :all, except: [:edit, :new, :create, :update]

  index do
    selectable_column
    id_column
    column :title
    column :director
    column :view_count
    column :adult
    column :user_id
    column :soundtrack
    column :evaluation_state
    column :judge_rating
    actions
  end

  filter :title
  filter :user_id
  filter :profile_first_name_cont, label:  -> { I18n.t('first_name') }
  filter :profile_last_name_cont, label: -> { I18n.t('last_name') }
  filter :adult
  filter :soundtrack

  scope :all
  scope :finished
  scope :evaluated
  scope(I18n.t('deleted')) { |scope| scope.unscoped.where(deleted: true) }

  member_action :show, method: :get do
    @resource = Content.unscoped.find(params[:id])
  end

  show do
    attributes_table do
      row :id
      row :title
      row :user_id
      row :adult
      row :judge_rating
      row :soundtrack
      row :view_count
      row :zip_code
      row :director
      row :co_director
      row :team
      row :allow_comments
      row :deleted
      row :created_at
      row :evaluation_state
      row :original_content do |content|
        video_tag(content.original_medias.last.media.url, controls: true)
      end
    end
    panel I18n.t('active_admin.details', model: EncodedContent.model_name.human) do
      table_for resource.encoded_contents do
        column :video do |encoded_content|
          video_tag(encoded_content.video.url, controls: true)
        end
      end
    end
  end

  # This overrides the default destroy method implemented by ActiveAdmin
  member_action :destroy, method: :delete do
    resource.update(deleted: true)
    redirect_to admin_contents_path, notice: I18n.t('flash.actions.destroy.notice', resource_name: Content.model_name.human)
  end
end
