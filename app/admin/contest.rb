ActiveAdmin.register Contest do
  class Contest
    accepts_nested_attributes_for :contest_translations
  end

  actions :all, except: [:destroy]

  permit_params :start_date, :end_date, contest_translations_attributes: [:id, :locale, :name, :description, :logo], sponsor_ids: []

  index do
    selectable_column
    id_column
    column :name
    column(:start_date) { |contest| I18n.l(contest.start_date.to_date, format: :long) }
    column(:end_date) { |contest| I18n.l(contest.end_date.to_date, format: :long) unless contest.end_date.nil? }
    column :created_at
    actions
  end

  filter :start_date
  filter :end_date

  scope :started
  scope :finished

  form do |f|
    f.inputs 'Contest Details' do
      f.input :start_date, as: :datepicker
      f.input :end_date, as: :datepicker, hint: I18n.t('end_date_note').to_s
      f.has_many :contest_translations do |contest_translation|
        contest_translation.input :locale, as: :select, collection: I18n.available_locales, include_blank: false
        contest_translation.input :name
        contest_translation.input :description
        contest_translation.input(
          :logo,
          as: :file,
          hint: (contest_translation.object.logo.exists? ? contest_translation.template.image_tag(contest_translation.object.logo.url) : nil)
        )
      end
      f.input :sponsors, as: :select, input_html: { multiple: true }, collection: Sponsor.all
    end
    f.actions
  end

  show do
    panel I18n.t('active_admin.details', model: Contest.model_name.human) do
      table_for contest do
        column :id
        column(:start_date) { |contest| I18n.l(contest.start_date.to_date, format: :long) }
        column(:end_date) { |contest| I18n.l(contest.end_date.to_date, format: :long) unless contest.end_date.nil? }
        column :created_at
      end
    end
    panel I18n.t('active_admin.details', model: ContestTranslation.model_name.human) do
      table_for contest.contest_translations do
        column :locale
        column :name
        column :description
        column :logo do |contest|
          image_tag(contest.logo.url(:thumb))
        end
      end
    end
  end
end
