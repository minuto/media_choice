ActiveAdmin.register Playlist do
  class Playlist
    accepts_nested_attributes_for :playlist_translations
  end

  actions :all, except: [:destroy]

  permit_params :visible, playlist_translations_attributes: [:id, :locale, :name], content_ids: []

  index do
    selectable_column
    column :name
    column :videos do |playlist|
      playlist.contents.size
    end
    actions
  end

  form do |f|
    f.inputs I18n.t('active_admin.details', model: Playlist.model_name.human) do
      f.input :visible, as: :boolean
      f.has_many :playlist_translations, heading: PlaylistTranslation.model_name.human do |playlist_translation|
        playlist_translation.input :locale, as: :select, collection: I18n.available_locales, include_blank: false
        playlist_translation.input :name
      end
      f.input :contents, as: :select, input_html: { multiple: true }, collection: Content.finished.evaluated, include_blank: false
    end
    f.actions
  end
end
