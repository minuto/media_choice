ActiveAdmin.register Sponsor do
  actions :all, except: [:destroy]

  permit_params :address, :logo, :name

  index do
    selectable_column
    id_column
    column :address
    column :display_count
    column :name
    column 'Logo' do |sponsor|
      image_tag sponsor.logo.url
    end
    actions
  end

  filter :address

  form do |f|
    f.inputs I18n.t('active_admin.details', model: Sponsor.model_name.human) do
      f.input :address
      f.input :name
      f.input :logo, as: :file, hint: (f.object.logo.present? ? image_tag(object.logo.url) : nil)
    end
    f.actions
  end

  show do
    attributes_table do
      row :id
      row :address
      row :display_count
      row :name
      row :logo do |sponsor|
        image_tag(sponsor.logo.url(:thumb))
      end
    end
  end
end
