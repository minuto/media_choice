ActiveAdmin.register MassMail do
  permit_params :subject, :body, :locale

  index do
    id_column
    column :locale
    column :subject
    column :status

    actions defaults: false do |mass_mail|
      actions = []

      actions << link_to(I18n.t('active_admin.view'), admin_mass_mail_path(mass_mail))
      if mass_mail.pending?
        actions << link_to(I18n.t('active_admin.edit'), edit_admin_mass_mail_path(mass_mail))
        actions << link_to(I18n.t('active_admin.delete'), admin_mass_mail_path(mass_mail), method: :delete)
        actions << link_to(I18n.t('active_admin.send_mass_mail'), send_email_admin_mass_mail_path(mass_mail), method: :put)
      end

      safe_join(actions, ' ')
    end
  end

  filter :subject
  filter :body
  filter :locale, as: :select, collection: I18n.available_locales
  filter :status, as: :select, collection: proc { MassMail.state_machines[:send_email].states.map(&:name).map(&:to_s) }

  form do |f|
    f.inputs  I18n.t('active_admin.details', model: MassMail.model_name.human) do
      f.input :locale, as: :select, collection: I18n.available_locales, include_blank: false
      f.input :subject
      f.input :body, as: :text
    end
    f.actions
  end

  config.clear_action_items!

  member_action :send_email, method: :put do
    resource.send_email

    redirect_to resource_path, notice: I18n.t('active_admin.sent_mass_mail')
  end

  action_item :send_email, only: :show, if: proc { mass_mail.pending? } do
    link_to I18n.t('active_admin.send_mass_mail'), send_email_admin_mass_mail_path(mass_mail), method: :put
  end

  action_item :edit, only: :show, if: proc { mass_mail.pending? } do
    link_to "#{I18n.t('active_admin.edit')} #{MassMail.model_name.human}", edit_admin_mass_mail_path(mass_mail)
  end

  action_item :destroy, only: :show, if: proc { mass_mail.pending? } do
    link_to "#{I18n.t('active_admin.delete')} #{MassMail.model_name.human}", admin_mass_mail_path, method: :delete
  end

  action_item :new, only: :index do
    link_to I18n.t('active_admin.new_model', model: MassMail.model_name.human), new_admin_mass_mail_path
  end
end
