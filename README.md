# README - MediaChoiceWeb

[![build status](https://gitlab.com/minuto/media_choice/badges/master/build.svg)](https://gitlab.com/minuto/media_choice/commits/master)[![rspec coverage report](https://gitlab.com/minuto/media_choice/badges/master/coverage.svg?job=rspec)](https://gitlab.com/minuto/media_choice/commits/master)

This is the MediaChoiceWeb application. It covers most of a film (or gif, or instagram...) festival functionalities such as:

* user registration
* content receiving
* content administration
* contests
* sponsors

Missing but planned features:

* Sponsor ads over the video
* Geolocalization and display videos on the map
* User profile page
* Judgement criteria display
* News
* Mass emailing
* Gifs
* Instagram tags contests

It **does not** cover the judgement of contents.

To know how to contribute and how to setup a working environment, please refer to the [CONTRIBUTING](CONTRIBUTING.md) and [HACKING](HACKING.md) files, respectively.

### License

Copyright (c) 2013-2016 The Author developers.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Authors

Please see the humans.txt (at the `public` folder) file with the authors
