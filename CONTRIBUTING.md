### Contributing

Please, have a look the wiki pages about development workflow and code standards:

  - https://gitlab.com/minuto/media_choice/wikis/development-workflow
  - https://gitlab.com/minuto/media_choice/wikis/code-standards
