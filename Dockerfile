FROM ruby:2.3.1
RUN apt-get update -qq && apt-get upgrade -y
RUN apt-get update -qq && apt-get install -y \
      build-essential \
      libpq-dev \
      sqlite3 \
      libsqlite3-dev \
      nodejs \
      npm
RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN npm -g install npm
RUN npm -g install phantomjs-prebuilt
RUN npm -g install istanbul
RUN gem update --system
RUN gem install bundler --no-ri --no-rdoc
ENV RAILS_ENV=test