# Developing

## Ruby version

`2.3.1`

You can easily install it through the Ruby Version Manager - RVM. Instructions on how to do it can be found at http://rvm.io

*NOTE:* If you are using the gnome-shell, or any derivate like terminator, you have to mark on your profile the option to use a "login bash".

## Application setup

* `./bin/setup`

### reCAPTCHA

For development and test environments, reCAPTCHA will not work out of the box (they use `localhost` as domain). Please, follow the steps documented [here](https://developers.google.com/recaptcha/docs/faq#localhost_support) and do not forget to update your keys on `recaptcha.yml`.

## CI container

We host a basic build system at gitlab's docker registry.

In order to update such container, edit the `Dockerfile` and run:

* **fist time only**: `docker login registry.gitlab.com`
* Always
  - `docker build -t registry.gitlab.com/minuto/media_choice .`
  - `docker push registry.gitlab.com/minuto/media_choice`

### Cucumber acceptance tests
If you are willing to run the cucumber acceptance tests inside Docker, don't forget to create `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `ZENCODER_API_KEY` variables as environment variables before building the container. The values need to be valid AWS and Zencoder keys, respectively.

### Google Maps API usage
To test and use the geocoding parts, which can retrieve the coordinates from videos' location, don't forget to replace the placeholder api key on the google_maps.yml configuration file with a valid one.
To get a valid geocoding API, visit https://developers.google.com/maps/documentation/geocoding/intro

## Migrating data
If you are trying to migrate data from the production database of `media-choice` app ('old website') to the database of this app ('new website'), firstly you'll need access to the database replica of the old website, on port 3306. The replica is currently hosted on Amazon RDS.

In case you can't access it, you'll need to create and SSH tunnel passing through a machine that has permission. Use the following command:

`ssh -v -L <localhost-port>:<replica-database-address>:3306 <user>@<machine-with-access-permission-to-the-replica> -N`

- `localhost-port` can be any port available on your local machine. For example, 8888
- `replica-database-address` can be either the IP address or the hostname of the replica database
- `machine-with-access-permission-to-the-replica` neccessarily needs to have permission to access the database replica
- `user` is a valid user on the 'machine with permissions' that you can access through SSH

You can test the connection by directly accessing the database with `mysql`:

`mysql -h 127.0.0.1 -u <database-user> -P <localhost-port> -p`

- `database-user` is a valid user registered on the database with read permissions at least
- `localhost-port` is the same number that you used for the SSH tunnel
- The `-p` option prompts you to type the database password

You'll also need to adjust your `database.yml` and add entries for `username`, `password`, `host` (the same as `replica-database-address`) and `port` (usually 3306) on the `production_replica` namespace.

After that, you can start using the migration tasks implemented under `lib/tasks/migration`.
