source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'

# Use Puma as the app server
# FIXME: the default restriction is '~> 3.0', but version 3.7 breaks teaspoon
gem 'puma', '~> 3.6.0'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'

# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Manage user authentication
gem 'devise'

# Browser language detection
gem 'http_accept_language'

# Country lists and form helpers
# TODO: see https://gitlab.com/minuto/media_choice/issues/166
gem 'carmen', git: 'https://github.com/jim/carmen.git', branch: 'master'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Foundation framework for Rails
gem 'foundation-rails', '~> 6.2.4' # 6.3 deprecates several used features that come up when running cucumber

# A collection of CSS3 powered hover effects to be applied to links, buttons, logos, SVG, featured images and so on
gem 'hover-rails'

# Easy file attachment management for Active Record
#
# 5.1.0 has shown errors on CI cucumber tests hanging forever:
# - https://gitlab.com/minuto/media_choice/builds/4943415
# - https://gitlab.com/minuto/media_choice/builds/4946758
# - https://gitlab.com/minuto/media_choice/builds/5025653
# - https://gitlab.com/minuto/media_choice/builds/5027041
#
gem 'paperclip', '~> 5.0.0'

# The official aws sdk gem, provides an API to access S3
gem 'aws-sdk', '~> 2'

# Easy Application settings without relying on environment vars
gem 'settingslogic'

# Make links open in lightbox (colorbox) a breeze
gem 'colorbox-rails'

# Zencoder integration: Cloud video encoding
gem 'zencoder'

# Job queue adapter for Application Job
gem 'delayed_job_active_record'

# Adds videojs v5.4.6 javascript asset
gem 'videojs_rails', git: 'https://github.com/marcheing/videojs_rails', branch: 'master'

# Enables to export the delayed_job queue to system's task manager
# FIXME: Version 0.83 generates invalid systemd units
# https://github.com/ddollar/foreman/issues/668
gem 'foreman', '< 0.83'

# Provides translations for devise's views
gem 'devise-i18n'

# Provides javascript and stylesheet assets for own carousel
gem 'owlcarousel-rails'

# Provides a state machine for controlling model states
gem 'state_machines-activerecord'

# Provides error messages and other active record translations for locales other than en
gem 'rails-i18n'

# Recaptcha. Human input.
gem 'recaptcha', require: 'recaptcha/rails'

# Framework for website admin backend
gem 'activeadmin', github: 'activeadmin' # FIXME: When version 1.0.0.pre5 or later gets out use it!

# Necessary for activeadmin on Rails 5
gem 'inherited_resources', github: 'activeadmin/inherited_resources'

# Official sendgrid api integration gem
gem 'sendgrid-ruby'

# MySQL2 adapter for migrating data
gem 'mysql2'

# Bulk operations for migrations
gem 'activerecord-import'

# Use PostgreSQL as the database for Active Record on production environment
gem 'pg'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri

  # Unit test framework
  gem 'rspec-rails', '~> 3.5'

  # Javascript test runner for Rails
  gem 'teaspoon-mocha'

  # Fixtures creation and management
  gem 'factory_girl'

  # Test zencoder notifications
  # PR address https://github.com/zencoder/zencoder-fetcher/pull/11
  gem 'zencoder-fetcher', git: 'https://github.com/marcheing/zencoder-fetcher.git'

  # Static code analyzer based on the ruby community guidelines: github.com/styleguide/ruby
  # FIXME: Newer version introduce additional checks that break the linting
  gem 'rubocop', '~> 0.42', '<= 0.43', require: false

  # Use sqlite3 as the database for Active Record on development and test environments
  gem 'sqlite3'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Automated gem update
  gem 'gisdatigo'

  # Implements the cucumber command for Spring
  gem 'spring-commands-cucumber'

  # Implements the rspec command for Spring
  gem 'spring-commands-rspec'

  # Implements the teaspoon command for Spring
  gem 'spring-commands-teaspoon'

  # Implements the rubocop command for Spring
  gem 'spring-commands-rubocop'
end

group :test do
  # Acceptance tests framework
  gem 'cucumber-rails', require: false

  # Database cleanup for each scenario
  gem 'database_cleaner'

  # JavaScript driver for Capybara
  gem 'poltergeist'

  # RSpec one-liners
  gem 'shoulda-matchers', '~> 3.1'

  # Test coverage report
  gem 'simplecov', require: false
end

group :production do
  # Cache store for memcached
  gem 'dalli'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
