class GoogleMapsSettings < Settingslogic
  source "#{Rails.root}/config/google_maps.yml"
  namespace Rails.env
end
