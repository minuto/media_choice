class RecaptchaSettings < Settingslogic
  source "#{Rails.root}/config/recaptcha.yml"
  namespace Rails.env
end
