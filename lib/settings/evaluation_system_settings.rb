class EvaluationSystemSettings < Settingslogic
  source "#{Rails.root}/config/evaluation_system.yml"
  namespace Rails.env
end
