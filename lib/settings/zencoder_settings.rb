class ZencoderSettings < Settingslogic
  source "#{Rails.root}/config/zencoder.yml"
  namespace Rails.env
end
