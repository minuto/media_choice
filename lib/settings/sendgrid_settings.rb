class SendgridSettings < Settingslogic
  source "#{Rails.root}/config/sendgrid.yml"
  namespace Rails.env
end
