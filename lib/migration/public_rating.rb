module Migration
  class PublicRating < Base
    self.table_name = 'votes'.freeze

    def self.migrate
      columns = %w(content_id user_id grade)

      select_columns = Array.new(columns) # Ensure it is a copy
      select_columns[2] = 'MAX(grade) as grade' # This, with the group clasule, ensure the unicity of user_id within content_id
      ratings_data = self.select(select_columns).where.not(grade: nil).group(:content_id, :user_id)

      values = ratings_data.pluck(*select_columns)

      save_records(columns, values, ::PublicRating)
    end
  end
end
