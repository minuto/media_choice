module Migration
  class Coordinate < Base
    def self.migrate
      columns = %w(latitude longitude found content_id)

      values = Migration::Coordinate.pluck(*columns)

      save_records(columns, values, ::Coordinate)
    end
  end
end
