module Migration
  class Contest < Base
    def self.migrate
      columns = %w(id start_date end_date parent_id created_at updated_at)

      contests_data = self.all.select columns

      values = contests_data.pluck(*columns)

      save_records(columns, values, ::Contest)
    end
  end
end
