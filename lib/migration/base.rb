module Migration
  class Base < ActiveRecord::Base
    self.abstract_class = true
    # The unless clause make possible to run unit tests without requiring the database
    # Trying to mock ActiveRecord::Base.estabilish_connection was not successful
    establish_connection :production_replica unless Rails.env == 'test'

    def self.save_records(columns, values, model_class, validate = true)
      transaction do
        begin
          model_class.import columns, values, validate: validate
        rescue ActiveRecord::RecordInvalid => e
          logger.debug 'The following records could not be migrated:'
          logger.debug "\t#{e.inspect}"
          logger.debug ''
          logger.debug '* * * * * * * * * * * * * * * *'
          logger.debug ''
          return false
        end
      end
      true
    end
  end
end
