module Migration
  class Configuration
    PRODUCTION_BUCKET_ADDRESS = 's3.amazonaws.com/production.mediachoice/'.freeze
    ACCESS_PROTOCOL = 'https'.freeze
  end
end
