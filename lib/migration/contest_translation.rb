module Migration
  class ContestTranslation < Base
    # The constest_logo.id will become the contest_translation id in order to keep the logo paths working
    def self.migrate
      contest_logo_columns = %w(id locale logo_file_name logo_content_type logo_file_size logo_updated_at)
      contest_logo_query_columns = contest_logo_columns.map { |col| "`contest_logos`.#{col}" }
      contest_translations_columns = %w(description contest_id created_at updated_at)
      contest_translations_query_columns = contest_translations_columns.map { |col| "`contest_translations`.#{col}" }
      contest_translations_columns << 'name'
      contest_translations_query_columns << '`contest_translations`.title AS name'

      query_columns = contest_logo_query_columns + contest_translations_query_columns
      columns = contest_logo_columns + contest_translations_columns

      data = self.joins('INNER JOIN contest_logos ON `contest_logos`.contest_id = `contest_translations`.contest_id AND `contest_logos`.locale = `contest_translations`.idiom')

      values = data.pluck(*query_columns)

      save_records(columns, values, ::ContestTranslation)

      cleanup_duplicated_contest_logos
    end

    # Cleaning up small contest logos. The legacy version has two sizes: highlights and covers. We only want the bigger ones, the highlights.
    #
    # Unit testing this part of the code would bring little benefit compared to the required work.
    #:nocov:
    def self.cleanup_duplicated_contest_logos
      ::ContestTranslation.group(:contest_id, :locale).count.each do |key, count|
        next if count <= 1
        duplicate_contest_logos = ::ContestTranslation.where(contest_id: key.first, locale: key.last)
        biggest = find_biggest_contest_logo(duplicate_contest_logos)
        duplicate_ids = duplicate_contest_logos.pluck(:id)
        duplicate_ids.delete(biggest.id)
        ::ContestTranslation.delete(duplicate_ids)
      end
    end

    def self.find_biggest_contest_logo(duplicate_contest_logos)
      biggest = duplicate_contest_logos.first
      biggest_width = Paperclip::Geometry.from_file(biggest.logo.url).width.to_i
      duplicate_contest_logos.each do |contest_logo|
        next if biggest.id == contest_logo.id
        logo_width = Paperclip::Geometry.from_file(contest_logo.logo.url).width.to_i
        if logo_width > biggest_width
          biggest = contest_logo
          biggest_width = logo_width
        end
      end
      biggest
    end
    #:nocov:
  end
end
