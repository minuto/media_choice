module Migration
  class ContestsSponsors < Base
    class << self
      def migrate
        columns = %w(contest_id sponsor_id)

        data = self.pluck(*columns)

        insert(data)
      end

      private

      # There is no model for HABTM association table, so we must build the SQL for fast insertion
      def insert(data)
        values = data.map { |pair| "(#{pair[0]},#{pair[1]})" }.join(',')

        query = "INSERT INTO #{table_name} (contest_id,sponsor_id) VALUES #{values}"

        ::ActiveRecord::Base.connection.execute(query)
      end
    end
  end
end
