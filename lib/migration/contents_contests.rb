module Migration
  class ContentsContests < Base
    def self.migrate
      columns = %w(content_id contest_id)
      values = self.pluck(*columns)
      self.insert(values)
    end

    # Because there is no model for this class, the query must be constructed
    def self.insert(data)
      values = data.map { |pair| "(#{pair[0]},#{pair[1]})" }.join(',')
      query = "INSERT INTO #{table_name} (content_id,contest_id) VALUES #{values}"
      ::ActiveRecord::Base.connection.execute(query)
    end
  end
end
