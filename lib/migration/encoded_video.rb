module Migration
  class EncodedVideo < Base
    self.table_name = 'encoded_contents'
    self.inheritance_column = :not_type
    # necessary to use #includes on the query
    belongs_to :content, class_name: 'Migration::Content'

    def self.migrate
      columns = %w(id content_id output_id created_at updated_at width height)
      query_columns = columns.map { |col| "`encoded_contents`.#{col}" }
      columns += %w(state video_file_name video_content_type video_file_size video_updated_at duration job_id)
      query_columns << "if(encoded_contents.state = 'ready', 'finished', '')"
      query_columns << '`encoded_contents`.output_file_name AS video_file_name'
      query_columns << '`encoded_contents`.output_content_type AS video_content_type'
      query_columns << '`encoded_contents`.output_file_size AS video_file_size'
      query_columns << '`encoded_contents`.output_updated_at AS video_updated_at'
      query_columns << '`encoded_contents`.duration_in_ms AS duration'
      # job_id stays on the old website's contents table
      query_columns << '`contents`.job_id'

      # includes is here so we can access Content#job_id.
      # contents with job_id nil are probably invalid. We ignore them here. If we ever need them, it's better to import them manually.
      data = self.where("encoded_contents.type = 'EncodedVideo' AND encoded_contents.state = 'ready'").joins(:content).where('contents.job_id IS NOT NULL').includes(:content)
      values = data.pluck(*query_columns)

      save_records(columns, values, ::EncodedVideo, false)
    end
  end
end
