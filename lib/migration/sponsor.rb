module Migration
  class Sponsor < Base
    STATIC_PATH_PREFIX = "#{Migration::Configuration::ACCESS_PROTOCOL}://#{Migration::Configuration::PRODUCTION_BUCKET_ADDRESS}sponsors/".freeze

    def self.migrate
      relevant_attributes_for_the_sponsors = Migration::Sponsor.connection.exec_query('select a1.id, a1.name, a1.logo_file_name, a1.logo_file_size, a1.logo_content_type, a1.logo_updated_at, a1.created_at, a1.updated_at, a1.url as "address", a2.total_count as "display_count" from sponsors a1, sponsor_view_counts a2 where a1.id = a2.sponsor_id')
      columns = relevant_attributes_for_the_sponsors.columns
      update_all_logo_files if save_records(columns, relevant_attributes_for_the_sponsors.map(&:values), ::Sponsor)
    end

    class << self
      def get_file_uri(sponsor_id)
        file_path = "#{STATIC_PATH_PREFIX}#{sponsor_id}/logos/original.png"
        uri = URI.parse(file_path)
        return uri if accessible_publicly? uri
        nil
      end

      def update_all_logo_files
        ::Sponsor.all.map do |sponsor|
          file = get_file_uri sponsor.id
          next if file.nil?
          sponsor.logo.reprocess! :thumb
        end
      end

      def accessible_publicly?(uri)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        response = http.request(Net::HTTP::Get.new(uri.request_uri))
        response.code == '200'
      end
    end
  end
end
