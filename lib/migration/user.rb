module Migration
  class User < Base
    PROFILE_ATTRIBUTES_TO_EXCLUDE = %w(commercialize_videos old_id facebook_token facebook_event_publisher_status authentication_token student unsubscribe_code).freeze
    COUNTRIES_TO_SKIP = ['Netherlands Antilles', 'Serbia and Montenegro', 'KV'].freeze
    COUNTRIES_TO_CORRECT = {'Bolivia' => 'BO', 'Brunei' => 'BN', 'Macedonia' => 'MK', 'Venezuela' => 'VE', 'Iran' => 'IR', 'British Virgin Islands' => 'VG', 'Reunion' => 'RE', 'Fiji Islands' => 'FJ', 'Moldova' => 'MD', 'Congo (Dem. Rep.)' => 'CD'}.freeze

    def self.migrate(user_batch)
      relevant_attributes_for_the_users_on_this_db = user_batch.first.attributes.except(*(user_batch.first.attributes.keys - ::User.column_names))
      columns = relevant_attributes_for_the_users_on_this_db.keys.map(&:to_sym)
      # The User model needs a password (not an encrypted password) in order to be created
      users_on_this_db = user_batch.select(relevant_attributes_for_the_users_on_this_db.keys.join(',')).pluck(*columns)
      create_profiles(user_batch) if save_records(columns, users_on_this_db, ::User, false)
    end

    def self.create_profiles(users_on_the_other_db)
      users_on_the_other_db = users_on_the_other_db.where('first_name REGEXP "[a-zA-Z]+" AND last_name REGEXP "[a-zA-Z]+" AND city REGEXP "[a-zA-Z]+" AND state REGEXP "[a-zA-Z]+" AND country REGEXP "[a-zA-Z]+" AND gender REGEXP "[FM]" AND occupation_id REGEXP "[0-9]+"')
      user_ids = users_on_the_other_db.pluck(:id)
      relevant_attributes_for_the_profiles = Migration::User.connection.exec_query("select id as user_id, first_name, last_name, birthdate, if(gender = 'M', 1, 2) as gender, state, city, country AS country_numeric_code, school, responsible_document, responsible_name, '00000-000' as zip_code, if(receive_mass_mail = 1, 'true', 'false') as receive_mass_email, phone, occupation_id as occupation, school_type_id as school_type from users where #{::User.send('sanitize_sql_for_assignment', ['id in (?)', user_ids])}").to_hash
      relevant_attributes_for_the_profiles.map! do |relevant_attributes_for_the_profile|
        treat_profile_attributes(relevant_attributes_for_the_profile)
      end
      columns = relevant_attributes_for_the_profiles.first.keys
      save_records(columns, relevant_attributes_for_the_profiles.map(&:values), ::Profile)
    end

    def self.migrate_leftover_profiles
      user_ids = ::Profile.pluck(:user_id)
      # Supposedly the date when the One Minute Festival was created. At least the year is correct.
      leftover_profiles = Migration::User.connection.exec_query("select id as user_id, first_name, last_name, COALESCE(birthdate, '01/01/1991') as birthdate, (CASE gender when 'M' then 1 when 'F' then 2 else NULL END) as gender, state, city, country AS country_numeric_code, school, responsible_document, responsible_name, '00000-000' as zip_code, if(receive_mass_mail = 1, 'true', 'false') as receive_mass_email, phone, occupation_id as occupation, school_type_id as school_type from users where #{::User.send('sanitize_sql_for_assignment', ['id NOT in (?)', user_ids])}").to_hash
      leftover_profiles.map! do |attributes|
        treat_profile_attributes(attributes)
      end
      columns = leftover_profiles.first.keys

      save_records(columns, leftover_profiles.map(&:values), ::Profile, false)
    end

    class << self
      def treat_profile_attributes(profile_attributes)
        occupation_id = profile_attributes['occupation']
        unless occupation_id.nil?
          occupation_id -= 1 if occupation_id > 8 # There's an entry missing (id = 8) on the old database Occupation table
          profile_attributes['occupation'] = occupation_id
        end

        profile_attributes = names_to_numeric_codes(profile_attributes)

        school_type_id = profile_attributes['school_type']
        unless school_type_id.nil?
          school_type_id -= 1 if !school_type_id.nil? && school_type_id > 4 # There's a duplicate entry for 'Faculdade' (id = 5) on the old database SchoolType table
          profile_attributes['school_type'] = school_type_id
        end
        profile_attributes
      end

      def names_to_numeric_codes(profile_attributes)
        country = profile_attributes['country_numeric_code']
        # Antilles was subdivided into a lot of countries. Just skip this one
        return profile_attributes if country&.empty? || COUNTRIES_TO_SKIP.include?(country)
        country = correct_country_names(country)
        # See https://github.com/jim/carmen/issues/214
        if country == 'PR' || country == 'Puerto Rico'
          profile_attributes['state'] = 'Puerto Rico'
          country_numeric_code = '840' # US numeric code
        else
          country_numeric_code = Carmen::Country.coded(country)&.numeric_code
          country_numeric_code = Carmen::Country.named(country)&.numeric_code if country_numeric_code.nil?
        end
        profile_attributes['country_numeric_code'] = country_numeric_code
        profile_attributes
      end

      def correct_country_names(country)
        COUNTRIES_TO_CORRECT[country].nil? ? country : COUNTRIES_TO_CORRECT[country]
      end
    end
  end
end
