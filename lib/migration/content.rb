module Migration
  class Content < Base
    DEFAULT_USER_ID = 6569 # default user created for the website.
    DEFAULT_NAME = 'Acervo Minuto'.freeze
    self.inheritance_column = :not_type

    def self.migrate
      # If the user that owns the Content does not have a first or last name, then "Acervo Minuto" should be used
      # Also, some boolean fields are returned as '0' or '1' from the query, so they are changed in here to avoid any errors upon insertion
      adult_filter = 'if(adult = 1, "true", "false") as adult'
      comment_filter = 'if(allow_comments = 1, "true", "false") as allow_comments'
      name_filter = "if(users.first_name is not null and users.last_name is not null, concat(users.first_name, \" \", users.last_name), \"#{DEFAULT_NAME}\") as director"
      relevant_attributes_for_the_contents = Migration::Content.connection.exec_query("select #{adult_filter}, #{comment_filter}, contents.id, contents.created_at, contents.updated_at, title, user_id, soundtrack, c2.total_count as view_count, zip_code, #{name_filter} from contents, users, content_view_counts as c2 where contents.user_id = users.id and contents.id = c2.content_id union (select #{adult_filter}, #{comment_filter}, contents.id, created_at, updated_at, title, #{DEFAULT_USER_ID}, soundtrack, view_count, zip_code, '#{DEFAULT_NAME}' as director from contents where user_id not in (select id from users))")
      columns = relevant_attributes_for_the_contents.columns
      # Some users do not have Profiles because of inconsistencies on the old database,
      # which would not allow for the content to be migrated.
      save_records(columns, relevant_attributes_for_the_contents.map(&:values), ::Content, false)
    end

    def self.migrate_original_medias
      columns = %w(content_id created_at updated_at media_file_name media_content_type media_file_size media_updated_at)
      query_columns = %w(id created_at updated_at input_file_name input_content_type input_file_size input_updated_at)
      values = Migration::Content.pluck(*query_columns)
      save_records(columns, values, OriginalMedia, false)
    end

    # There's no table for judge evaluations on the old database, therefore it must use Content to prepare the query
    def self.migrate_judge_evaluations
      columns = %w(content_id judge_rating created_at updated_at evaluation_state)
      query_columns = %w(id rating created_at updated_at)
      query_columns << "if(evaluation_state = 'pending_evaluation', 'pending', evaluation_state)"
      values = Migration::Content.pluck(*query_columns)
      save_records(columns, values, ::JudgeEvaluation)
    end
  end
end
