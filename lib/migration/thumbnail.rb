module Migration
  class Thumbnail < Base
    def self.migrate
      columns = %w(id encoded_content_id image_file_name image_content_type image_file_size image_updated_at created_at updated_at)
      query_columns = %w(id encoded_video_id image_file_name image_content_type image_file_size image_updated_at created_at updated_at)

      encoded_videos_id = ::EncodedVideo.pluck(:id)
      values = Migration::Thumbnail.where(encoded_video_id: encoded_videos_id).pluck(*query_columns)

      save_records(columns, values, ::Thumbnail)
    end
  end
end
