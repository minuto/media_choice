require 'sendgrid-ruby'

class MailSender
  include SendGrid

  attr_reader :mail

  def initialize(mass_mail)
    @mail = Mail.new
    @mail.from = Email.new(email: ::SendgridSettings.from)
    @mail.subject = mass_mail.subject
    @mail.contents = Content.new(type: 'text/plain', value: mass_mail.body)
    @mail.personalizations = personalization(mass_mail.locale)
  end

  def send
    response = api.client.mail._('send').post(request_body: @mail.to_json)

    unless response.status_code.starts_with?('2')
      raise response.status_code + ': ' + response.body
    end
  end

  private

  def personalization(locale)
    Personalization.new.tap do |personalization|
      User.mailing_list(locale).each do |user|
        personalization.to = Email.new(email: user.email)
      end
    end
  end

  def api
    @api ||= SendGrid::API.new(api_key: ::SendgridSettings.key, host: ::SendgridSettings.host)
  end
end
