require 'net/http'

class RequestMaker
  class << self
    def post(action, parameters)
      request = build_request(action, Net::HTTP::Post, parameters)
      make_request(request)
    end

    private

    def make_request(request)
      http = Net::HTTP.start(request.uri.hostname, request.uri.port)
      response = http.request(request)
      http.finish
      response
    end

    def build_request(action, http_verb, body)
      uri = URI("#{EvaluationSystemSettings.uri}/#{action}")
      request = http_verb.new uri
      request.add_field('Content-Type', 'application/json')
      body[:auth_token] = EvaluationSystemSettings.auth_token
      request.body = body.to_json
      request
    end
  end
end
