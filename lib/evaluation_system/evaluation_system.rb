class EvaluationSystem
  def self.request_evaluation(content)
    body = {video_evaluation_request: { external_id: content.id, duration_in_ms: content.encoded_contents.first.duration } }
    RequestMaker.post('video_evaluation_requests', body)
  end

  def self.content_json_for_evaluation(content)
    json_content = content.as_json(only: [:id, :title, :soundtrack, :evaluation_state, :judge_rating, :created_at],
                                   methods: [:description, :evaluation_state, :judge_rating],
                                   include: {
                                     user: {only: [:id, :email], methods: [:first_name, :last_name, :birthdate, :city, :locale]},
                                     contests: {only: [], methods: :name}
                                   })

    json_content[:output_url] = content.encoded_contents.first.video.url
    json_content[:tags] = []
    thumbnail = content.thumbnails.last
    json_content.merge!(thumb_small_url: thumbnail.url(:small),
                        thumb_url: thumbnail.url,
                        thumb_width: thumbnail.width,
                        thumb_height: thumbnail.height)
  end
end
