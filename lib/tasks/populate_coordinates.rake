namespace :populate do
  desc 'Create coordinates for every content that does not have it yet'
  task coordinates: :environment do
    Content.where.not(id: Coordinate.all.pluck(:content_id)).limit(1000).each(&:update_coordintates)
  end
end
