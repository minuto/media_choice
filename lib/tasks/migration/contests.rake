namespace :migrate do
  desc 'Migrate contests from the old website to the new one'
  task contests: :base do
    puts '> Migrating Contests'
    Migration::Contest.migrate
  end

  desc 'Migrate contest translations from the old website to the new one (WARNING: depends on the `contests` task)'
  task contest_translations: :base do
    puts '> Migrating Contest Translations'
    Migration::ContestTranslation.migrate
  end

  desc 'Migrate relations between contest and sponsors from the old website to the new one (WARNING: depends on the `contests` and `sponsors` tasks)'
  task contests_sponsors: :base do
    puts '> Migrating Contest Sponsors'
    Migration::ContestsSponsors.migrate
  end
end
