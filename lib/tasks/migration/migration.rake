namespace :migrate do
  task base: :environment do
    require 'migration/migration'
  end

  desc 'Migrate data from old website to the new'
  task all: [:users, :sponsors, :contests, :contest_translations, :contests_sponsors, :contents, :original_medias, :encoded_videos, :judge_evaluations, :contents_contests, :thumbnails, :public_ratings, :leftover_profiles, :coordinates, :reset_sequences]
end
