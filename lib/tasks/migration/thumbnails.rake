namespace :migrate do
  desc 'Migrate thumbnails from the old website to the new. This task requires the encoded videos to be already migrated.'
  task thumbnails: :base do
    puts '> Migrating Thumbnails'
    Migration::Thumbnail.migrate
  end
end
