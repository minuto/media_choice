namespace :migrate do
  desc 'Migrate users from the old website to the new'
  task users: :base do
    puts '> Migrating Users'
    Migration::User.in_batches { |user_batch| Migration::User.migrate(user_batch) }
  end

  desc 'Migrate profiles that violate validations from the old website to the new'
  task leftover_profiles: :base do
    puts '> Migrating Profiles That Violate Validations'
    Migration::User.migrate_leftover_profiles
  end
end
