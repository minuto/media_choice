namespace :migrate do
  desc 'Migrate Judge Evaluations for contents from the old website to the new. This task requires the contents to be already migrated'
  task judge_evaluations: :base do
    puts '> Migrating Judge Evaluations'
    Migration::Content.migrate_judge_evaluations
  end
end
