namespace :migrate do
  desc 'Migrate coordinates from the old website to the new. This task requires the contents to be already migrated'
  task coordinates: :base do
    puts '> Migrating Coordinates'
    Migration::Coordinate.migrate
  end
end
