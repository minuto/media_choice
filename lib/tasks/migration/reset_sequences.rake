namespace :migrate do
  desc 'Updates all sequences to MAX(id)+1. Assumes all our primary keys are the ids.'
  task reset_sequences: :base do
    puts '> Updating Sequences'
    ActiveRecord::Base.connection.tables.each do |table|
      ActiveRecord::Base.connection.reset_pk_sequence!(table)
    end
  end
end
