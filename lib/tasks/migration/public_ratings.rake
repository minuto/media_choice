namespace :migrate do
  desc 'Migrate votes from the old website to the new. This task requires the users and contents to be already migrated'
  task public_ratings: :base do
    puts '> Migrating PublicRatings'
    Migration::PublicRating.migrate
  end
end
