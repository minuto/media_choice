namespace :migrate do
  desc 'Migrate sponsors from the old website to the new'
  task sponsors: :base do
    puts '> Migrating Sponsors'
    Migration::Sponsor.migrate
  end
end
