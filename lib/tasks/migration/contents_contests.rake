namespace :migrate do
  desc 'Migrate relations between contents and contests from the old database. This task requires the contents and contests to be already migrated'
  task contents_contests: :base do
    puts '> Migrating Contents Contests'
    Migration::ContentsContests.migrate
  end
end
