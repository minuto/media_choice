namespace :migrate do
  desc 'Migrate contents from the old website to the new. This task requires the users to be already migrated'
  task contents: :base do
    puts '> Migrating Contents'
    Migration::Content.migrate
  end

  desc 'Migrate original medias from the old website to the new. This task requires the users and contents to be already migrated'
  task original_medias: :base do
    puts '> Migrating Original Medias'
    Migration::Content.migrate_original_medias
  end
end
