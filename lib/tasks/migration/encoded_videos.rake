namespace :migrate do
  desc 'Migrate encoded videos from the old website to the new. This task requires the contents to be already migrated'
  task encoded_videos: :base do
    puts '> Migrating Encoded Videos'
    Migration::EncodedVideo.migrate
  end
end
