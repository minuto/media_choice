desc 'Schedule periodic updates to view counters'
task update_view_counts: :environment do
  if ActiveRecord::Base.connection.tables.include?('delayed_jobs')
    ViewCounterJob.perform_later if Delayed::Job.where(queue: 'counter').count.zero?
  else
    warn 'NOTICE: You need to setup the database before running this task!'
  end
end
