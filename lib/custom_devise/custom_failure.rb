# :nocov:
# This class needs an entire environment from devise's helper to be tested and it is currently tested
# on devises's gem at
# * github.com/plataformatec/devise/blob/9a11586a724487dc98dddea0ab9c8afcc0e9439d/test/test/controller_helpers_test.rb#L68
# * https://github.com/plataformatec/devise/blob/4015488b90eb0ad7c87360b29908986d82966f28/test/failure_app_test.rb
class CustomFailure < Devise::FailureApp
  def redirect_url
    root_path locale: I18n.locale
  end

  def respond
    if http_auth?
      http_auth
    else
      flash[:warning] = I18n.t('needs_login')
      redirect_to redirect_url
    end
  end
end
# :nocov:
