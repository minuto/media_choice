# Media Choice Playbooks

Ansible playbooks for setting up media choice

## Requirements

**Local**

- Ansible
- ssh-askpass (allows ansible to ask for ssh and sudo passwords)
- Ansible Galaxy
  * rvm_io.ruby
  * ANXS.postgresql
  * All can be installed by running `ansible-galaxy install -r requirements.yml -p roles.lib`
- Create `.vault-pass` file that allows access to encrypted files
- Development
  * docker
  * vagrant

**Remote**

- Ubuntu 16.04
- Accessible through ssh
- python
- User `minuto` with sudo permission

## Quick start

In order to deploy to all hosts:

```bash
vagrant up --provider docker
ansible-playbook provision.yml -u minuto -kK
ansible-playbook configure.yml -u minuto -kK
ansible-playbook deploy.yml
ansible-playbook services.yml -u minuto -kK
```

If you want to deploy a specific host append `-l <HOST>` to the `ansible-playbook` commands. The `<HOST>` should be one of the names defined in the `inventory` file.

To check the hosts reachability run `ansible all -m ping -u minuto -k`.

In case of trouble don't forget about checking the **[Notes](README.md#notes)** at the end of the file!

## Adding a new installation instance

1. Add a new host to the `inventory` file
2. Create a new folder under `roles/apache/files/ssl` with the same host name. If you are going to run using a SSL certificate there is the place for the files. Remeber to use [`ansible-vault`](README.md#ansible-vault) to encrypt them
3. Create a new folder under `roles/setup_app/files/replacements` with the same host name. If you wish to do file replacements at installation time this is the place to put your files
4. Create a new folder under `roles/setup_app/files/patches` with the same host name. If you wish to apply custom code patches at installation time this is the place to put your files
5. If you wish, replace shared installation secrets: `roles/setup_app/files/google_maps_secrets.yml` and `roles/setup_app/files/sendgrid.yml`
6. Create a new folder under `host_vars` with the same name as the one in the `inventory`
7. Create the variable files accordingly to the samples. From inside the new folder run and [fill the variables](README.md#ansible-vault) with the correct information:

```bash
ansible-vault encrypt ../samples/aws_secrets.yml.sample --output aws_secrets.yml
ansible-vault encrypt ../samples/database_secrets.yml.sample --output database_secrets.yml
ansible-vault encrypt ../samples/evaluation_system_secrets.yml.sample --output evaluation_system_secrets.yml
cp ../samples/file_replace_list.yml.sample --output file_replace_list.yml
ansible-vault encrypt ../samples/mailer_secrets.yml.sample --output mailer_secrets.yml
cp ../samples/metadata.yml.sample --output metadata.yml
cp ../samples/patch_list.yml.sample --output patch_list.yml
ansible-vault encrypt ../samples/rails_secrets.yml.sample --output rails_secrets.yml
ansible-vault encrypt ../samples/recaptcha_secrets.yml.sample --output recaptcha_secrets.yml
cp ../samples/ssl.yml.sample --output ssl.yml # Make sure to set the same filenames as they are at roles/apache/files/ssl/<HOST>
ansible-vault encrypt ../samples/zencoder_secrets.yml.sample --output zencoder_secrets.yml
```

## Read More

### Deployment tasks

The deployment occurs on 3 steps:

1. **provisioning**: install required packages and make system wide configurations. Should be run once and at each update (multiple hosts can run concurrently)
1. **configure**: install required packages and make system wide configurations. Should be run once and at each update
2. **deployment**: Pulls new code, installs dependencies, runs DB migrations and compiles assets
3. **services**: Create, enable and start systemd units for the webserver and DelayedJob worker
4. **migration**: Configures the database and add a cronjob that imports data from the legacy website

### Ansible Vault

If you need to create a new configuration file that contains secrets (such as passwords or keys), it is **highly recommended** to use `ansible-vault`. __Every__ new configuration file used should be placed on the `config` directory on the project's root directory. They should be used as models to create the configuration files for deployment. Currently, we have configuration files for AWS, Mailer, Zencoder, ReCaptcha, Evaluation System, Google Maps and SendGrid API.

We use templates to create configuration files on the remote host. Templates are useful when the secrets are different for each host. To use them, we need to create a encrypted file that will have variables storing keys and credentials. Then, we use Jinja2 syntax to access them from the template. The file has to be created as a `host_var` inside the corresponding host directory and its template file must be listed on the `var/main.yml` file of the `setup_app` role. For all the files we have to encrypt, there are sample files to indicate their format on the `samples` directory under `host_vars`.

To create a new secret file to be used by a template, run:

`ansible-vault encrypt <path-to-MODEL-file> --output <path-to-secret-file>`

You will be asked to enter a password for encryption. If you want to use a single password to encrypt all secrets and not be asked to type it every time you want to edit them, create a `.vault-pass` file on the deploy's root directory containing a single line with the password. If you want to be prompted for the password each time, remove the `vault_password_file` option from `ansible.cfg`, and add `ask_vault_pass=yes`. Be aware that that will very likely break automatic provisioning and/or Vagrant.

To edit a secret configuration file, run:

`ansible-vault edit <path-to-secret-file>`

### How to obtain the keys for reCAPTCHA

Log in Google and access the reCAPTCHA service. Register a new domain and update the keys on `roles/setup_app/files/recaptcha_secrets.yml`.

### Generate self-signed SSL certificate

The certificates used for dev and staging server are valid for a finite amount of time. To generate them over again use:

```bash
openssl req -x509 -newkey rsa:4096 -keyout key.pem -days 365 -out media_choice.crt -nodes
```

## Notes

* You can check the password for the local docker environment `minuto` user in the `Dockerfile`
* After installing rvm, the first login may yield a warning message saying that PATH is not properly
set up. Running `rvm reset` should suffice to fix it
* You must run any `ansible-vault` command from the directory your `.vault-pass` file is stored
