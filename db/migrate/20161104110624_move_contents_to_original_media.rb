class MoveContentsToOriginalMedia < ActiveRecord::Migration[5.0]
  class Content < Content
    has_attached_file :video,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region
  end

  def self.up
    # These prevents https://github.com/rails/rails/issues/12330 (or something related to it) from happening
    Content.connection.schema_cache.clear!
    Content.reset_column_information

    Content.all.each do |c|
      # We have to substitute the string because the video is now within the scope of the migration, which causes paperclip to append the migration class name in the beginning of the url
      new_url = (c.video.url =~ /missing.png/) ? nil : (URI.parse(c.video.url.gsub('/move_contents_to_original_media', '')))
      OriginalMedia.create!(media: new_url, content: c)
    end
  end

  def self.down
    OriginalMedia.all.each { |om| om.content.update!(video: om.media) }
  end
end
