class ChangeEncodedContentHashData < ActiveRecord::Migration[5.0]
  class DefaultPathEncodedContent < ApplicationRecord
    self.table_name = 'encoded_contents'
    def self.inheritance_column
      nil
    end

    has_attached_file :video,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      hash_secret: 'EncodedContent',
      hash_data: 'encoded_videos/:attachment/:id',
      path: 'encoded_videos/:id/:hash.:extension'
    do_not_validate_attachment_file_type :video
  end

  class LegacyPathEncodedContent < ApplicationRecord
    self.table_name = 'encoded_contents'
    def self.inheritance_column
      nil
    end

    has_attached_file :video,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      hash_secret: 'EncodedContent',
      hash_data: 'encoded_videos/outputs/:id',
      path: 'encoded_videos/:id/:hash.:extension'
    do_not_validate_attachment_file_type :video
  end

  def up
    DefaultPathEncodedContent.all.each do |encoded_content|
      old_url = encoded_content.video.url =~ /missing.png/ ? nil : URI.parse(encoded_content.video.url.gsub('move_encoded_content_media_on_s3/default_path_', ''))
      next if old_url.nil?
      http = Net::HTTP.new(old_url.host, old_url.port)
      response = http.request(Net::HTTP::Get.new(old_url.request_uri))

      LegacyPathEncodedContent.find(encoded_content.id).update!(video: old_url) if response.code == '200'
    end
  end

  def down
    LegacyPathEncodedContent.all.each do |encoded_content|
      new_url = encoded_content.video.url =~ /missing.png/ ? nil : URI.parse(encoded_content.video.url.gsub('move_encoded_content_media_on_s3/legacy_path_', ''))
      next if new_url.nil?
      http = Net::HTTP.new(new_url.host, new_url.port)
      response = http.request(Net::HTTP::Get.new(new_url.request_uri))

      DefaultPathEncodedContent.find(encoded_content.id).update!(video: new_url) if response.code == '200'
    end
  end
end
