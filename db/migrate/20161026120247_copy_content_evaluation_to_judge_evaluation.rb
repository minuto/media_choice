class CopyContentEvaluationToJudgeEvaluation < ActiveRecord::Migration[5.0]
  def self.up
    Content.all.each do |content|
      # We cannot override delegations, so we have to read the attribute directly from the database
      JudgeEvaluation.create!(judge_rating: content.read_attribute(:judge_rating), evaluation_state: content.read_attribute(:evaluation_state), content: content)
    end
  end

  def self.down
    JudgeEvaluation.all.each do |evaluation|
      evaluation.content.update!(judge_rating: evaluation.judge_rating, evaluation_state: evaluation.evaluation_state)
    end
  end
end
