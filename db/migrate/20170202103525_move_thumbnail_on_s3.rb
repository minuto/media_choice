class MoveThumbnailOnS3 < ActiveRecord::Migration[5.0]
  class DefaultPathThumbnail < Thumbnail
    has_attached_file :image,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { small: '94x52' }
  end

  class LegacyPathThumbnail < Thumbnail
    has_attached_file :image,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { small: '94x52' },
      path: 'thumbnails/:id/:style.:extension'
  end

  def up
    DefaultPathThumbnail.all.each do |thumbnail|
      old_url = (thumbnail.image.url(:original) =~ /missing.png/) ? nil : (URI.parse(thumbnail.image.url(:original).gsub('move_thumbnail_on_s3/default_path_', '')))
      unless old_url.nil?
        http = Net::HTTP.new(old_url.host, old_url.port)
        http.use_ssl = false
        response = http.request(Net::HTTP::Get.new(old_url.request_uri))

        LegacyPathThumbnail.find(thumbnail.id).update!(image: old_url) if response.code == '200'
      end
    end
  end

  def down
    LegacyPathThumbnail.all.each do |thumbnail|
      new_url = (thumbnail.image.url(:original) =~ /missing.png/) ? nil : (URI.parse(thumbnail.image.url(:original).gsub('move_thumbnail_on_s3/legacy_path_', '')))
      unless new_url.nil?
        http = Net::HTTP.new(new_url.host, new_url.port)
        http.use_ssl = false
        response = http.request(Net::HTTP::Get.new(new_url.request_uri))

        DefaultPathThumbnail.find(thumbnail.id).update!(image: new_url) if response.code == '200'
      end
    end
  end
end
