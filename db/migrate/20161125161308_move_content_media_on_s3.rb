class MoveContentMediaOnS3 < ActiveRecord::Migration[5.0]
  class DefaultPathOriginalMedia < OriginalMedia
    has_attached_file :media,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region
  end

  class LegacyPathOriginalMedia < OriginalMedia
    has_attached_file :media,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      path: ':class/:id/:filename' # legacy path from festivaldominuto.com.br
  end

  def up
    DefaultPathOriginalMedia.all.each do |original_media|
      old_url = original_media.media.url(:original) =~ /missing.png/ ? nil : URI.parse(original_media.media.url(:original).gsub('move_original_medias_on_s3/default_path_', ''))
      next if old_url.nil?
      http = Net::HTTP.new(old_url.host, old_url.port)
      response = http.request(Net::HTTP::Get.new(old_url.request_uri))

      LegacyPathOriginalMedia.find(original_media.id).update!(media: old_url) if response.code == '200'
    end
  end

  def down
    LegacyPathOriginalMedia.all.each do |original_media|
      new_url = original_media.media.url(:original) =~ /missing.png/ ? nil : URI.parse(original_media.media.url(:original).gsub('move_original_medias_on_s3/legacy_path_', ''))
      next if new_url.nil?
      http = Net::HTTP.new(new_url.host, new_url.port)
      response = http.request(Net::HTTP::Get.new(new_url.request_uri))

      DefaultPathOriginalMedia.find(original_media.id).update!(media: new_url) if response.code == '200'
    end
  end
end
