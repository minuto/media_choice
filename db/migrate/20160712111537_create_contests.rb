class CreateContests < ActiveRecord::Migration[5.0]
  def change
    create_table :contests do |t|
      t.datetime :start_date, null: false
      t.datetime :end_date

      t.timestamps
    end
  end
end
