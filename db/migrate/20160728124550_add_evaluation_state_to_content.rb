class AddEvaluationStateToContent < ActiveRecord::Migration[5.0]
  def change
    add_column :contents, :evaluation_state, :string, null: false, default: 'pending'
  end
end
