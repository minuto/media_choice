class AddEncodedContent < ActiveRecord::Migration[5.0]
  def change
    create_table :encoded_contents do |t|
      t.integer :content_id, null: false
      t.integer :job_id, null: false
      t.integer :output_id
      t.integer :file_size
      t.integer :width
      t.integer :height
      t.integer :duration
      t.string :state
      t.attachment :video
      t.timestamps
    end
  end
end
