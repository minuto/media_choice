class RemoveDescriptionFromContent < ActiveRecord::Migration[5.0]
  def change
    remove_column :contents, :description
  end
end
