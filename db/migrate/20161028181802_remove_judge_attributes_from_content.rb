class RemoveJudgeAttributesFromContent < ActiveRecord::Migration[5.0]
  def change
    remove_column :contents, :judge_rating, :float
    remove_column :contents, :evaluation_state, :string
  end
end
