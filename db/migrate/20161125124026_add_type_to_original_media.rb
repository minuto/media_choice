class AddTypeToOriginalMedia < ActiveRecord::Migration[5.0]
  def change
    add_column :original_media, :type, :string, default: 'Video'
  end
end
