class CreateViewCounter < ActiveRecord::Migration[5.0]
  def up
    create_table :view_counters do |t|
      t.string :viewed_model
      t.integer :model_id
    end

    drop_table :sponsor_views
  end

  def down
    create_table :sponsor_views do |t|
      t.integer :sponsor_id
    end

    drop_table :view_counters
  end
end
