class AddMailAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :mail_attachments do |t|
      t.attachment :file
      t.references :mass_mail, foreign_key: true

      t.timestamps
    end
  end
end
