class ChangeLinkToAddress < ActiveRecord::Migration[5.0]
  def change
    rename_column :sponsors, :link, :address
  end
end
