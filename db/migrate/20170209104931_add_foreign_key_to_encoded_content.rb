class AddForeignKeyToEncodedContent < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :encoded_contents, :contents
  end
end
