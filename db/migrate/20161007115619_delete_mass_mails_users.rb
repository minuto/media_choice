class DeleteMassMailsUsers < ActiveRecord::Migration[5.0]
  def change
    drop_table :mass_mails_users
  end
end
