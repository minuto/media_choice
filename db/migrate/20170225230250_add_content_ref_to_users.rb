class AddContentRefToUsers < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :contents, :users
    add_index :contents, :user_id
  end
end
