class CreateThumbnails < ActiveRecord::Migration[5.0]
  def change
    create_table :thumbnails do |t|
      t.attachment :image
      t.references :encoded_content, foreign_key: true

      t.timestamps
    end
  end
end
