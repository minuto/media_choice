class CreatePlaylists < ActiveRecord::Migration[5.0]
  def change
    create_table :playlists do |t|
      t.string :name, null: false
      t.string :locale, null: false, default: 'pt-BR'
      t.timestamps
    end
  end
end
