class CreateContents < ActiveRecord::Migration[5.0]
  def change
    create_table :contents do |t|
      t.string :title, null: false
      t.string :description
      t.integer :user_id, null: false
      t.boolean :adult, default: false, null: false
      t.integer :rating
      t.string :soundtrack, null: false
      t.integer :view_count, default: 0
      t.boolean :deleted, default: false, null: false
      t.string :zip_code
      t.string :director, null: false
      t.string :co_director
      t.string :team
      t.boolean :allow_comments, default: true

      t.timestamps
    end
  end
end
