class AddSuperContestIdToContest < ActiveRecord::Migration[5.0]
  def change
    add_column :contests, :parent_id, :integer
  end
end
