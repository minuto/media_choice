class DeleteMailAttachment < ActiveRecord::Migration[5.0]
  def change
    drop_table :mail_attachments
  end
end
