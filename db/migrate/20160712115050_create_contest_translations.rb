class CreateContestTranslations < ActiveRecord::Migration[5.0]
  def change
    create_table :contest_translations do |t|
      t.string :locale, null: false
      t.string :name, null: false
      t.string :description
      t.integer :contest_id, null: false
      t.timestamps
    end
  end
end
