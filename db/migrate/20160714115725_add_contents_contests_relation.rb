class AddContentsContestsRelation < ActiveRecord::Migration[5.0]
  def change
    create_table :contents_contests do |t|
      t.integer :content_id, null: false
      t.integer :contest_id, null: false
    end
  end
end
