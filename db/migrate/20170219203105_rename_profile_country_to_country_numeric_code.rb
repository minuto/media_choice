class RenameProfileCountryToCountryNumericCode < ActiveRecord::Migration[5.0]
  def change
    rename_column :profiles, :country, :country_numeric_code
  end
end
