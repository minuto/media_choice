class CreatePlaylistTranslation < ActiveRecord::Migration[5.0]
  def change
    create_table :playlist_translations do |t|
      t.string :locale, null: false, default: 'pt-BR'
      t.string :name, null: false
      t.references :playlist, foreign_key: true
      t.timestamps
    end

    remove_column :playlists, :locale
    remove_column :playlists, :name
  end
end
