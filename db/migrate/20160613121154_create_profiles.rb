class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.string :gender
      t.string :state
      t.string :city
      t.string :country
      t.string :school
      t.string :responsible_document
      t.string :responsible_name
      t.boolean :receive_mass_email, default: true
      t.string :phone
      t.integer :user_id, null: false
      t.string :address
      t.string :zip_code
      t.string :occupation
      t.string :school_type

      t.timestamps
    end
  end
end
