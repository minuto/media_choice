class MoveEncodedContentThumbnailsToModel < ActiveRecord::Migration[5.0]
  class EncodedContent < EncodedContent
    has_attached_file :thumbnail,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { small: '94x52' }
  end

  def self.up
    EncodedContent.all.each do |ec|
      new_url = (ec.thumbnail.url =~ /missing.png/) ? nil : (URI.parse(ec.thumbnail.url.gsub('/move_encoded_content_thumbnails_to_model', '')))
      Thumbnail.create!(image: new_url, encoded_content: ec)
    end
  end

  def self.down
    Thumbnail.all.each { |t| t.encoded_content.update!(thumbnail: t.image) }
  end
end
