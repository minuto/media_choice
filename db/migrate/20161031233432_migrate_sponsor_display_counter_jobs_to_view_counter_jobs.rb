class MigrateSponsorDisplayCounterJobsToViewCounterJobs < ActiveRecord::Migration[5.0]
  def up
    sponsor_job = Delayed::Job.where(queue: 'sponsor').first
    sponsor_job.update(handler: sponsor_job.handler.gsub!('SponsorDisplayCounterJob', 'ViewCounterJob').gsub!('sponsor', 'counter'), queue: 'counter') unless sponsor_job.nil?
  end

  def down
    view_counter_job = Delayed::Job.where(queue: 'counter').first
    view_counter_job.update(handler: view_counter_job.handler.gsub!('ViewCounterJob', 'SponsorDisplayCounterJob').gsub!('counter', 'sponsor'), queue: 'sponsor') unless view_counter_job.nil?
  end
end
