class MoveContestLogoOnS3 < ActiveRecord::Migration[5.0]
  class DefaultPathContestTranslation < ContestTranslation
    has_attached_file :logo,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { thumb: '270x90>' }
  end

  class LegacyPathContestTranslation < ContestTranslation
    has_attached_file :logo,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { thumb: '270x90>' },
      path: 'contest_logos/:id/:attachment/:style.:extension'
  end

  def up
    DefaultPathContestTranslation.all.each do |contest_translation|
      old_url = (contest_translation.logo.url(:original) =~ /missing.png/) ? nil : (URI.parse(contest_translation.logo.url(:original).gsub('move_contest_logo_on_s3/default_path_', '')))
      unless old_url.nil?
        http = Net::HTTP.new(old_url.host, old_url.port)
        http.use_ssl = false
        response = http.request(Net::HTTP::Get.new(old_url.request_uri))

        LegacyPathContestTranslation.find(contest_translation.id).update!(logo: old_url) if response.code == '200'
      end
    end
  end

  def down
    LegacyPathContestTranslation.all.each do |contest_translation|
      new_url = (contest_translation.logo.url(:original) =~ /missing.png/) ? nil : (URI.parse(contest_translation.logo.url(:original).gsub('move_contest_logo_on_s3/legacy_path_', '')))
      unless new_url.nil?
        http = Net::HTTP.new(new_url.host, new_url.port)
        http.use_ssl = false
        response = http.request(Net::HTTP::Get.new(new_url.request_uri))

        DefaultPathContestTranslation.find(contest_translation.id).update!(logo: new_url) if response.code == '200'
      end
    end
  end
end
