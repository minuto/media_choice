class AddAttachmentLogoToContestTranslations < ActiveRecord::Migration
  def self.up
    change_table :contest_translations do |t|
      t.attachment :logo
    end
  end

  def self.down
    remove_attachment :contest_translations, :logo
  end
end
