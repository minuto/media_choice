class ChangeRatingToPublicRating < ActiveRecord::Migration[5.0]
  def change
    change_table :ratings do |t|
      t.rename :rate, :grade
    end
    rename_table :ratings, :public_ratings
    rename_column :contents, :rating, :judge_rating
  end
end
