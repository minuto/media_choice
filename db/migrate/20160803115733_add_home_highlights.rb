class AddHomeHighlights < ActiveRecord::Migration[5.0]
  def change
    create_table :home_highlights do |t|
      t.string :link, null: false
      t.attachment :banner
      t.boolean :visible, null: false, default: true

      t.timestamps
    end
  end
end
