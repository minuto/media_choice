class CreateMassMail < ActiveRecord::Migration[5.0]
  def change
    create_table :mass_mails do |t|
      t.string :subject
      t.text :body
      t.string :status, null: false, default: 'pending'
      t.string :locale, null: false, default: 'pt-BR'
      t.timestamps
    end

    create_table :mass_mails_users do |t|
      t.integer :mass_mail_id, null: false
      t.integer :user_id, null: false
    end
  end
end
