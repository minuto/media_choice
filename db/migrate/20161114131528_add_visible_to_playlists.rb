class AddVisibleToPlaylists < ActiveRecord::Migration[5.0]
  def change
    add_column :playlists, :visible, :boolean, default: true, null: false
  end
end
