class AddSponsor < ActiveRecord::Migration[5.0]
  def change
    create_table :sponsors do |t|
      t.attachment :logo
      t.string :link
      t.integer :display_count, null: false, default: 0
      t.timestamps
    end
  end
end
