class AddCoordinatesToContent < ActiveRecord::Migration[5.0]
  def change
    create_table :coordinates do |t|
      t.float :latitude
      t.float :longitude
      t.boolean :found, null: false, default: false
      t.belongs_to :content, index: true
    end
    add_foreign_key :coordinates, :contents
  end
end
