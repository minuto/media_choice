class AddRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings  do |t|
      t.integer :user_id, null: false
      t.integer :content_id, null: false
      t.float :rate, null: false
      t.timestamps
    end
  end
end
