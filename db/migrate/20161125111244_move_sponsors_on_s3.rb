class MoveSponsorsOnS3 < ActiveRecord::Migration[5.0]
  class DefaultPathSponsor < Sponsor
    has_attached_file :logo,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { thumb: 'x100' }
  end

  class LegacyPathSponsor < Sponsor
    has_attached_file :logo,
      storage: :s3,
      s3_credentials: AWSSettings.to_h,
      s3_region: AWSSettings.aws_region,
      styles: { thumb: 'x100' },
      path: ':class/:id/:attachment/:style.:extension' # legacy path from festivaldominuto.com.br
  end

  def up
    DefaultPathSponsor.all.each do |sponsor|
      old_url = (sponsor.logo.url(:original) =~ /missing.png/) ? nil : (URI.parse(sponsor.logo.url(:original).gsub('move_sponsors_on_s3/default_path_', '')))
      unless old_url.nil?
        http = Net::HTTP.new(old_url.host, old_url.port)
        http.use_ssl = false
        response = http.request(Net::HTTP::Get.new(old_url.request_uri))

        LegacyPathSponsor.find(sponsor.id).update!(logo: old_url) if response.code == '200'
      end
    end
  end

  def down
    LegacyPathSponsor.all.each do |sponsor|
      new_url = (sponsor.logo.url(:original) =~ /missing.png/) ? nil : (URI.parse(sponsor.logo.url(:original).gsub('move_sponsors_on_s3/legacy_path_', '')))
      unless new_url.nil?
        http = Net::HTTP.new(new_url.host, new_url.port)
        http.use_ssl = false
        response = http.request(Net::HTTP::Get.new(new_url.request_uri))

        DefaultPathSponsor.find(sponsor.id).update!(logo: new_url) if response.code == '200'
      end
    end
  end
end
