class AddSponsorContestRelation < ActiveRecord::Migration[5.0]
  def change
    create_table :contests_sponsors do |t|
      t.integer :contest_id, null: false
      t.integer :sponsor_id, null: false
    end
  end
end
