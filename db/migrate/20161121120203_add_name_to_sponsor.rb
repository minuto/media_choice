class AddNameToSponsor < ActiveRecord::Migration[5.0]
  def change
    add_column :sponsors, :name, :string, null: false, default: ''
  end
end
