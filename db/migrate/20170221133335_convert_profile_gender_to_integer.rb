class ConvertProfileGenderToInteger < ActiveRecord::Migration[5.0]
  class LegacyProfile < ApplicationRecord
    belongs_to :user

    validates :terms_of_service, acceptance: true
    validates :first_name, :last_name, :country_numeric_code, :state, :city,
      :zip_code, :birthdate, :gender, :occupation, presence: true
    validates :responsible_name, :responsible_document, presence: true, if: :underaged?
    validates :gender, format: { with: /\A(1|2)\z/, message: I18n.t('invalid_gender') }

    # When migrating data from the old website, some users may not have registered birthdates.
    # That would break the migration for we enforce a 'null: false' on this attribute on the database.
    # This callback is a workaround for those cases. We simply use a default value of 12 years. It won't affect new users because we validate the birthdate field on the form.
    # Someday this may be removed.
    before_validation do
      self.birthdate = (Time.zone.today - 12.years) if birthdate.blank?
    end

    self.table_name = 'profiles'

    def adult?
      ((Time.zone.today - birthdate) / 365).to_i >= 18
    end

    def underaged?
      !adult?
    end

    def full_name
      "#{first_name} #{last_name}"
    end
  end

  def up
    LegacyProfile.all.each do |profile|
      gender = ''

      I18n.available_locales.each do |locale|
        localized_male = I18n.t(:genders, locale: locale)[1]
        localized_female = I18n.t(:genders, locale: locale)[2]

        if profile.gender == localized_male
          gender = '1'
          break
        elsif profile.gender == localized_female
          gender = '2'
          break
        end
      end

      profile.update!(gender: gender)
    end

    # PG requires explicit conversion
    # This might not work on other databases
    change_column :profiles, :gender, 'integer USING CAST(gender AS integer)'
  end

  # Not a perfect down as it loses the user original translation.
  #
  # The option to not lose such information would be create a backup table which
  # looks not worth of the database mess it will start.
  #
  # Also, if the translation key changes it will become broken
  # https://gitlab.com/minuto/media_choice/issues/69 is a candidate for that.
  def down
    change_column :profiles, :gender, :string

    LegacyProfile.reset_column_information
    LegacyProfile.all.each { |profile| profile.update!(gender: (profile.gender == 1 ? I18n.t(:male) : I18n.t(:female))) }
  end
end
