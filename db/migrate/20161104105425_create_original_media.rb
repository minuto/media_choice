class CreateOriginalMedia < ActiveRecord::Migration[5.0]
  def change
    create_table :original_media do |t|
      t.attachment :media
      t.references :content, foreign_key: true

      t.timestamps
    end
  end
end
