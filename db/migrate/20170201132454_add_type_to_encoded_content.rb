class AddTypeToEncodedContent < ActiveRecord::Migration[5.0]
  def change
    add_column :encoded_contents, :type, :string, default: 'EncodedVideo'
  end
end
