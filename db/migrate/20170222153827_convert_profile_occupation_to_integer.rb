class ConvertProfileOccupationToInteger < ActiveRecord::Migration[5.0]
  class LegacyProfile < ApplicationRecord
    belongs_to :user

    validates :terms_of_service, acceptance: true
    validates :first_name, :last_name, :country_numeric_code, :state, :city,
      :zip_code, :birthdate, :gender, :occupation, presence: true
    validates :responsible_name, :responsible_document, presence: true, if: :underaged?
    validates :gender, format: { with: /\A(1|2)\z/, message: I18n.t('invalid_gender') }
    validates :occupation, format: { with: /\A([1-9]|1[0-5])\z/, message: I18n.t('invalid_occupation') }

    # When migrating data from the old website, some users may not have registered birthdates.
    # That would break the migration for we enforce a 'null: false' on this attribute on the database.
    # This callback is a workaround for those cases. We simply use a default value of 12 years. It won't affect new users because we validate the birthdate field on the form.
    # Someday this may be removed.
    before_validation do
      self.birthdate = (Time.zone.today - 12.years) if birthdate.blank?
    end

    self.table_name = 'profiles'

    def adult?
      ((Time.zone.today - birthdate) / 365).to_i >= 18
    end

    def underaged?
      !adult?
    end

    def full_name
      "#{first_name} #{last_name}"
    end
  end

  def up
    occupations = {}
    I18n.available_locales.each { |locale| occupations.merge!(I18n.backend.send(:translations)[locale][:occupations].invert) }
    LegacyProfile.all.each { |profile| profile.update!(occupation: occupations[profile.occupation]) }

    # PG requires explicit conversion
    # This might not work on other databases
    change_column :profiles, :occupation, 'integer USING CAST(occupation AS integer)'
  end

  # Not a perfect down as it loses the user original translation.
  #
  # The option to not lose such information would be create a backup table which
  # looks not worth of the database mess it will start.
  #
  # Also, if the translation key changes it will become broken
  # https://gitlab.com/minuto/media_choice/issues/69 is a candidate for that.
  def down
    change_column :profiles, :occupation, :string

    occupations = I18n.backend.send(:translations)[I18n.default_locale][:occupations]
    LegacyProfile.reset_column_information
    LegacyProfile.all.each { |profile| profile.update!(occupation: occupations[profile.occupation.to_i]) }
  end
end
