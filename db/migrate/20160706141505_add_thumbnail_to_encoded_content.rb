class AddThumbnailToEncodedContent < ActiveRecord::Migration[5.0]
  def self.up
    change_table :encoded_contents do |t|
      t.attachment :thumbnail
    end
  end

  def self.down
    remove_attachment :encoded_contents, :thumbnail
  end
end
