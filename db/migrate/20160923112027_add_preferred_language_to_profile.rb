class AddPreferredLanguageToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :preferred_language, :string, null: false, default: 'pt-BR'
  end
end
