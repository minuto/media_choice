class AddIndexToViewCounters < ActiveRecord::Migration[5.0]
  def change
    add_index :view_counters, :viewed_model
    add_index :view_counters, :model_id
  end
end
