class ChangeCountryNamesToNumericCodes < ActiveRecord::Migration[5.0]
  class LegacyProfile < ApplicationRecord
    belongs_to :user

    validates :terms_of_service, acceptance: true
    validates :first_name, :last_name, :country_numeric_code, :state, :city,
      :zip_code, :birthdate, :gender, :occupation, presence: true
    validates :responsible_name, :responsible_document, presence: true, if: :underaged?

    # When migrating data from the old website, some users may not have registered birthdates.
    # That would break the migration for we enforce a 'null: false' on this attribute on the database.
    # This callback is a workaround for those cases. We simply use a default value of 12 years. It won't affect new users because we validate the birthdate field on the form.
    # Someday this may be removed.
    before_validation do
      self.birthdate = (Time.zone.today - 12.years) if birthdate.blank?
    end

    self.table_name = 'profiles'

    def adult?
      ((Time.zone.today - birthdate) / 365).to_i >= 18
    end

    def underaged?
      !adult?
    end

    def full_name
      "#{first_name} #{last_name}"
    end
  end

  def up
    LegacyProfile.all.each do |profile|
      next if profile.country_numeric_code.nil?
      country_numeric_code = Carmen::Country.named(profile.country_numeric_code)&.numeric_code
      if country_numeric_code.nil?
        # Carmen won't find the correct country if it is on a different locale. We have to check for each locale we curretly support. Those are: pt-BR and en.
        # The default locale is pt-BR. But Carmen only recognizes pt. So its locale fallbacks to en.
        Carmen.i18n_backend.locale = :pt
        country_numeric_code = Carmen::Country.named(profile.country_numeric_code)&.numeric_code
        Carmen.i18n_backend.locale = :en
      end
      profile.update!(country_numeric_code: country_numeric_code)
    end
  end

  def down
    LegacyProfile.all.each do |profile|
      next if profile.country_numeric_code.nil?
      preferred_language = profile.preferred_language == 'pt-BR' ? :pt : :en
      Carmen.i18n_backend.locale = preferred_language
      country_name = Carmen::Country.coded(profile.country_numeric_code)&.name
      profile.update!(country_numeric_code: country_name)
      # Resets Carmen to its default locale
      Carmen.i18n_backend.locale = :en
    end
  end
end
