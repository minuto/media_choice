class ConvertProfileSchoolTypeToInteger < ActiveRecord::Migration[5.0]
  def up
    school_types = {}
    I18n.available_locales.each { |locale| school_types.merge!(I18n.backend.send(:translations)[locale][:school_types].invert) }
    Profile.all.each { |profile| profile.update!(school_type: school_types[profile.school_type]) }

    # PG requires explicit conversion
    # This might not work on other databases
    change_column :profiles, :school_type, 'integer USING CAST(school_type AS integer)'
  end

  # Not a perfect down as it loses the user original translation.
  #
  # The option to not lose such information would be create a backup table which
  # looks not worth of the database mess it will start.
  #
  # Also, if the translation key changes it will become broken
  # https://gitlab.com/minuto/media_choice/issues/69 is a candidate for that.
  def down
    change_column :profiles, :school_type, :string

    school_types = I18n.backend.send(:translations)[I18n.default_locale][:school_types]
    Profile.reset_column_information
    Profile.all.each { |profile| profile.update!(school_type: school_types[profile.school_type.to_i]) }
  end
end
