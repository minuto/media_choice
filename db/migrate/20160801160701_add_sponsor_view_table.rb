class AddSponsorViewTable < ActiveRecord::Migration[5.0]
  def change
    create_table :sponsor_views do |t|
      t.integer :sponsor_id, null: false
    end
  end
end
