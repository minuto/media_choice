class CreateJudgeEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :judge_evaluations do |t|
      t.float :judge_rating
      t.string :evaluation_state
      t.references :content, foreign_key: true

      t.timestamps
    end
  end
end
