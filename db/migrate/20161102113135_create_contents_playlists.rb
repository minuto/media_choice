class CreateContentsPlaylists < ActiveRecord::Migration[5.0]
  def change
    create_table :contents_playlists do |t|
      t.integer :content_id, null: false
      t.integer :playlist_id, null: false
    end
  end
end
