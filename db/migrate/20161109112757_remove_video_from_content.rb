class RemoveVideoFromContent < ActiveRecord::Migration[5.0]
  def change
    remove_attachment :contents, :video
  end
end
