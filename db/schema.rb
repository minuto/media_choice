# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170225230250) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contents", force: :cascade do |t|
    t.string   "title",                          null: false
    t.integer  "user_id",                        null: false
    t.boolean  "adult",          default: false, null: false
    t.string   "soundtrack",                     null: false
    t.integer  "view_count",     default: 0
    t.boolean  "deleted",        default: false, null: false
    t.string   "zip_code"
    t.string   "director",                       null: false
    t.string   "co_director"
    t.string   "team"
    t.boolean  "allow_comments", default: true
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["user_id"], name: "index_contents_on_user_id", using: :btree
  end

  create_table "contents_contests", force: :cascade do |t|
    t.integer "content_id", null: false
    t.integer "contest_id", null: false
  end

  create_table "contents_playlists", force: :cascade do |t|
    t.integer "content_id",  null: false
    t.integer "playlist_id", null: false
  end

  create_table "contest_translations", force: :cascade do |t|
    t.string   "locale",            null: false
    t.string   "name",              null: false
    t.string   "description"
    t.integer  "contest_id",        null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "contests", force: :cascade do |t|
    t.datetime "start_date", null: false
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
  end

  create_table "contests_sponsors", force: :cascade do |t|
    t.integer "contest_id", null: false
    t.integer "sponsor_id", null: false
  end

  create_table "coordinates", force: :cascade do |t|
    t.float   "latitude"
    t.float   "longitude"
    t.boolean "found",      default: false, null: false
    t.integer "content_id"
    t.index ["content_id"], name: "index_coordinates_on_content_id", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "encoded_contents", force: :cascade do |t|
    t.integer  "content_id",                                  null: false
    t.integer  "job_id",                                      null: false
    t.integer  "output_id"
    t.integer  "file_size"
    t.integer  "width"
    t.integer  "height"
    t.integer  "duration"
    t.string   "state"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "type",               default: "EncodedVideo"
  end

  create_table "home_highlights", force: :cascade do |t|
    t.string   "link",                               null: false
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.boolean  "visible",             default: true, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "judge_evaluations", force: :cascade do |t|
    t.float    "judge_rating"
    t.string   "evaluation_state"
    t.integer  "content_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["content_id"], name: "index_judge_evaluations_on_content_id", using: :btree
  end

  create_table "mass_mails", force: :cascade do |t|
    t.string   "subject"
    t.text     "body"
    t.string   "status",     default: "pending", null: false
    t.string   "locale",     default: "pt-BR",   null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "original_media", force: :cascade do |t|
    t.string   "media_file_name"
    t.string   "media_content_type"
    t.integer  "media_file_size"
    t.datetime "media_updated_at"
    t.integer  "content_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "type",               default: "Video"
    t.index ["content_id"], name: "index_original_media_on_content_id", using: :btree
  end

  create_table "playlist_translations", force: :cascade do |t|
    t.string   "locale",      default: "pt-BR", null: false
    t.string   "name",                          null: false
    t.integer  "playlist_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["playlist_id"], name: "index_playlist_translations_on_playlist_id", using: :btree
  end

  create_table "playlists", force: :cascade do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "visible",    default: true, null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birthdate",                              null: false
    t.integer  "gender"
    t.string   "state"
    t.string   "city"
    t.string   "country_numeric_code"
    t.string   "school"
    t.string   "responsible_document"
    t.string   "responsible_name"
    t.boolean  "receive_mass_email",   default: true
    t.string   "phone"
    t.integer  "user_id",                                null: false
    t.string   "address"
    t.string   "zip_code"
    t.integer  "occupation"
    t.integer  "school_type"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "preferred_language",   default: "pt-BR", null: false
  end

  create_table "public_ratings", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "content_id", null: false
    t.float    "grade",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sponsors", force: :cascade do |t|
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "address"
    t.integer  "display_count",     default: 0,  null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "name",              default: "", null: false
  end

  create_table "thumbnails", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "encoded_content_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["encoded_content_id"], name: "index_thumbnails_on_encoded_content_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "facebook_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  create_table "view_counters", force: :cascade do |t|
    t.string  "viewed_model"
    t.integer "model_id"
    t.index ["model_id"], name: "index_view_counters_on_model_id", using: :btree
    t.index ["viewed_model"], name: "index_view_counters_on_viewed_model", using: :btree
  end

  add_foreign_key "contents", "users"
  add_foreign_key "coordinates", "contents"
  add_foreign_key "encoded_contents", "contents"
  add_foreign_key "judge_evaluations", "contents"
  add_foreign_key "original_media", "contents"
  add_foreign_key "playlist_translations", "playlists"
  add_foreign_key "thumbnails", "encoded_contents"
end
