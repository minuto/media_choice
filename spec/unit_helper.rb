require 'simplecov'
SimpleCov.start do
  coverage_dir('coverage/rspec_unit')
  add_filter '/app/admin'
end
# Minimum coverage is only desired on CI tools when building the environment. CI is a
# default environment variable used by GitlabCI (and Travis). For reference, see here:
# http://docs.gitlab.com/ce/ci/variables/README.html
SimpleCov.minimum_coverage(100) if ENV['CI'] == 'true'
require 'rails_helper'
