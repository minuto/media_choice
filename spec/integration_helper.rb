require 'simplecov'
SimpleCov.start do
  coverage_dir('coverage/rspec_integration')
  add_filter '/app/admin'
end

# Minimum coverage is not desired here because we don't expect to create many integration tests
require 'rails_helper'
