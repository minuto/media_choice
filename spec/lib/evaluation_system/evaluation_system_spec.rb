require 'rails_helper'

describe EvaluationSystem do
  describe 'class methods' do
    describe 'request_evaluation' do
      let(:content) { build(:video) }
      let(:encoded_content) { build(:encoded_video) }
      let(:encoded_contents) { [encoded_content] }
      it 'is expected to prepare a evaluation request to be passed to the RequestMaker' do
        body = {video_evaluation_request: {external_id: content.id, duration_in_ms: encoded_content.duration } }
        expect(content).to receive(:encoded_contents).and_return(encoded_contents)
        expect(RequestMaker).to receive(:post).with('video_evaluation_requests', body)

        EvaluationSystem.request_evaluation(content)
      end
    end

    describe 'content_json_for_evaluation' do
      let(:content) { build :video }
      let(:encoded_content) { build :encoded_video }
      let(:keys) { %w(id title soundtrack evaluation_state judge_rating created_at description user contests) }
      let(:profile) { build :profile }
      let(:user) { build :user }
      let(:thumbnail) { double('thumbnail') }

      before do
        expect(thumbnail).to receive(:width)
        expect(thumbnail).to receive(:height)
        expect(thumbnail).to receive(:url).with(no_args)
        expect(thumbnail).to receive(:url).with(:small)
        expect(content).to receive(:thumbnails).and_return([thumbnail])
        expect(content).to receive(:user).and_return user
        expect(content).to receive(:encoded_contents).and_return [encoded_content]
        expect(user).to receive(:profile).at_least(1).times.and_return profile
      end

      it 'returns a json with the required evaluation extra fields' do
        expect(EvaluationSystem.content_json_for_evaluation(content)).to include(*keys, :output_url, :tags, :thumb_width, :thumb_height, :thumb_url)
      end

      it 'is expected to return the URL for the encoded content' do
        expect(EvaluationSystem.content_json_for_evaluation(content)[:output_url]).to eq(encoded_content.video.url)
      end
    end
  end
end
