require 'rails_helper'

describe RequestMaker do
  describe 'class methods' do
    let(:request) { double('Net::HTTP::Post') }
    let(:auth_token) { EvaluationSystemSettings.auth_token }
    let(:uri) { URI(EvaluationSystemSettings.uri) }
    let(:action) { '/action' }
    let(:http_start) { double('http_start') }

    before :each do
      expect(http_start).to receive(:finish)
      expect(http_start).to receive(:request).with(request)
      expect(Net::HTTP).to receive(:start).and_return(http_start)
      expect(request).to receive(:uri).twice.and_return(uri)
    end

    it 'makes a post request' do
      parameters = {name: 'media_choice'}
      body = parameters.merge(auth_token: auth_token)

      expect(Net::HTTP::Post).to receive(:new).and_return(request)
      expect(request).to receive(:add_field).with('Content-Type', 'application/json')
      expect(request).to receive(:body=).with(body.to_json)

      described_class.post(action, parameters)
    end
  end
end
