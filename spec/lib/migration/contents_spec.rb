require 'unit_helper'
require 'migration/migration'

describe Migration::Content do
  describe 'class methods' do
    describe 'migrate' do
      let(:content_to_be_migrated) { build(:migration_content) }
      let(:prepared_query_result) do
        hash = content_to_be_migrated.to_h
        hash.stringify_keys
      end
      let(:columns) { prepared_query_result.keys }
      let(:relation_mock) { double('ActiveRecord::Relation') }
      let(:values) { [prepared_query_result.values] }

      before do
        # There's no way to retrieve the column names from the database, as the connection is not allowed on test environment
        # Therefore, we use an OpenStruct with the attributes method (or mock) to retrieve the attributes
        content_to_be_migrated.class.send(:define_method, 'attributes') do
          to_h.stringify_keys
        end
        expect(Migration::Content.connection).to receive(:exec_query).and_return(relation_mock)
        expect(relation_mock).to receive(:columns).and_return columns
        expect(relation_mock).to receive(:map).and_return values
      end

      context 'without errors' do
        it 'correctly saves the contents on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::Content, false).and_return true
          Migration::Content.migrate
        end
      end

      context 'with errors' do
        it 'doesn\'t save the contents on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::Content, false).and_return false
          Migration::Content.migrate
        end
      end
    end

    describe 'migrate_original_medias' do
      let(:content_to_be_migrated) { build(:migration_content) }
      let(:prepared_query_result) do
        hash = content_to_be_migrated.to_h
        hash.stringify_keys
        result = {}
        result['content_id'] = hash['id']
        result['created_at'] = hash['created_at']
        result['updated_at'] = hash['updated_at']
        result['media_file_name'] = hash['input_file_name']
        result['media_content_type'] = hash['input_content_type']
        result['media_file_size'] = hash['input_file_size']
        result['media_updated_at'] = hash['input_updated_at']
        result
      end
      let(:columns) { %w(content_id created_at updated_at media_file_name media_content_type media_file_size media_updated_at) }
      let(:values) { [prepared_query_result.values] }

      before do
        # There's no way to retrieve the column names from the database, as the connection is not allowed on test environment
        # Therefore, we use an OpenStruct with the attributes method (or mock) to retrieve the attributes
        content_to_be_migrated.class.send(:define_method, 'attributes') do
          to_h.stringify_keys
        end
        expect(Migration::Content).to receive(:pluck).and_return(values)
      end

      context 'without errors' do
        it 'correctly saves the original medias on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, OriginalMedia, false).and_return true
          Migration::Content.migrate_original_medias
        end
      end

      context 'with errors' do
        it 'doesn\'t save the original medias on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, OriginalMedia, false).and_return false
          Migration::Content.migrate_original_medias
        end
      end
    end

    describe 'migrate_judge_evaluations' do
      let(:content_to_be_migrated) { build(:migration_content) }
      let(:prepared_query_result) do
        hash = content_to_be_migrated.to_h
        hash.stringify_keys
      end
      let(:relation_mock) { double('ActiveRecord::Relation') }
      let(:columns) { %w(content_id judge_rating created_at updated_at evaluation_state) }
      let(:values) { [prepared_query_result.values] }

      before do
        # There's no way to retrieve the column names from the database, as the connection is not allowed on test environment
        # Therefore, we use an OpenStruct with the attributes method (or mock) to retrieve the attributes
        content_to_be_migrated.class.send(:define_method, 'attributes') do
          to_h.stringify_keys
        end
        expect(Migration::Content).to receive(:pluck).and_return(values)
      end

      context 'without errors' do
        it 'correctly saves the judge evaluations on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::JudgeEvaluation).and_return true
          Migration::Content.migrate_judge_evaluations
        end
      end

      context 'with errors' do
        it 'doesn\'t save the judge evaluations on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::JudgeEvaluation).and_return false
          Migration::Content.migrate_judge_evaluations
        end
      end
    end
  end
end
