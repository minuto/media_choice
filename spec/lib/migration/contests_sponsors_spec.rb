require 'unit_helper'
require 'migration/migration'

describe Migration::ContestsSponsors do
  describe 'class method' do
    describe 'migrate' do
      let(:columns) { %w(contest_id sponsor_id) }
      let(:values) { [[23, 12]] }

      it 'is expected to insert into the association table' do
        insert_query = 'INSERT INTO contests_sponsors (contest_id,sponsor_id) VALUES'
        values.each { |value| insert_query << " (#{value.first},#{value.second})" }

        expect(described_class).to receive(:pluck).with(*columns).and_return(values)

        expect(ActiveRecord::Base.connection).to receive(:execute).with(insert_query)

        described_class.migrate
      end
    end
  end
end
