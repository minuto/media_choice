require 'unit_helper'
require 'migration/migration'

describe Migration::Contest do
  describe 'class method' do
    describe 'migrate' do
      let(:columns) { %w(id start_date end_date parent_id created_at updated_at) }
      let(:values) { double('values') }

      before do
        all = double('all relation')
        selection = double('selection relation')

        expect(selection).to receive(:pluck).with(*columns).and_return values
        expect(all).to receive(:select).with(columns).and_return selection
        expect(described_class).to receive(:all).and_return all
      end

      it 'is expected to query the database and save the records' do
        expect(described_class).to receive(:save_records).with(columns, values, ::Contest)

        described_class.migrate
      end
    end
  end
end
