require 'unit_helper'
require 'migration/migration'

describe Migration::EncodedVideo do
  describe 'class methods' do
    describe 'migrate' do
      let(:encoded_video_to_be_migrated) { build(:migration_encoded_video) }
      let(:prepared_query_result) do
        hash = encoded_video_to_be_migrated.to_h
        hash['job_id'] = 123
        hash.stringify_keys
      end
      let(:columns) { prepared_query_result.keys }
      let(:relation_mock) { double('ActiveRecord::Relation') }
      let(:values) { [prepared_query_result.values] }

      before do
        # There's no way to retrieve the column names from the database, as the connection is not allowed on test environment
        # Therefore, we use an OpenStruct with the attributes method (or mock) to retrieve the attributes
        encoded_video_to_be_migrated.class.send(:define_method, 'attributes') do
          to_h.stringify_keys
        end
        expect(described_class).to receive(:where).and_return(relation_mock)
        expect(relation_mock).to receive(:joins).and_return(relation_mock)
        expect(relation_mock).to receive(:where).and_return relation_mock
        expect(relation_mock).to receive(:includes).and_return relation_mock
        expect(relation_mock).to receive(:pluck).and_return values
      end

      context 'without errors' do
        it 'correctly saves the encoded videos on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::EncodedVideo, false).and_return true
          Migration::EncodedVideo.migrate
        end
      end

      context 'with errors' do
        it 'doesn\'t save the contents on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::EncodedVideo, false).and_return false
          Migration::EncodedVideo.migrate
        end
      end
    end
  end
end
