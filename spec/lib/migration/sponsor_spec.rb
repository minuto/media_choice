require 'unit_helper'
require 'migration/migration'

describe Migration::Sponsor do
  describe 'class methods' do
    describe 'migrate' do
      let(:sponsor_to_be_migrated) { build(:migration_sponsor) }
      let(:total_count) { 42 }
      let(:prepared_query_result) do
        hash = sponsor_to_be_migrated.to_h
        hash[:address] = hash.delete(:url)
        hash[:display_count] = total_count
        hash.delete :impressions
        hash.delete :order
        hash.stringify_keys
      end
      let(:relation_mock) { double('ActiveRecord::Relation') }
      let(:columns) { prepared_query_result.keys }
      let(:values) { [prepared_query_result.values] }

      before do
        # There's no way to retrieve the column names from the database, as the connection is not allowed on test environment
        # Therefore, we use an OpenStruct with the attributes method (or mock) to retrieve the attributes
        sponsor_to_be_migrated.class.send(:define_method, 'attributes') do
          to_h.stringify_keys
        end
        expect(Migration::Sponsor.connection).to receive(:exec_query).and_return(relation_mock)
        expect(relation_mock).to receive(:columns).and_return columns
        expect(relation_mock).to receive(:map).and_return values
      end

      context 'without errors' do
        it 'correctly saves the sponsor on the database and updates the logo files' do
          expect(described_class).to receive(:save_records).with(columns, values, ::Sponsor).and_return true
          expect(described_class).to receive(:update_all_logo_files)
          Migration::Sponsor.migrate
        end
      end

      context 'with errors' do
        it 'doesn\'t save the sponsor on the database' do
          expect(described_class).to receive(:save_records).with(columns, values, ::Sponsor).and_return false
          Migration::Sponsor.migrate
        end
      end
    end

    describe 'get_file_uri' do
      let(:id) { attributes_for(:migration_sponsor)['id'] }
      let(:uri) { "#{Migration::Sponsor::STATIC_PATH_PREFIX}#{id}/logos/original.png" }
      context 'the file is accessible' do
        before do
          expect(described_class).to receive(:accessible_publicly?).with(URI.parse(uri)).and_return true
        end

        it 'returns the file\'s URI' do
          uri = described_class.get_file_uri id
          expect(uri).to be_a(URI::HTTPS)
          expect(uri.to_s).to match(%r{#{Migration::Sponsor::STATIC_PATH_PREFIX}.*#{id}.*logos/original.png})
        end
      end

      context 'the file is not accessible' do
        before do
          expect(described_class).to receive(:accessible_publicly?).with(URI.parse(uri)).and_return false
        end

        it 'returns nil' do
          expect(described_class.get_file_uri(id)).to be_nil
        end
      end
    end

    describe 'update_all_logo_files' do
      let(:sponsor_1) { build(:sponsor, id: 1) }
      let(:sponsor_2) { build(:sponsor, id: 2) }
      let(:sponsor_3) { build(:sponsor, id: 3) }
      let(:sponsors) { [sponsor_1, sponsor_2, sponsor_3] }
      let(:sponsor_1_uri) { double(URI::HTTPS) }
      let(:sponsor_3_uri) { double(URI::HTTPS) }
      let(:logo1) { double(:logo1) }
      let(:logo3) { double(:logo3) }

      before do
        expect(Sponsor).to receive(:all).and_return sponsors
        expect(described_class).to receive(:get_file_uri).with(sponsor_1.id).and_return sponsor_1_uri
        expect(described_class).to receive(:get_file_uri).with(sponsor_2.id).and_return nil
        expect(described_class).to receive(:get_file_uri).with(sponsor_3.id).and_return sponsor_3_uri
      end

      it 'tries to associate a new file with the sponsors' do
        expect(sponsor_1).to receive(:logo).and_return logo1
        expect(sponsor_3).to receive(:logo).and_return logo3
        expect(logo1).to receive(:reprocess!).with(:thumb).and_return true
        expect(logo3).to receive(:reprocess!).with(:thumb).and_return false
        described_class.update_all_logo_files
      end
    end

    describe 'accessible_publicly?' do
      let(:uri) { URI.parse('https://www.google.com/ncr') }
      let(:http) { double(Net::HTTP) }
      let(:get_request) { double(Net::HTTP::Get) }
      let(:response) { double(Net::HTTPResponse) }

      before do
        expect(Net::HTTP).to receive(:new).with(uri.host, uri.port).and_return http
        # The requests will have to use ssl
        expect(http).to receive(:use_ssl=).with(true)
        expect(Net::HTTP::Get).to receive(:new).with(uri.request_uri).and_return get_request
        expect(http).to receive(:request).with(get_request).and_return response
      end

      context 'when the return code from an http request is 200' do
        before do
          expect(response).to receive(:code).and_return '200'
        end

        it 'returns true' do
          expect(described_class.accessible_publicly?(uri)).to be true
        end
      end

      context 'when the return code from an http request is not 200' do
        before do
          expect(response).to receive(:code).and_return '500'
        end

        it 'returns false' do
          expect(described_class.accessible_publicly?(uri)).to be false
        end
      end
    end
  end
end
