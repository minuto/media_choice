require 'unit_helper'
require 'migration/migration'

describe Migration::PublicRating do
  describe 'class method' do
    describe 'migrate' do
      let(:columns) { %w(content_id user_id grade) }
      let(:select_columns) { ['content_id', 'user_id', 'MAX(grade) as grade'] }
      let(:values) { double('values') }

      before do
        selection = double('selection relation')
        where = double('where relation')
        not_clause = double('not relation')
        group = double('group relation')

        expect(group).to receive(:pluck).with(*select_columns).and_return values
        expect(not_clause).to receive(:group).with(:content_id, :user_id).and_return group
        expect(where).to receive(:not).with(grade: nil).and_return not_clause
        expect(selection).to receive(:where).and_return where
        expect(described_class).to receive(:select).with(select_columns).and_return selection
      end

      it 'is expected to query the database and save the records' do
        expect(described_class).to receive(:save_records).with(columns, values, ::PublicRating)

        described_class.migrate
      end
    end
  end
end
