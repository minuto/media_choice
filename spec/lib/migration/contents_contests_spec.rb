require 'unit_helper'
require 'migration/migration'

describe Migration::ContentsContests do
  describe 'class methods' do
    describe 'migrate' do
      let(:columns) { %w(content_id contest_id) }
      let(:data) { [[1, 2], [3, 4]] }

      it 'inserts the plucked data into the database)' do
        expect(described_class).to receive(:pluck).with(*columns).and_return(data)
        expect(described_class).to receive(:insert).with(data)
        described_class.migrate
      end
    end

    describe 'insert' do
      let(:data) { [[1, 2], [3, 4]] }
      let(:table_name) { 'table_name' }
      let(:sanitized_data) { data.map { |x| "(#{x[0]},#{x[1]})" }.join(',') }
      let(:desired_query) { "INSERT INTO #{table_name} (content_id,contest_id) VALUES #{sanitized_data}" }

      it 'builds a query to insert the required rows in the database' do
        expect(described_class).to receive(:table_name).and_return table_name
        expect(described_class.connection).to receive(:execute).with desired_query
        described_class.insert(data)
      end
    end
  end
end
