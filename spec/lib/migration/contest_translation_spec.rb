require 'unit_helper'
require 'migration/migration'

describe Migration::ContestTranslation do
  describe 'class method' do
    describe 'migrate' do
      let(:columns) { %w(id locale logo_file_name logo_content_type logo_file_size logo_updated_at description contest_id created_at updated_at name) }
      let(:values) { double('values') }
      let(:query_columns) {
        ['`contest_logos`.id', '`contest_logos`.locale', '`contest_logos`.logo_file_name',
         '`contest_logos`.logo_content_type', '`contest_logos`.logo_file_size',
         '`contest_logos`.logo_updated_at', '`contest_translations`.description',
         '`contest_translations`.contest_id', '`contest_translations`.created_at',
         '`contest_translations`.updated_at', '`contest_translations`.title AS name']
      }

      it 'is expected to join contest_logos and save the records' do
        join_statement = 'INNER JOIN contest_logos ON `contest_logos`.contest_id = `contest_translations`.contest_id AND `contest_logos`.locale = `contest_translations`.idiom'
        data = double('join relation')

        expect(data).to receive(:pluck).with(*query_columns).and_return(values)
        expect(described_class).to receive(:joins).with(join_statement).and_return(data)

        expect(described_class).to receive(:save_records).with(columns, values, ::ContestTranslation)

        described_class.migrate
      end
    end
  end
end
