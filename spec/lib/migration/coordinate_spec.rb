require 'unit_helper'
require 'migration/migration'

describe Migration::Coordinate do
  describe 'class method' do
    describe 'migrate' do
      let(:columns) { %w(latitude longitude found content_id) }
      let(:values) { double('values') }

      before do
        expect(described_class).to receive(:pluck).with(*columns).and_return values
      end

      it 'is expected to query the database and save the records' do
        expect(described_class).to receive(:save_records).with(columns, values, ::Coordinate)

        described_class.migrate
      end
    end
  end
end
