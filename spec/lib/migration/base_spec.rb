require 'unit_helper'
require 'migration/migration'

describe Migration::Base do
  describe 'class methods' do
    let(:placeholder_class) do
      Migration::User
    end
    let(:attribute_values) { [['attribute1', 'attribute2'], ['another1', 'another2']] }
    let(:columns) { [:column1, :column2] }

    describe 'save_record' do
      context 'when the record is successfully saved' do
        it 'returns the saved record' do
          expect(placeholder_class).to receive(:import).with(columns, attribute_values, validate: true)
          expect(placeholder_class.save_records(columns, attribute_values, placeholder_class)).to eq true
        end
      end

      context 'when the record is not successfully saved' do
        let(:errors) { double('ActiveModel::Errors') }
        let(:error_messages) { 'Error Messages' }
        it 'logs into debug level the error messages and returns nil' do
          expect(placeholder_class).to receive(:import).with(columns, attribute_values, validate: true).and_raise ActiveRecord::RecordInvalid
          expect(Rails.logger).to receive(:debug).at_least(1).times
          expect(placeholder_class.save_records(columns, attribute_values, placeholder_class)).to eq false
        end
      end
    end
  end
end
