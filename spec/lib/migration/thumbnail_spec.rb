require 'unit_helper'
require 'migration/migration'

describe Migration::Thumbnail do
  describe 'class method' do
    describe 'migrate' do
      let(:columns) { %w(id encoded_content_id image_file_name image_content_type image_file_size image_updated_at created_at updated_at) }
      let(:query_columns) { %w(id encoded_video_id image_file_name image_content_type image_file_size image_updated_at created_at updated_at) }
      let(:values) { double('values') }
      let(:relation_mock) { double('ActiveRecord::Relation') }
      let(:id_list) { double('id list') }

      before do
        expect(EncodedVideo).to receive(:pluck).and_return id_list
        expect(described_class).to receive(:where).with(encoded_video_id: id_list).and_return relation_mock
        expect(relation_mock).to receive(:pluck).with(*query_columns).and_return values
      end

      it 'is expected to query the database and save the records' do
        expect(described_class).to receive(:save_records).with(columns, values, ::Thumbnail)

        described_class.migrate
      end
    end
  end
end
