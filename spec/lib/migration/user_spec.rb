require 'unit_helper'
require 'migration/migration'

describe Migration::User do
  describe 'class methods' do
    let(:user_1_to_be_migrated) { build(:migration_user) }
    let(:user_batch) { instance_double(ActiveRecord::Relation) }
    let(:user_1) { build(:user) }
    let(:user_2) { build(:user) }
    let(:columns) { [:id, :email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :confirmation_token, :confirmed_at, :confirmation_sent_at, :created_at, :updated_at, :admin, :facebook_id] }
    let(:values) { [user_1.attributes.values, user_2.attributes.values] }

    before do
      # There's no way to retrieve the column names from the database, as the connection is not allowed on test environment
      # Therefore, we use an OpenStruct with the attributes method (or mock) to retrieve the attributes
      user_1_to_be_migrated.class.send(:define_method, 'attributes') do
        to_h.stringify_keys
      end
    end

    describe 'migrate' do
      context 'without errors' do
        it 'saves the user\'s relevant attributes on the database' do
          expect(user_batch).to receive(:first).at_least(1).times.and_return user_1_to_be_migrated
          expect(user_batch).to receive(:select).and_return user_batch
          expect(user_batch).to receive(:pluck).and_return values
          expect(described_class).to receive(:save_records).with(columns, values, ::User, false).and_return true
          expect(described_class).to receive(:create_profiles).with(user_batch)
          Migration::User.migrate(user_batch)
        end
      end

      context 'with errors' do
        it 'doesn\'t create the profile' do
          expect(user_batch).to receive(:first).at_least(1).times.and_return user_1_to_be_migrated
          expect(user_batch).to receive(:select).and_return user_batch
          expect(user_batch).to receive(:pluck).and_return values
          expect(described_class).to receive(:save_records).with(columns, values, ::User, false).and_return false
          expect(described_class).to_not receive(:create_profiles)
          Migration::User.migrate(user_batch)
        end
      end
    end

    describe 'create_profile' do
      let(:occupation) { build :migration_occupation }
      let(:school_type) { build :migration_school_type }
      let(:ids) { [1] }
      let(:connection) { double('connection') }
      let(:profile_1_attributes) { build(:profile).attributes.to_h }
      let(:profile_2_attributes) { build(:profile, country_numeric_code: 'PR').attributes.to_h }
      let(:query_response) { double('query_response') }

      context 'profile successfully created' do
        it 'saves the profile\'s relevant attributes on the database' do
          # set numbers for the fields below, so they can be similar to what we would receive after querying the old database
          profile_1_attributes['occupation'] = profile_2_attributes['occupation'] = 1
          profile_1_attributes['school_type'] = profile_2_attributes['school_type'] = 1
          expect(user_batch).to receive(:where).and_return user_batch
          expect(user_batch).to receive(:pluck).with(:id).and_return ids
          expect(described_class).to receive(:connection).and_return connection
          expect(connection).to receive(:exec_query).with(instance_of(String)).and_return query_response
          expect(query_response).to receive(:to_hash).and_return [profile_1_attributes, profile_2_attributes]
          expect(described_class).to receive(:save_records)
          Migration::User.create_profiles(user_batch)
        end
      end
    end

    describe 'migrate_leftover_profiles' do
      let(:id_list) { [1, 2] }
      let(:connection) { double('connection') }
      let(:profile_attributes) { build(:profile).attributes.to_h }
      let(:columns) { %w(user_id first_name last_name birthdate gender state city country school responsible_document responsible_name zip_code receive_mass_email phone occupation school_type) }

      it 'is expected to create the profiles that violate validations' do
        # test nil values so we can ensure nothing breaks
        profile_attributes['occupation'] = nil
        profile_attributes['school_type'] = nil
        expect(Profile).to receive(:pluck).with(:user_id).and_return id_list
        expect(described_class).to receive(:connection).and_return connection
        expect(connection).to receive(:exec_query).with(instance_of(String)).and_return profile_attributes
        expect(profile_attributes).to receive(:to_hash).and_return [profile_attributes]

        expect(described_class).to receive(:save_records)

        described_class.migrate_leftover_profiles
      end
    end
  end
end
