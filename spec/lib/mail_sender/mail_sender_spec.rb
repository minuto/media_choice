require 'unit_helper'
require 'mail_sender/mail_sender'

describe MailSender do
  describe 'initialize' do
    let(:mass_mail) { build(:mass_mail) }

    it 'creates a new MailSender object' do
      mail_sender = described_class.new mass_mail
      sendgrid_mail_object = mail_sender.mail
      expect(sendgrid_mail_object).to be_a SendGrid::Mail
      expect(sendgrid_mail_object.subject).to eq mass_mail.subject
      expect(sendgrid_mail_object.from).to eq(SendGrid::Email.new(email: ::SendgridSettings.from).to_json)
      expect(sendgrid_mail_object.contents.first['value']).to eq mass_mail.body
    end
  end

  describe 'send' do
    let(:mass_mail_locale) { 'pt-BR' }
    let(:mass_mail) { build(:mass_mail, locale: mass_mail_locale) }
    subject { MailSender.new mass_mail }
    let(:user_1) { build(:user, profile: build(:profile, preferred_language: mass_mail_locale)) }
    let(:user_2) { build(:user, profile: build(:profile, preferred_language: mass_mail_locale)) }
    let(:users) { [user_1, user_2] }

    context 'without mail attachments' do
      let(:code) { '202' }
      let(:body) { 'success' }
      let(:response) { Struct.new('SendGridResponse', :status_code, :body).new(code, body) }

      it 'calls the post method on the api client' do
        expect_any_instance_of(SendGrid::Client).to receive(:build_request).with(:post, [request_body: subject.mail.to_json]).and_return(response)
        subject.send
      end

      it 'adds users as recipients' do
        expect(User).to receive(:mailing_list).with(mass_mail_locale).and_return users
        expect_any_instance_of(SendGrid::Client).to receive(:build_request).with(:post, [request_body: subject.mail.to_json]).and_return(response)
        subject.send
        personalization = subject.mail.personalizations.first
        recipients = personalization['to'].map { |tos| tos['email'] }

        expect(recipients).to include(user_1.email)
        expect(recipients).to include(user_2.email)
      end
    end

    context 'with response errors' do
      let(:code) { '404' }
      let(:body) { 'Mystery Error' }
      let(:response) { Struct.new('SendGridResponse', :status_code, :body).new(code, body) }

      it 'is expected to raise a RuntimeError' do
        expect(User).to receive(:mailing_list).with(mass_mail_locale).and_return users
        expect_any_instance_of(SendGrid::Client).to receive(:build_request).with(:post, [request_body: subject.mail.to_json]).and_return(response)

        expect { subject.send }.to raise_error(RuntimeError, code + ': ' + body)
      end
    end
  end
end
