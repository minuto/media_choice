require 'unit_helper'

RSpec.describe ContestsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/contests').to route_to('contests#index')
    end

    it 'routes to #show' do
      expect(get: '/contests/1').to route_to('contests#show', id: '1')
    end
  end
end
