require 'unit_helper'

describe StaticViewsController do
  describe 'routing' do
    actions = [:terms_of_use, :creative_commons, :sponsorship, :contact_us, :soundtrack_copyright, :faq, :education, :exhibit_on_your_institution, :about_us, :image_copyright]

    actions.each do |action|
      it "routes root to #{action}" do
        expect(get: "/#{action}").to route_to(controller: 'static_views', action: action.to_s)
      end
      it "routes root to #{action} with locale" do
        expect(get: "/en/#{action}").to route_to(controller: 'static_views', action: action.to_s, locale: 'en')
      end
    end
  end
end
