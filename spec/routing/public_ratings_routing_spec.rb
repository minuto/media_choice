RSpec.describe PublicRatingsController, type: :routing do
  describe 'routing' do
    it 'routes to #public_rating' do
      expect(post: '/public_rating').to route_to('public_ratings#public_rating')
    end
  end
end
