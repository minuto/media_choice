require 'unit_helper'

describe HomeController do
  describe 'routing' do
    it 'routes root to index' do
      expect(get: '/').to route_to(controller: 'home', action: 'index')
    end
    it 'routes root to index with locale' do
      expect(get: '/en').to route_to(controller: 'home', action: 'index', locale: 'en')
    end
  end
end
