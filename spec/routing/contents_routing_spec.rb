require 'unit_helper'

describe ContentsController do
  describe 'routing' do
    it 'does not route to #index with html format' do
      expect(get: '/contents.html').not_to route_to('contents#index', format: 'html')
      expect(get: '/videos.html').not_to route_to('contents#index', format: 'html')
    end

    it 'routes to #index with json format' do
      expect(get: '/contents.json').to route_to('contents#index', format: 'json')
      expect(get: '/videos.json').to route_to('contents#index', format: 'json')
    end

    it 'does not route to #evaluation with html format' do
      expect(put: '/contents/1/evaluation.html').not_to route_to('contents#evaluation', format: 'html', id: '1')
    end

    it 'routes to #evaluation with json format' do
      expect(put: '/contents/1/evaluation.json').to route_to('contents#evaluation', format: 'json', id: '1')
    end

    it 'does not route to #video with html format' do
      expect(get: '/videos/1.html').not_to route_to('contents#video', format: 'html', id: '1')
    end

    it 'routes to #video with json format' do
      expect(get: '/videos/1.json').to route_to('contents#video', format: 'json', id: '1')
    end

    it 'routes to #new' do
      expect(get: '/contents/new').to route_to('contents#new')
    end

    it 'routes to #show' do
      expect(get: '/contents/1').to route_to('contents#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/contents/1/edit').to_not route_to('contents#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/contents').to route_to('contents#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/contents/1').to_not route_to('contents#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/contents/1').to_not route_to('contents#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/contents/1').to_not route_to('contents#destroy', id: '1')
    end

    it 'routes to #callback' do
      expect(post: '/contents/zencoder_callback').to route_to('contents#zencoder_callback')
    end

    it 'routes to #map_view' do
      expect(get: '/contents/1/map_view').to route_to('contents#map_view', id: '1')
    end

    it 'does not route to #related_contents with html format' do
      expect(get: '/contents/1/related.html').not_to route_to('contents#related_contents', format: 'html', id: '1')
    end

    it 'routes to #related_contents with json format' do
      expect(get: '/contents/1/related.json').to route_to('contents#related_contents', format: 'json', id: '1')
    end

    it 'routes to #map_player' do
      expect(get: '/contents/1/map_player').to route_to('contents#map_player', id: '1')
    end

    it 'routes to #share_button' do
      expect(get: '/contents/1/share_button').to route_to('contents#share_button', id: '1')
    end

    it 'routes to #share_box' do
      expect(get: '/contents/1/share_box').to route_to('contents#share_box', id: '1')
    end
  end
end
