require 'unit_helper'

describe Admin::UsersController, type: :controller do
  describe 'resource existence' do
    it { expect(ActiveAdmin.application.namespaces[:admin].resources.keys.map(&:name)).to include('User') }
    xit { expect(ActiveAdmin.application.namespaces[:admin].resources.keys.map(&:name)).to_not include('Comment') }
  end

  describe 'resource configuration' do
    let(:resource_class) { User }
    let(:all_resources)  { ActiveAdmin.application.namespaces[:admin].resources }
    let(:resource)       { all_resources[resource_class] }

    it 'verifies resource name' do
      expect(resource.resource_name).to eq 'User'
    end

    it 'defines actions for the resource' do
      expect(resource.defined_actions).to include(:create, :new, :update, :edit, :index, :show, :destroy)
    end

    it 'defines filters for the resource' do
      expect(resource.filters).to include(:email, :current_sign_in_at, :sign_in_count, :created_at)
    end

    it 'adds the resource to the menu' do
      expect(resource.include_in_menu?).to eq true
    end
  end
end
