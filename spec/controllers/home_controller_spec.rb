require 'unit_helper'

describe HomeController do
  describe 'Rendering' do
    it 'is expected to include the locale on the url' do
      # Ensure the locale is merged into urls' options
      expect(root_url).to match(/#{I18n.locale}/)
    end
  end

  describe 'Language auto-detection' do
    it 'is expected to automatically use the language specified in the request headers' do
      request.env['HTTP_ACCEPT_LANGUAGE'] = 'pt-BR'
      get :index
      expect(I18n.locale).to eq(:'pt-BR')
    end

    it 'is expected to use a different region if still the best match' do
      request.env['HTTP_ACCEPT_LANGUAGE'] = 'en-GB'
      get :index
      expect(I18n.locale).to eq(:en)
    end

    it 'is expected to use the default language if no available language matches the requested one' do
      request.env['HTTP_ACCEPT_LANGUAGE'] = 'de'
      get :index
      expect(I18n.locale).to eq(:'pt-BR')
    end

    after do
      I18n.locale = I18n.default_locale
    end
  end

  describe 'actions' do
    describe 'index' do
      let(:recent_contents) { double('Content') }
      let(:featured_contents) { double('Content') }
      let(:ordered_featured_contents) { double('Content') }

      before do
        Rails.cache.clear
      end

      it 'searches for contests and contents' do
        expect(Content).to receive(:featured).and_return featured_contents
        expect(ordered_featured_contents).to receive(:limit).with(30)
        expect(featured_contents).to receive(:order).with(created_at: :desc).and_return ordered_featured_contents
        expect(Content).to receive(:recent).and_return recent_contents
        expect(recent_contents).to receive(:first)
        expect(Contest).to receive(:started)
        expect(HomeHighlight).to receive(:visible)
        expect(Playlist).to receive(:visible)
        get :index
        expect(response.status).to eq(200)
      end

      after do
        Rails.cache.clear
      end
    end
  end
end
