require 'unit_helper'

RSpec.describe PublicRatingsController, type: :controller do
  include Devise::Test::ControllerHelpers
  describe 'public_rating' do
    let(:content) { build :video }
    let(:grade) { 3 }

    before do
      sign_in create(:user, confirmed_at: Time.zone.now)
    end

    context 'with valid parameters' do
      before do
        expect_any_instance_of(PublicRating).to receive(:save).and_return true
        post :public_rating, params: { content_id: content.id, grade: grade }
      end

      it { is_expected.to respond_with(:ok) }

      it 'returns the successful public rating message' do
        expect(JSON.parse(response.body)['message']).to eq I18n.t 'successful_public_rating'
      end
    end

    context 'with invalid parameters' do
      before do
        expect_any_instance_of(PublicRating).to receive(:save).and_return false
        post :public_rating, params: { content_id: content.id, grade: grade }
      end

      it { is_expected.to respond_with(:ok) }

      it 'returns the unsuccessful public rating message' do
        expect(JSON.parse(response.body)['message']).to eq I18n.t 'unsuccessful_public_rating'
      end
    end
  end
end
