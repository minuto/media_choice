require 'unit_helper'

RSpec.describe ContestsController, type: :controller do
  describe 'GET #show' do
    let(:contest) { build(:contest, id: 1) }
    let(:contents_list) { double('contents_list') }

    before do
      Rails.cache.clear
      expect(Contest).to receive(:find).with(contest.id.to_s).and_return contest
      expect(contest).to receive(:contents_to_show).and_return contents_list
    end

    context 'without sponsors' do
      it 'assigns the requested contest as @contest' do
        get :show, params: { id: contest.to_param }
      end
    end

    context 'with sponsors' do
      let(:sponsors) { build_list(:sponsor, 2) }
      let(:display_count_double) { double('Integer') }

      before do
        expect(contest).to receive(:sponsors).and_return sponsors
      end

      it 'creates a Sponsor View for each sponsor' do
        sponsors.each do |sponsor|
          expect(ViewCounter).to receive(:create!).with(viewed_model: Sponsor.to_s, model_id: sponsor.id)
        end

        get :show, params: { id: contest.to_param }
      end
    end

    after do
      Rails.cache.clear
    end
  end

  describe 'GET #index' do
    let(:finished_contests) { build_list(:contest, 2, end_date: Time.zone.yesterday) }
    let(:started_contests) { build_list(:contest, 2) }

    before do
      expect(Contest).to receive(:finished).and_return finished_contests
      expect(Contest).to receive(:started).and_return started_contests
      get :index
    end

    it 'retrieves the lists of finished and started contests' do
      expect(JSON.parse(response.body)['finished_contests']).to match_array JSON.parse(finished_contests.to_json)
      expect(JSON.parse(response.body)['started_contests']).to match_array JSON.parse(started_contests.to_json)
    end

    it { is_expected.to respond_with :ok }
  end
end
