require 'unit_helper'

RSpec.describe Users::ConfirmationsController do
  describe 'after_confirmation_path_for' do
    let(:resource) { double('resource') }
    let(:resource_name) { double('resource_name') }
    it 'is expected to redirect to the root path after confirming a user' do
      expect(subject.send(:after_confirmation_path_for, resource_name, resource)).to eq(root_path)
    end
  end
end
