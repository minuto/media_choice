require 'unit_helper'

describe StaticViewsController do
  # WARNING! StaticViewsController is supposed only to render static views and, therefore, this file not supposed to
  # serve as a base model for other controller's unit tests.
  actions_and_responses = {
    terms_of_use: ['Festival do Minuto.* Rules and Regulations', 'Termos de Uso e.* Regulamento do Festival do Minuto'],
    creative_commons: ['About Creative Commons', 'Sobre o Creative Commons'],
    sponsorship: ['Sponsorship and Partnership', 'Patrocínio e parceria'],
    contact_us: ['Talk with us', 'Fale conosco'],
    soundtrack_copyright: ['Soundtrack licenses', 'Direitos autorais de trilha sonora'],
    faq: ['Frequently asked questions', 'Dúvidas frequentes'],
    education: ['Education', 'Educação e Minuto Escola'],
    exhibit_on_your_institution: ['Exhibit the Festival', 'Exiba o Minuto'],
    about_us: ['About the One Minute Festival', 'Sobre o Festival do Minuto'],
    image_copyright: ['Image licenses', 'Direitos autorais de imagem']
  }

  context 'actions' do
    actions_and_responses.each do |action, response_pair|
      context action.to_s do
        context 'rendering' do
          render_views

          before do
            # check if no layout is rendered
            expect(response.body).to_not have_css('header.general-header') # navbar
            expect(response.body).to_not have_css('footer') # footer
          end

          context 'in english' do
            before do
              get :"#{action}", params: { locale: :en }
            end

            it 'renders no layout' do
              expect(response.body).to match(/#{response_pair.first}/)
            end
          end

          context 'in portuguese' do
            before do
              get :"#{action}", params: { locale: :'pt-BR' }
            end

            it 'renders no layout' do
              expect(response.body).to match(/#{response_pair.last}/)
            end
          end

          it 'responds with ok' do
            get :"#{action}"
            is_expected.to respond_with(:ok)
          end
        end
      end
    end

    after do
      I18n.locale = I18n.default_locale
    end
  end
end
