#= require spec_helper
#= require profiles/state_select

describe 'state_select', ->
  before ->
    @state_select_div = html: (new_html) -> @content = new_html
    @state_select_div.html('old_data')
    sinon.stub(window, '$')
    $.withArgs('select#profile_country_numeric_code :selected').returns(val: -> '076')
    $.withArgs('#locale').returns(val: -> 'en')
    $.withArgs('#stored_state').returns(val: -> 'Sao Paulo')
    $.withArgs('div#state_select').returns(@state_select_div)

  after ->
    $.restore()

  it 'is expected to access the state route', ->
    response_data = 'data'
    $.get = (url, params, callback) ->
      callback(response_data)

    expect(@state_select_div.content).to.equal('old_data')

    state_select()

    expect(@state_select_div.content).to.equal(response_data)
