#= require spec_helper
#= require profiles/school_fields

describe 'school_fields', ->
  before ->
    @first_mock = is: ->
    @fields = css: ->
    @school_type = prop: ->
    @school_name = prop: ->
    @student_inputs = first: ->

    sinon.stub(window, '$')
    $.withArgs('input[name=student]').returns(@student_inputs)
    $.withArgs('#school_fields').returns(@fields)
    $.withArgs('#profile_school_type').returns(@school_type)
    $.withArgs('#user-school-name').returns(@school_name)

    @css_spy_mock = sinon.mock(@fields)
    @type_prop_mock = sinon.mock(@school_type)
    @name_prop_mock = sinon.mock(@school_name)
    @first_stub = sinon.stub(@student_inputs, 'first').returns(@first_mock)

  describe 'when the student input is checked', ->
    before ->
      @checked = true
      @is_stub = sinon.stub(@first_mock, 'is').withArgs(':checked').returns(@checked)

    it 'shows the school fields', ->
      @css_spy_mock.expects('css').once().withArgs('display', 'block')
      @type_prop_mock.expects('prop').once().withArgs('required', true)
      @name_prop_mock.expects('prop').once().withArgs('required', true)

      school_fields()

      sinon.assert.called(@first_stub)
      sinon.assert.called(@is_stub)

    after ->
      @first_mock.is.restore()

  describe 'when the student input is not checked', ->
    before ->
      @checked = false
      @is_stub = sinon.stub(@first_mock, 'is').withArgs(':checked').returns(@checked)

    it 'hides the school fields', ->
      @css_spy_mock.expects('css').once().withArgs('display', 'none')
      @type_prop_mock.expects('prop').once().withArgs('required', false)
      @name_prop_mock.expects('prop').once().withArgs('required', false)

      school_fields()

      sinon.assert.called(@first_stub)
      sinon.assert.called(@is_stub)

    after ->
      @first_mock.is.restore()

  after ->
    @css_spy_mock.verify()
    @type_prop_mock.verify()
    @name_prop_mock.verify()
    $.restore()
