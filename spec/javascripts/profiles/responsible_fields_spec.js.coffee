#= require spec_helper
#= require profiles/responsible_fields

describe 'responsible_fields', ->
  before ->
    @profile_year_input = val: ->
    @profile_month_input = val: ->
    @profile_day_input = val: ->
    @val = val: ->
    @fields = css: ->
    @responsible_name = prop: ->
    @responsible_id = prop: ->
    @clock = sinon.useFakeTimers(new Date(2016, 0, 1).getTime())

    sinon.stub(window, '$')
    $.withArgs('#profile_year').returns(@profile_year_input)
    $.withArgs('#profile_month').returns(@profile_month_input)
    $.withArgs('#profile_day').returns(@profile_day_input)
    $.withArgs('#responsible_fields').returns(@fields)
    $.withArgs('#user-responsible-name').returns(@responsible_name)
    $.withArgs('#user-responsible-id-number').returns(@responsible_id)

    @css_spy_mock = sinon.mock(@fields)
    @name_prop_mock = sinon.mock(@responsible_name)
    @id_prop_mock = sinon.mock(@responsible_id)

  describe 'when the user is younger than 18', ->
    before ->
      @birth_year = 1999
      @birth_month = 12
      @birth_day = 31
      @profile_year_stub = sinon.stub(@profile_year_input, 'val').returns(@birth_year)
      @profile_month_stub = sinon.stub(@profile_month_input, 'val').returns(@birth_month)
      @profile_day_stub = sinon.stub(@profile_day_input, 'val').returns(@birth_day)

    it 'shows the responsible fields', ->
      @css_spy_mock.expects('css').once().withArgs('display', 'block')
      @name_prop_mock.expects('prop').once().withArgs('required', true)
      @id_prop_mock.expects('prop').once().withArgs('required', true)

      responsible_fields()

      sinon.assert.called(@profile_year_stub)
      sinon.assert.called(@profile_month_stub)
      sinon.assert.called(@profile_day_stub)

    after ->
      @profile_year_input.val.restore()
      @profile_month_input.val.restore()
      @profile_day_input.val.restore()

  describe 'when the user is older than 18', ->
    before ->
      @birth_year = 1998
      @birth_month = 1
      @birth_day = 1
      @profile_year_stub = sinon.stub(@profile_year_input, 'val').returns(@birth_year)
      @profile_month_stub = sinon.stub(@profile_month_input, 'val').returns(@birth_month)
      @profile_day_stub = sinon.stub(@profile_day_input, 'val').returns(@birth_day)

    it 'shows the responsible fields', ->
      @css_spy_mock.expects('css').once().withArgs('display', 'none')
      @name_prop_mock.expects('prop').once().withArgs('required', false)
      @id_prop_mock.expects('prop').once().withArgs('required', false)

      responsible_fields()

      sinon.assert.called(@profile_year_stub)
      sinon.assert.called(@profile_month_stub)
      sinon.assert.called(@profile_day_stub)

    after ->
      @profile_year_input.val.restore()
      @profile_month_input.val.restore()
      @profile_day_input.val.restore()

  after ->
    @css_spy_mock.verify()
    @name_prop_mock.verify()
    @id_prop_mock.verify()
    $.restore()
    @clock.restore()
