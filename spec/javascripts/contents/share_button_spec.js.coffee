#= require spec_helper
#= require contents/share_button

describe 'share_button', ->
  before ->
    sinon.stub(window, '$')

  after ->
    $.restore()

  describe 'ShareButtonPluginBuilder', ->
    before ->
      sinon.stub(videojs, 'plugin')
      @share_button_html = '<html></html>'
      @share_box_html = '<html></html>'

    after ->
      videojs.plugin.restore()

    describe 'constructor', ->
      it 'is expected to register a new videojs plugin', ->
        plugin = new ShareButtonPluginBuilder(@share_button_html, @share_box_html)
        sinon.assert.called(videojs.plugin)

    describe 'shareButton', ->
      before ->
        @share_button_wrapper = {}
        @share_box_wrapper = {}
        @create_element_stub = sinon.stub(document, 'createElement')
          .onFirstCall().returns(@share_button_wrapper)
          .onSecondCall().returns(@share_box_wrapper)
        @plugin = new ShareButtonPluginBuilder(@share_button_html, @share_box_html)

        element = { appendChild: (event) -> }
        @append_child_stub = sinon.stub(element, 'appendChild')
        @plugin.el = -> element

      after ->
        document.createElement.restore()

      it 'is expected to add the share button HTML on videojs', ->
        @plugin.shareButton({ plugin_builder: @plugin })

        expect(@share_button_wrapper.innerHTML).to.equal(@share_button_html)
        expect(@share_box_wrapper.innerHTML).to.equal(@share_box_html)
        sinon.assert.calledWith(@append_child_stub, @share_button_wrapper)
        sinon.assert.calledWith(@append_child_stub, @share_box_wrapper)
