#= require spec_helper
#= require contents/toggle_upload_animation

describe 'toggle_upload_animation', ->
  before ->
    sinon.stub(window, 'toggle_display_element').withArgs('loader', 'block')

  it 'is expected to toggle the loader div', ->
    toggle_upload_animation(Object())

    sinon.assert.called(toggle_display_element)

  after ->
    toggle_display_element.restore()
