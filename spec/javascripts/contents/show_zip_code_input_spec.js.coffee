#= require spec_helper
#= require contents/show_zip_code_input

describe 'show_zip_code_input', ->
  before ->
    @first_mock = is: ->
    @location = {
      css: ->,
      prop: ->
    }
    @location_inputs = first: ->

    sinon.stub(window, '$')
    $.withArgs('input[name=video-has-location]').returns(@location_inputs)
    $.withArgs('#video-location').returns(@location)

    @location_mock = sinon.mock(@location)
    @first_stub = sinon.stub(@location_inputs, 'first').returns(@first_mock)

  describe 'when the location input is checked', ->
    before ->
      @checked = true
      @is_stub = sinon.stub(@first_mock, 'is').withArgs(':checked').returns(@checked)

    it 'shows the location fields', ->
      @location_mock.expects('css').once().withArgs('display', 'block')
      @location_mock.expects('prop').once().withArgs('required', true)

      video_zip_code()

      sinon.assert.called(@first_stub)
      sinon.assert.called(@is_stub)

    after ->
      @first_mock.is.restore()

  describe 'when the location input is not checked', ->
    before ->
      @checked = false
      @is_stub = sinon.stub(@first_mock, 'is').withArgs(':checked').returns(@checked)

    it 'hides the location fields', ->
      @location_mock.expects('css').once().withArgs('display', 'none')
      @location_mock.expects('prop').once().withArgs('required', false)

      video_zip_code()

      sinon.assert.called(@first_stub)
      sinon.assert.called(@is_stub)

    after ->
      @first_mock.is.restore()

  after ->
    @location_mock.verify()
    $.restore()
