#= require spec_helper
#= require contents/public_rating

describe 'public_rating', ->
  before ->
    sinon.stub(window, '$')

  after ->
    $.restore()

  describe 'addCallbackToPublicRatingForm', ->
    before ->
      @callback_function_stub = sinon.stub(window, 'changePublicRatingValueAndMessage')
      @public_rating_form_mock = on: ->
      @on_stub = sinon.stub(@public_rating_form_mock, 'on').withArgs('ajax:success', @callback_function_stub)
      $.withArgs('#public_rating_form').returns(@public_rating_form_mock)
    after ->
      changePublicRatingValueAndMessage.restore()

    it 'adds a callback to the public rating form on ajax success', ->
      addCallbackToPublicRatingForm()
      sinon.assert.called(@on_stub)

  describe 'changePublicRatingValueAndMessage', ->
    before ->
      @response_data =
        message: 'Message'
        grade: 4
      @rateit_range_mock = attr: ->
      @public_rating_message_mock = text: ->
      $.withArgs('.rateit-range').returns(@rateit_range_mock)
      $.withArgs('#public_rating_message').returns(@public_rating_message_mock)
      @attr_stub = sinon.stub(@rateit_range_mock, 'attr').withArgs(sinon.match(/value/), @response_data['grade'])

    it 'changes the value of the rateit-range to the received value', ->
      changePublicRatingValueAndMessage(null, @response_data, null, null)
      sinon.assert.called(@attr_stub)

    it 'changes the public rating message to the received message', ->
      @text_stub = sinon.stub(@public_rating_message_mock, 'text').withArgs(@response_data['message'])
      changePublicRatingValueAndMessage(null, @response_data, null, null)
      sinon.assert.called(@text_stub)
