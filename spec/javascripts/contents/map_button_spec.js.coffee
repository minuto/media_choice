#= require spec_helper
#= require contents/map_button

describe 'map_button', ->
  before ->
    sinon.stub(window, '$')

  after ->
    $.restore()

  describe 'MapButtonPluginBuilder', ->
    before ->
      sinon.stub(videojs, 'plugin')
      @map_button_html = '<html></html>'

    after ->
      videojs.plugin.restore()

    describe 'constructor', ->
      it 'is expected to register a new videojs plugin', ->
        plugin = new MapButtonPluginBuilder(@map_button_html)
        sinon.assert.called(videojs.plugin)

    describe 'mapButton', ->
      before ->
        @map_button_wrapper = {}
        @create_element_stub = sinon.stub(document, 'createElement')
          .returns(@map_button_wrapper)
        @plugin = new MapButtonPluginBuilder(@map_button_html)

        element = { appendChild: (event) -> }
        @append_child_stub = sinon.stub(element, 'appendChild')
        @plugin.el = -> element

      after ->
        document.createElement.restore()

      it 'is expected to add the map button HTML on videojs', ->
        @plugin.mapButton({ plugin_builder: @plugin })

        expect(@map_button_wrapper.innerHTML).to.equal(@map_button_html)
        sinon.assert.calledWith(@append_child_stub, @map_button_wrapper)
