#= require spec_helper
#= require common

describe 'toggle_display_element', ->
  before ->
    sinon.stub(window, '$')
    @id = 'object_id'
    @object = document.createElement('div')
    @new_display_type = 'none'
    @jquery_call = $.withArgs('#' + @id).returns([@object])

  after ->
    $.restore()

  it 'adds a callback to the public rating form on ajax success', ->
    toggle_display_element(@id, @new_display_type)
    sinon.assert.called(@jquery_call)
    expect(@object.style.display).to.equal(@new_display_type)
