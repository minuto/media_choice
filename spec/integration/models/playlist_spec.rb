require 'integration_helper'

RSpec.describe Playlist do
  describe 'scopes' do
    describe 'visible' do
      let(:visible_playlist) { create(:playlist, visible: true) }
      let(:invisible_playlist) { create(:playlist, visible: false) }

      context 'with visible and not visible playlists' do
        it 'returns only the visible playlists' do
          visible_playlists = described_class.visible
          expect(visible_playlists).to include(visible_playlist)
          expect(visible_playlists).to_not include(invisible_playlist)
        end
      end
    end
  end
end
