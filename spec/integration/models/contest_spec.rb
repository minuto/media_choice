require 'integration_helper'

RSpec.describe Contest do
  before do
    Content.skip_callback(:save, :after, :update_coordinates)
  end

  after do
    Content.set_callback(:save, :after, :update_coordinates)
  end

  describe 'methods' do
    describe 'finished_contents' do
      subject { create :contest }
      let(:user) { create :user }
      let!(:profile) { create :profile, user_id: user.id }
      let(:contents) {
        [create(:content_with_original_medias, user_id: user.id, contests: [subject]),
         create(:content_with_original_medias, user_id: user.id, contests: [subject]),
         create(:content_with_original_medias, user_id: user.id)]
      }
      let!(:encoded_contents) {
        [create(:encoded_video, content_id: contents.first.id, state: 'finished'),
         create(:encoded_video, content_id: contents.second.id, state: nil),
         create(:encoded_video, content_id: contents.last.id, state: 'finished')]
      }

      before do
        contents.each { |c| c.judge_evaluation.update(evaluation_state: 'evaluated') }
      end

      it 'is expected to return only the contents from this contest with finished encoded contents' do
        expect(subject.finished_contents).to include(contents.first)
      end
    end
  end

  describe 'contents_to_show' do
    subject { create :contest }
    let(:user) { create :user }
    let!(:profile) { create :profile, user_id: user.id }
    let(:contents) { create_list(:content_with_original_medias, 5, user_id: user.id, contests: [subject]) }

    before do
      contents.map do |content|
        create(:encoded_video, content_id: content.id, state: 'finished')
      end
      contents[0].judge_evaluation.update(judge_rating: 5, evaluation_state: 'evaluated')
      contents[1].judge_evaluation.update(judge_rating: 1, evaluation_state: 'evaluated')
      contents[2].judge_evaluation.update(judge_rating: 3, evaluation_state: 'evaluated')
      contents[3].judge_evaluation.update(judge_rating: 5, evaluation_state: 'evaluated')
      contents[4].judge_evaluation.update(judge_rating: 1, evaluation_state: 'evaluated')
      contents[0].update(created_at: Time.zone.today)
      contents[1].update(created_at: Time.zone.yesterday)
      contents[2].update(created_at: Time.zone.tomorrow)
      contents[3].update(created_at: Time.zone.yesterday)
      contents[4].update(created_at: Time.zone.today)
    end

    it 'is expected to return the finished contents ordered by their judge ratings and creation date' do
      response = [contents[0], contents[3], contents[2], contents[4], contents[1]]
      expect(subject.contents_to_show).to eq(response)
    end
  end

  describe 'class methods' do
    describe 'started' do
      context 'with contests on the database' do
        let!(:contest_1) { create(:contest, start_date: Time.zone.yesterday) }
        let!(:contest_2) { create(:contest, start_date: Time.zone.tomorrow) }
        let!(:contest_3) { create(:contest, start_date: Time.zone.yesterday - 1, end_date: Time.zone.yesterday) }
        let!(:contest_4) { create(:contest, start_date: Time.zone.today, end_date: Time.zone.tomorrow) }
        let!(:contest_5) { create(:contest, start_date: Time.zone.today, end_date: Time.zone.today) }

        it 'returns the ongoing contests' do
          expect(described_class.started).to include(contest_1, contest_4, contest_5)
          expect(described_class.started).to_not include(contest_2, contest_3)
        end
      end

      context 'without contests on the database' do
        it 'returns an empty array' do
          expect(described_class.started).to be_empty
        end
      end
    end

    describe 'finished' do
      context 'with contests on the database' do
        let!(:contest_1) { create(:contest, start_date: Time.zone.yesterday) }
        let!(:contest_2) { create(:contest, start_date: Time.zone.tomorrow) }
        let!(:contest_3) { create(:contest, start_date: Time.zone.yesterday - 1, end_date: Time.zone.yesterday) }
        let!(:contest_4) { create(:contest, start_date: Time.zone.today, end_date: Time.zone.tomorrow) }
        let!(:contest_5) { create(:contest, start_date: Time.zone.today, end_date: Time.zone.today) }

        it 'returns the contests that have already finished' do
          expect(described_class.finished).to include(contest_3)
          expect(described_class.finished).to_not include(contest_1, contest_2, contest_5, contest_4)
        end
      end

      context 'without finished contests' do
        it 'returns an empty array' do
          expect(described_class.finished).to be_empty
        end
      end
    end
  end
end
