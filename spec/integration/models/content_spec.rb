require 'integration_helper'

describe Content do
  before do
    Content.skip_callback(:save, :after, :update_coordinates)
  end

  after do
    Content.set_callback(:save, :after, :update_coordinates)
  end

  describe 'methods' do
    describe 'finished_encoded_contents' do
      context 'with finished encoded contents' do
        let(:user) { create :user }
        let!(:profile) { create :profile, user_id: user.id }
        subject { create :content_with_original_medias, user_id: user.id }
        let(:finished_encoded_content) { create(:encoded_video, state: :finished, content_id: subject.id) }
        let(:unfinished_encoded_content) { create(:encoded_video, state: nil, content_id: subject.id) }
        let(:encoded_contents) { [finished_encoded_content, unfinished_encoded_content] }

        before do
          user.confirm
        end

        it 'returns the finished encoded contents' do
          expect(subject.finished_encoded_contents).to include finished_encoded_content
        end
      end

      context 'without finished encoded contents' do
        let(:user) { create :user }
        let!(:profile) { create :profile, user_id: user.id }
        subject { create :content_with_original_medias, user_id: user.id }

        it 'returns an empty array' do
          expect(subject.finished_encoded_contents).to be_empty
        end
      end
    end

    describe 'states' do
      let(:user) { create :user }
      let!(:profile) { create :profile, user_id: user.id }
      subject { create :content_with_original_medias, user_id: user.id }

      context 'initial state' do
        it 'is pending' do
          expect(subject.evaluation_state).to eq 'pending'
        end
      end

      context 'state after sending to evaluation' do
        before do
          expect(EvaluationSystem).to receive(:request_evaluation).with(subject)
          subject.send_to_evaluation
        end

        it 'is evaluating' do
          expect(subject.evaluation_state).to eq 'evaluating'
        end
      end

      context 'state after evaluating' do
        before do
          expect(EvaluationSystem).to receive(:request_evaluation).with(subject)
          subject.send_to_evaluation
          subject.label_evaluated
        end

        it 'is evaluated' do
          expect(subject.evaluation_state).to eq 'evaluated'
        end
      end
    end
  end

  describe 'scope' do
    let!(:user) { create :user }
    let!(:profile) { create :profile, user_id: user.id }

    describe 'encoding job related' do
      let!(:content_with_encoding) { create :content_with_original_medias, user_id: user.id }
      let!(:content_with_unfinished_encoding) { create :content_with_original_medias, user_id: user.id }
      let!(:content_without_encoding) { create :content_with_original_medias, user_id: user.id }
      let!(:finished_encoded_content) { create(:encoded_video, state: :finished, content_id: content_with_encoding.id) }
      let!(:unfinished_encoded_content) { create(:encoded_video, state: nil, content_id: content_with_unfinished_encoding.id) }

      describe 'finished' do
        it 'returns only finished encoded contents' do
          expect(Content.finished).to include(content_with_encoding)
          expect(Content.finished).to_not include(content_with_unfinished_encoding)
          expect(Content.finished).to_not include(content_without_encoding)
        end
      end

      describe 'unfinished' do
        it 'returns only unfinished encoded contents' do
          expect(Content.unfinished).to_not include(content_with_encoding)
          expect(Content.unfinished).to include(content_with_unfinished_encoding)
          expect(Content.unfinished).to include(content_without_encoding)
        end
      end
    end

    describe 'evaluation_related' do
      let(:pending_content) { create :content_with_original_medias, user_id: user.id }
      let(:evaluating_content) { create :content_with_original_medias, user_id: user.id }
      let(:evaluated_content) { create :content_with_original_medias, user_id: user.id }

      before do
        pending_content.judge_evaluation.update(evaluation_state: 'pending')
        evaluating_content.judge_evaluation.update(evaluation_state: 'evaluating')
        evaluated_content.judge_evaluation.update(evaluation_state: 'evaluated')
      end

      describe 'evaluated' do
        it 'returns only evaluated contents' do
          expect(Content.evaluated).to include(evaluated_content)
          expect(Content.evaluated).to_not include(pending_content)
          expect(Content.evaluated).to_not include(evaluating_content)
        end
      end

      describe 'not_evaluated' do
        it 'returns only non evaluated contents' do
          expect(Content.not_evaluated).to_not include(evaluated_content)
          expect(Content.not_evaluated).to include(pending_content)
          expect(Content.not_evaluated).to include(evaluating_content)
        end
      end
    end

    describe 'with_found_coordinates' do
      let!(:content_with_coordinates) { create :content_with_original_medias, user_id: user.id }
      let!(:content_without_coordinates) { create :content_with_original_medias, user_id: user.id }
      let!(:found_coordinate) { create :coordinate, found: true, content: content_with_coordinates }
      let!(:not_found_coordinate) { create :coordinate, found: false, content: content_without_coordinates }

      it 'returns only contents with found coordinates' do
        expect(Content.with_found_coordinates).to include(content_with_coordinates)
        expect(Content.with_found_coordinates).to_not include(content_without_coordinates)
      end
    end

    describe 'featured' do
      let!(:featured_content) { create :content_with_original_medias, user_id: user.id }
      let!(:not_featured_content) { create :content_with_original_medias, user_id: user.id }

      before do
        featured_content.judge_evaluation.update(judge_rating: 4)
        not_featured_content.judge_evaluation.update(judge_rating: 3)
      end

      it 'returns only featured contents' do
        expect(Content.featured).to include(featured_content)
        expect(Content.featured).to_not include(not_featured_content)
      end
    end

    describe 'recent' do
      let(:contents) do
        [
          create(:content_with_original_medias, user: user, created_at: Time.zone.tomorrow),
          create(:content_with_original_medias, user: user, created_at: Time.zone.today),
          create(:content_with_original_medias, user: user, created_at: Time.zone.yesterday),
          create(:content_with_original_medias, user: user, created_at: Time.zone.today)
        ]
      end

      before do
        contents.each do |content|
          create(:encoded_video, state: :finished, content_id: content.id)
          content.judge_evaluation.update(evaluation_state: 'evaluated')
        end
      end

      it 'returns the most recent finished and evaluated contents' do
        expect(described_class.recent).to eq contents.sort { |c1, c2| c2.created_at <=> c1.created_at }
      end
    end
  end

  describe 'related_contents' do
    let(:user) { create :user }
    let!(:profile) { create :profile, user_id: user.id }
    let(:contest_1) { create(:contest) }
    let(:contest_2) { create(:contest) }
    let(:contests) { [contest_1, contest_2] }
    let(:other_contest) { [create(:contest)] }
    subject { create(:video, user: user, contests: contests) }

    before do
      Rails.cache.clear
    end

    after do
      Rails.cache.clear
    end

    test_cases = (1..10).map do
      number_of_contents_same_contest = 50
      number_of_contents_other_contest = 50
      number_of_featured_contents_same_contest = rand(number_of_contents_same_contest)
      number_of_featured_contents_other_contest = rand(number_of_contents_other_contest)
      number_of_additional_contents = number_of_featured_contents_same_contest >= Content::RELATED_CONTENTS_LIST_SIZE ? 0 : [Content::RELATED_CONTENTS_LIST_SIZE - number_of_featured_contents_same_contest, number_of_featured_contents_other_contest].min
      {
        number_of_featured_contents_same_contest: number_of_featured_contents_same_contest,
        number_of_featured_contents_other_contest: number_of_featured_contents_other_contest,
        number_of_not_featured_contents_same_contest: number_of_contents_same_contest - number_of_featured_contents_same_contest,
        number_of_not_featured_contents_other_contest: number_of_contents_other_contest - number_of_featured_contents_other_contest,
        number_of_additional_contents: number_of_additional_contents,
        list_size: number_of_featured_contents_same_contest + number_of_additional_contents
      }
    end

    test_cases.each do |test_case|
      context "Same contest: #{test_case[:number_of_featured_contents_same_contest]} featured and #{test_case[:number_of_not_featured_contents_same_contest]} not featured. Other contest: #{test_case[:number_of_featured_contents_other_contest]} featured and #{test_case[:number_of_not_featured_contents_other_contest]} not featured" do
        it "is expected to create a related contents list with #{test_case[:list_size]} elements of which #{test_case[:number_of_featured_contents_same_contest]} are featured and in the same contest as the subject and #{test_case[:number_of_additional_contents]} are additional contents" do
          # setup
          featured_same_contest_contents = create_list(:video, test_case[:number_of_featured_contents_same_contest], contests: contests, user: user)
          not_featured_same_contest_contents = create_list(:video, test_case[:number_of_not_featured_contents_same_contest], contests: contests, user: user)
          featured_other_contest_contents = create_list(:video, test_case[:number_of_featured_contents_other_contest], contests: other_contest, user: user)
          not_featured_other_contest_contents = create_list(:video, test_case[:number_of_not_featured_contents_other_contest], contests: other_contest, user: user)

          # update judge evaluations
          subject.judge_evaluation.update(judge_rating: 5, evaluation_state: 'evaluated')
          featured_same_contest_contents.each { |content| content.judge_evaluation.update(judge_rating: 5, evaluation_state: 'evaluated') }
          not_featured_same_contest_contents.each { |content| content.judge_evaluation.update(judge_rating: 2, evaluation_state: 'evaluated') }
          featured_other_contest_contents.each { |content| content.judge_evaluation.update(judge_rating: 5, evaluation_state: 'evaluated') }
          not_featured_other_contest_contents.each { |content| content.judge_evaluation.update(judge_rating: 2, evaluation_state: 'evaluated') }

          # exercise
          related_contents_list = subject.related_contents

          # contents that should NEVER be included
          expect(related_contents_list).to_not include(subject)
          not_featured_same_contest_contents.each do |content|
            expect(related_contents_list).to_not include(content)
          end
          not_featured_other_contest_contents.each do |content|
            expect(related_contents_list).to_not include(content)
          end

          # contents that should ALWAYS be included
          featured_same_contest_contents.each do |content|
            expect(related_contents_list).to include(content)
          end

          # here we levarage the creation order
          included_additional_contests = 0
          featured_other_contest_contents.each do |content|
            included_additional_contests += 1 if related_contents_list.include? content
          end
          expect(included_additional_contests).to eq(test_case[:number_of_additional_contents])

          expect(related_contents_list.size).to eq(test_case[:list_size])
        end
      end
    end
  end
end
