require 'integration_helper'

describe User do
  describe 'scopes' do
    describe 'confirmed' do
      let(:confirmed_user) { create(:user, confirmed_at: Time.zone.now) }
      let(:unconfirmed_user) { create(:user) }

      it 'returns only the confirmed users' do
        confirmed = described_class.confirmed
        expect(confirmed).to include confirmed_user
        expect(confirmed).to_not include unconfirmed_user
      end
    end

    describe 'mailing_list' do
      let(:email_locale) { 'pt-BR' }
      let(:user_1) { create(:user, confirmed_at: Time.zone.now, profile: create(:profile, receive_mass_email: true, preferred_language: 'pt-BR')) } # Will have the correct locale and want to receive emails
      let(:user_2) { create(:user, confirmed_at: Time.zone.now, profile: create(:profile, receive_mass_email: false, preferred_language: 'pt-BR')) } # Won't want to receive emails
      let(:user_3) { create(:user, confirmed_at: Time.zone.now, profile: create(:profile, receive_mass_email: true, preferred_language: 'en')) } # Will want to receive email but won't have the correct locale

      it 'only returns the users that wanto to receive emails and have the correct locale' do
        email_list = described_class.mailing_list(email_locale)
        expect(email_list).to include user_1
        expect(email_list).to_not include user_2
        expect(email_list).to_not include user_3
      end
    end
  end
end
