require 'integration_helper'

describe ViewCounterJob do
  describe 'perform' do
    # Populate with content views
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user_id: user.id) }
    let(:content_number) { (1..10).to_a.sample }
    let(:contents) { create_list(:content_with_original_medias, content_number, user: user) }
    let!(:content_views) do
      contents.map do |content|
        count = (1..10).to_a.sample
        create_list(:content_view, count, model_id: content.id)
        [count, content]
      end
    end

    # Populate with sponsor views
    let(:sponsor_number) { (1..10).to_a.sample }
    let(:sponsors) { create_list(:sponsor, sponsor_number, :with_logo_filename) }
    let!(:sponsor_views) do
      sponsors.map do |sponsor|
        count = (1..10).to_a.sample
        create_list(:sponsor_view, count, model_id: sponsor.id)
        [count, sponsor]
      end
    end
    let!(:view_counter_of_nonexistent_model) { create(:sponsor_view, model_id: 0) }
    let(:new_job) { double(described_class) }

    it 'increments the view count of all models that count views, ignores not found models and removes all view counters' do
      expect(described_class).to receive(:set).and_return new_job
      expect(new_job).to receive(:perform_later)
      described_class.perform_now
      # Check if content views were correctly updated
      content_views.each do |views, content|
        expect(content.reload.view_count).to eq(views)
      end

      # Check if sponsor views were correctly updated
      sponsor_views.each do |views, sponsor|
        expect(sponsor.reload.display_count).to eq(views)
      end

      expect(ViewCounter.all).to be_empty
      expect { ViewCounterJob }.to_not raise_error
    end
  end
end
