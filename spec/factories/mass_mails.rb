FactoryGirl.define do
  factory :mass_mail do
    sequence(:id, 1)
    subject 'Mussum Ipsum, cacilds vidis litro abertis'
    body 'Mussum Ipsum, cacilds vidis litro abertis. Mé faiz elementum girarzis, nisi eros vermeio. Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose. Quem num gosta di mé, boa gente num é. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis.'
    locale 'pt-BR'
    status 'pending'
  end

  factory :other_mass_mail, class: MassMail do
    subject 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
    body 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur congue ligula et lorem dapibus, quis molestie arcu laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec urna ante, pellentesque tristique lorem id, gravida finibus massa. Pellentesque sit amet justo eu ipsum euismod dignissim. Nullam eu sodales lectus. Vestibulum egestas posuere tincidunt. Nullam ac nunc tempus, efficitur mauris vitae, tincidunt risus. Morbi vestibulum lacus vitae viverra dictum. Phasellus iaculis vehicula pharetra.'
    locale 'en'
  end
end
