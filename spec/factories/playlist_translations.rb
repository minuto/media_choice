FactoryGirl.define do
  factory :playlist_translation do
    name 'Playlist'
    locale 'pt-BR'
  end

  factory :another_playlist_translation, class: Playlist do
    name 'Another Playlist'
    locale 'en'
  end
end
