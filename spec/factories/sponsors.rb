FactoryGirl.define do
  factory :sponsor do
    sequence(:id, 1)
    display_count 0
    address 'www.example.org'
    name 'sponsor'

    trait :with_logo_filename do
      logo_file_name { Rails.root.join('features', 'support', 'fixtures', 'images', 'sponsor.jpg').to_s }
    end

    trait :with_logo do
      logo { File.new Rails.root.join('features', 'support', 'fixtures', 'images', 'sponsor.jpg') }
    end
  end
end
