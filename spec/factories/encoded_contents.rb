FactoryGirl.define do
  factory :encoded_video, class: EncodedContent do
    sequence(:id, 1)
    output_id 1
    state 'finished'
    width 640
    height 320
    duration 5000
    file_size 1024
    sequence(:job_id, 1)
  end
end
