FactoryGirl.define do
  factory :coordinate do
    latitude 10.0
    longitude 10.0
    found true
    association :content, factory: :video, strategy: :build
  end
end
