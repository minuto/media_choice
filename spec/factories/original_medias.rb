FactoryGirl.define do
  factory :original_media do
    sequence(:id, 1)
  end

  trait :zencoder do
    media { File.new Rails.root.join('features', 'support', 'fixtures', 'videos', 'video.mp4') }
  end

  trait :with_file_name do
    media_file_name Rails.root.join('features', 'support', 'fixtures', 'videos', 'video.mp4').to_s
  end
end
