FactoryGirl.define do
  factory :user, class: User do
    sequence(:id, 1)
    sequence :email do |n|
      "email#{n}@email.com"
    end
    password 'password'
    admin false

    trait :admin do
      admin true
    end

    trait :adult_user do
      association :profile, factory: :adult_profile, strategy: :build
    end
  end
end
