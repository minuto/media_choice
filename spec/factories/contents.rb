FactoryGirl.define do
  factory :video, class: Content do
    sequence(:id, 1)
    title 'Video Title'
    zip_code '05508-090'
    soundtrack 'soundtrack'
    director 'Director'
    association :judge_evaluation, factory: :judge_evaluation, strategy: :build
    transient do
      original_medias_count 1
    end

    factory :content_with_original_medias do
      after(:create) do |content, evaluator|
        create_list(:original_media, evaluator.original_medias_count, :zencoder, content: content)
      end
    end

    factory :content_with_original_medias_file_names do
      after(:create) do |content, evaluator|
        create_list(:original_media, evaluator.original_medias_count, :with_file_name, content: content)
      end
    end
  end
end
