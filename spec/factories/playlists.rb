FactoryGirl.define do
  factory :playlist do
    visible true

    after(:create) do |playlist|
      playlist.playlist_translations = build_list(:playlist_translation, 2)
    end
  end

  factory :another_playlist, class: Playlist do
    name 'Another Playlist'
    locale 'en'
  end
end
