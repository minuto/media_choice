FactoryGirl.define do
  factory :content_view, class: ViewCounter do
    sequence(:model_id, 1)
    viewed_model Content.to_s
  end

  factory :sponsor_view, class: ViewCounter do
    sequence(:model_id, 1)
    viewed_model Sponsor.to_s
  end
end
