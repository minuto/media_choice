FactoryGirl.define do
  factory :contest do
    sequence(:id, 1)
    start_date Time.zone.today
  end
end
