FactoryGirl.define do
  factory :contest_translation do
    locale :en
    name 'Contest Name'
    description 'Contest Description'

    trait :with_file_name do
      logo_file_name "#{Rails.root}/app/assets/images/logo.jpg"
    end

    trait :with_file do
      logo { File.new "#{Rails.root}/app/assets/images/logo.jpg" }
    end
  end
end
