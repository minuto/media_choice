FactoryGirl.define do
  factory :public_rating do
    association :content, factory: :video, strategy: :build
    association :user, strategy: :build
    grade 3.5
  end
end
