FactoryGirl.define do
  factory :profile do
    sequence(:id, 1)
    first_name 'Media'
    country_numeric_code '076'
    city 'Sao Paulo'
    responsible_document '918238910'
    birthdate Time.zone.today - 12.years
    responsible_name 'Responsible'
    gender 2
    last_name 'Choice User'
    school_type 3
    school 'Some School'
    phone '912345678'
    state 'São Paulo'
    terms_of_service '1'
    association :user, strategy: :build
    zip_code '09898-098'
    occupation 2
    preferred_language 'pt-BR'
    address 'Some place'

    factory :another_profile do
      first_name 'Name'
      city 'Rio de Janeiro'
      responsible_document '532221420'
      birthdate Date.yesterday
      responsible_name 'Name'
      gender 1
      last_name 'Last'
      school 'School'
      phone '012246654'
      state 'Rio de Janeiro'
      occupation 8
      preferred_language 'pt-BR'
    end

    factory :adult_profile do
      birthdate Date.yesterday - 18.years
    end
  end
end
