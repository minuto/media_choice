FactoryGirl.define do
  factory :home_highlight do
    sequence(:id, 1)
    link 'https://www.google.com'
    visible true

    trait :with_file_name do
      banner_file_name Rails.root.join('app', 'assets', 'images', 'logo.jpg').to_s
    end

    trait :with_file do
      banner { File.new Rails.root.join('app', 'assets', 'images', 'logo.jpg').to_s }
    end
  end
end
