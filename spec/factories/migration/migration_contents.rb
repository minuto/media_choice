FactoryGirl.define do
  factory :migration_content, class: OpenStruct do
    skip_create

    sequence(:id, 1)
    input_file_name 'input.flv'
    input_content_type 'video/x-flv'
    input_file_size 20_747
    input_updated_at '2013-04-28 012652'
    created_at '2009-09-10 165226'
    updated_at '2013-05-29 122041'
    job_id 206_100
    upload_file_name nil
    upload_content_type nil
    upload_file_size nil
    upload_updated_at nil
    state 'finished'
    lock_version 27
    title 'Content title'
    description 'Content description'
    user_id 1
    highlight nil
    evaluation_state 'evaluated'
    manual_featured nil
    adult false
    rating 5
    soundtrack 'Information missing'
    type 'Video'
    view_count 1337
    old_id nil
    removal_state 'non_removed'
    allow_comments true
    average_votes_rounded 2
    highlight_order 10
    city nil
    zip_code nil
  end
end
