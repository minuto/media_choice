FactoryGirl.define do
  factory :migration_contest, class: OpenStruct do
    skip_create

    sequence(:id, 1)
    logo_file_name nil
    logo_content_type nil
    logo_file_size nil
    logo_updated_at nil
    created_at Time.zone.now
    updated_at Time.zone.now
    start_date Time.zone.now
    end_date Time.zone.now
    always_visible nil
    parent_id nil
    lft nil
    rgt nil
    order nil
    campaign_id nil
    old_id nil
    show_in_upload false
    video_max_size 0
    audio_max_size 0
    photo_max_size 0
    wording_min_chars 0
    wording_max_chars 0
    video_min_duration_in_s 0
    video_max_duration_in_s 0
    audio_min_duration_in_s 0
    audio_max_duration_in_s 0
    hidden false
    display_after_end false
    type 'CuratorshipContest'
    open_voting false
    order_sponsors false
    home_cover nil
    home_highlight nil
    home_cover_order nil
    home_highlight_order nil
    require_city_on_upload nil
    festival false
  end
end
