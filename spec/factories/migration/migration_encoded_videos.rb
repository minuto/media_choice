FactoryGirl.define do
  factory :migration_encoded_video, class: OpenStruct do
    skip_create

    sequence(:id, 1)
    content_id 1
    output_id 1
    created_at '2009-09-10 165226'
    updated_at '2013-05-29 122041'
    width 10
    height 10
    state 'ready'
    video_file_name 'input.flv'
    video_content_type 'video/x-flv'
    video_file_size 20_747
    video_updated_at '2013-04-28 012652'
    duration 100
    job_id 123
  end
end
