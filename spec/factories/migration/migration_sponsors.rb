FactoryGirl.define do
  factory :migration_sponsor, class: OpenStruct do
    skip_create

    sequence(:id, 1)
    name 'Sponsor'
    logo_file_name 'Name'
    logo_content_type 'Content Type'
    logo_file_size 100
    logo_updated_at Time.zone.now
    created_at '2008-09-08 20:43:05'
    updated_at '2012-06-13 21:20:07'
    impressions 1000
    url 'http://internet.com'
    order 2
  end
end
