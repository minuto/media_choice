FactoryGirl.define do
  factory :migration_user, class: OpenStruct do
    skip_create

    sequence(:id, 1)
    sequence :email do |n|
      "email#{n}@email.com"
    end
    encrypted_password 'encrypted_password'
    reset_password_token nil
    reset_password_sent_at nil
    remember_created_at nil
    sign_in_count 52
    current_sign_in_at '2016-08-21 01:15:11'
    last_sign_in_at '2016-08-21 01:01:11'
    current_sign_in_ip '111.111.111.11'
    last_sign_in_ip '122.222.222.222'
    confirmation_token nil
    confirmed_at '2008-09-08 20:43:05'
    confirmation_sent_at '2008-09-08 20:43:05'
    created_at '2008-09-08 20:43:05'
    updated_at '2012-06-13 21:20:07'
    first_name 'First'
    last_name 'last'
    birthdate '1970-01-01'
    commercialize_videos true
    gender 'M'
    state 'SP'
    city 'Sao Paulo'
    occupation_id 15
    admin nil
    country 'BR'
    facebook_id ''
    old_id nil
    school ''
    responsible_document ''
    responsible_name ''
    receive_mass_mail true
    phone ''
    facebook_token ''
    facebook_event_publisher_status 'always'
    school_type_id nil
    authentication_token nil
    student false
    unsubscribe_code '39ad92d036d84e3d6638604ee9e6f8a65d4aa8f46ba31f70cb1c4ded5ca10b80'
  end
end
