require 'unit_helper'
require 'concerns/coordinate_searcher'

shared_examples 'Coordinate Searcher' do
  describe 'search_coordinates' do
    let(:address) { 'address' }
    let(:response_mock) { instance_double(Net::HTTPOK) }
    let(:uri_mock) { instance_double(URI::Generic) }

    before do
      expect(URI).to receive(:parse).with(instance_of(String)).and_return uri_mock
      expect(uri_mock).to receive(:query=)
    end

    context 'when the address is not found' do
      let(:response_body) { { results: [], status: 'ZERO_RESULTS' }.to_json }

      before do
        expect(response_mock).to receive(:body).and_return response_body
        expect(Net::HTTP).to receive(:get_response).with(uri_mock).and_return(response_mock)
      end

      it 'returns an empty hash' do
        result = subject.search_coordinates(address)
        expect(result).to be_a Hash
        expect(result).to be_empty
      end
    end

    context 'when the address is found' do
      let(:lat_lng_1) { { lat: -23.5328814, lng: -46.7920029 } }
      let(:lat_lng_2) { { lat: -34.6439925, lng: -57.8031130 } }
      let(:first_result) { { geometry: { location: lat_lng_1 } } }
      let(:second_result) { { geometry: { location: lat_lng_2 } } }
      let(:response_body) { { results: [first_result, second_result], status: 'OK' }.to_json }

      before do
        expect(response_mock).to receive(:body).and_return response_body
        expect(Net::HTTP).to receive(:get_response).with(uri_mock).and_return(response_mock)
      end

      context 'with a city address' do
        let(:type) { :city }
        let(:parameters) { { key: GoogleMapsSettings.key, address: address } }

        it 'is expected to encode the address in the uri' do
          expect(URI).to receive(:encode_www_form).with(parameters)
          subject.search_coordinates(address, type)
        end

        it "returns the first result's coordinates in a hash" do
          result = subject.search_coordinates(address, type)
          expect(result).to eq JSON.parse(lat_lng_1.to_json)
        end
      end

      context 'with a zip code address' do
        let(:type) { :zip_code }
        let(:parameters) { { key: GoogleMapsSettings.key, components: "postal_code: #{address}"} }

        it 'is expected to encode the zip code in the uri' do
          expect(URI).to receive(:encode_www_form).with(parameters)
          subject.search_coordinates(address, type)
        end

        it "returns the first result's coordinates in a hash" do
          result = subject.search_coordinates(address, type)
          expect(result).to eq JSON.parse(lat_lng_1.to_json)
        end
      end
    end
  end
end
