require 'unit_helper'
require 'concerns/support_for_view_counting'

shared_examples 'It Has Support For View Counting' do
  describe 'increase_view_count_by' do
    let(:amount) { (1..10).to_a.sample }
    let(:current_view_count) { (1..10).to_a.sample }
    let(:total_views) { current_view_count + amount }

    it 'is expected to increase the correct attribute with the given amount' do
      expect(subject).to receive(:send).with(subject.view_counter_attribute).and_return current_view_count
      expect(subject).to receive(:update).with("#{subject.view_counter_attribute}": total_views)

      subject.increase_view_count_by(amount)
    end
  end

  describe 'included' do
    it 'is expected to raise a NotImplementedError if the base class does not implement #view_counter_attribute' do
      instance_methods_double = double('instance_methods')
      expect(described_class).to receive(:instance_methods).and_return instance_methods_double
      expect(instance_methods_double).to receive(:include?).with(:view_counter_attribute).and_return false
      expect { SupportForViewCounting.included(described_class) }.to raise_error NotImplementedError
    end

    it 'is expected to do nothing if the base class implement #view_counter_attribute' do
      expect { SupportForViewCounting.included(described_class) }.to_not raise_error
    end
  end
end
