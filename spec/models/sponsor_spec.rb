require 'unit_helper'
require 'models/concerns/support_for_view_counting'

describe Sponsor do
  context 'Included modules' do
    subject { build(:sponsor) }
    it_behaves_like 'It Has Support For View Counting'
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :display_count }
    it { is_expected.to validate_presence_of :name }
  end

  describe 'paperclip attachments' do
    it { is_expected.to have_attached_file(:logo) }
    it { is_expected.to validate_attachment_presence(:logo) }
    it {
      is_expected.to validate_attachment_content_type(:logo)
        .allowing('image/*')
        .rejecting('video/*', 'text/*')
    }
  end

  describe 'relations' do
    it { is_expected.to have_and_belong_to_many(:contests) }
  end

  describe 'method' do
    describe '#view_counter_attribute' do
      it 'is expected to return the model attribute that count views' do
        subject = build(:sponsor)
        expect(subject.view_counter_attribute).to eq('display_count')
      end
    end
  end
end
