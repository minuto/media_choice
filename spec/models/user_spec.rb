require 'unit_helper'

describe User do
  describe 'associations' do
    it { is_expected.to have_one(:profile) }
    it { is_expected.to have_many(:contents) }
    it { is_expected.to have_many(:public_ratings) }
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:first_name).to :profile }
    it { is_expected.to delegate_method(:last_name).to :profile }
    it { is_expected.to delegate_method(:birthdate).to :profile }
    it { is_expected.to delegate_method(:city).to :profile }
  end

  describe 'methods' do
    describe 'locale' do
      let(:user) { build(:user) }

      context 'when the user does not have a profile' do
        it 'is expected to return the current locale' do
          expect(user.locale).to be_a String
          expect(user.locale).to eq(I18n.locale.to_s)
        end
      end

      context 'when the user has a profile' do
        let(:profile) { build(:profile) }

        it 'is expected to return the user preferred language' do
          expect(user.locale).to be_a String
          expect(user).to receive(:profile).at_least(2).and_return profile
          expect(user.locale).to eq profile.preferred_language
        end
      end
    end

    describe 'adult?' do
      let(:user) { build(:user) }
      context 'when the user does not have a profile' do
        it 'is expected to return false' do
          expect(user.adult?).to eq false
        end
      end

      context 'when the user has a profile' do
        let(:profile) { build(:profile) }
        it 'is expected to delegate the method to the profile' do
          expect(user).to receive(:profile).at_least(2).times.and_return profile

          expect(user.adult?).to eq profile.adult?
        end
      end
    end
  end
end
