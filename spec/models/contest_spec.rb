require 'unit_helper'

RSpec.describe Contest, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :start_date }
  end

  describe 'associations' do
    it { is_expected.to have_many(:contest_translations) }
    it { is_expected.to have_and_belong_to_many(:contents) }
    it { is_expected.to have_and_belong_to_many(:sponsors) }
  end

  describe 'methods' do
    subject { build(:contest) }

    describe 'name' do
      let(:locale) { I18n.locale }
      let(:translation_missing_error_message) { I18n.t('missing_contest_translation', field: I18n.t('activerecord.attributes.contest_translation.name'), _locale: locale, id: subject.id) }

      context 'when the contest has no translations' do
        it 'returns a translation missing message' do
          expect(subject.name).to eq(translation_missing_error_message)
        end
      end

      context 'when the contest has translations' do
        let(:locale) { :en }
        let(:translations) { instance_double('translations') }

        before do
          expect(subject).to receive(:contest_translations).and_return translations
        end

        context 'and the contest has no translation with the specified locale' do
          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return nil
          end

          it 'returns nil' do
            expect(subject.name(locale)).to eq translation_missing_error_message
          end
        end

        context 'when the contest has a translation with the specified locale' do
          let(:translation) { build(:contest_translation) }

          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return translation
          end

          it 'returns the name attribute from the translation' do
            expect(subject.name(locale)).to eq translation.name
          end
        end
      end
    end

    describe 'description' do
      let(:locale) { I18n.locale }
      let(:translation_missing_error_message) { I18n.t('missing_contest_translation', field: I18n.t('activerecord.attributes.contest_translation.description'), _locale: locale, id: subject.id) }

      context 'when the contest has no translations' do
        it 'returns a translation missing message' do
          expect(subject.description).to eq(translation_missing_error_message)
        end
      end

      context 'when the contest has translations' do
        let(:locale) { :en }
        let(:translations) { instance_double('translations') }

        before do
          expect(subject).to receive(:contest_translations).and_return translations
        end

        context 'and the contest has no translation with the specified locale' do
          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return nil
          end

          it 'returns nil' do
            expect(subject.description(locale)).to eq translation_missing_error_message
          end
        end

        context 'when the contest has a translation with the specified locale' do
          let(:translation) { build(:contest_translation) }

          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return translation
          end

          it 'returns the description attribute from the translation' do
            expect(subject.description(locale)).to eq translation.description
          end
        end
      end
    end

    describe 'logo_url' do
      context 'when the contest has no translations' do
        let(:asset_path) { '/assets/images/logo.jpg' }
        it 'returns the default logo asset url' do
          # Mocking the method that loads all assets, used inside 'image_tag'. Speeds up the unit test.
          expect(ActionController::Base.helpers).to receive(:image_path).with('logo.jpg').and_return asset_path
          expect(subject.logo_url).to eq asset_path
        end
      end

      context 'when the contest has translations' do
        let(:locale) { :en }
        let(:translations) { instance_double('translations') }

        before do
          expect(subject).to receive(:contest_translations).and_return translations
        end

        context 'and the contest has no translation with the specified locale' do
          let(:asset_path) { '/assets/images/logo.jpg' }
          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return nil
          end

          it 'returns the default logo asset url' do
            # Mocking the method that loads all assets, used inside 'image_tag'. Speeds up the unit test.
            expect(ActionController::Base.helpers).to receive(:image_path).with('logo.jpg').and_return asset_path
            expect(subject.logo_url(locale)).to eq asset_path
          end
        end

        context 'when the contest has a translation with the specified locale' do
          let(:translation) { build(:contest_translation) }

          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return translation
          end

          it 'returns the logo attribute from the translation' do
            expect(subject.logo_url(locale)).to eq translation.logo.url
          end
        end
      end
    end

    describe 'finished?' do
      context 'when the contest has a ending date' do
        context 'with a finished contest' do
          subject { build(:contest, end_date: Time.zone.yesterday) }

          it 'returns true' do
            expect(subject.finished?).to eq true
          end
        end

        context 'and it ends today' do
          subject { build(:contest, end_date: Time.zone.today) }

          it 'is expected to return false' do
            expect(subject.finished?).to eq false
          end
        end
      end

      context 'when the contest has no finish date' do
        subject { build(:contest, end_date: nil) }

        it 'returns false' do
          expect(subject.finished?).to eq false
        end
      end
    end

    describe 'started?' do
      context 'with a started contest' do
        subject { build(:contest, start_date: Time.zone.yesterday - 1, end_date: Time.zone.yesterday) }

        it 'returns true' do
          expect(subject.started?).to eq true
        end
      end

      context "when the contest hasn't started yet" do
        subject { build(:contest, start_date: Time.zone.tomorrow, end_date: Time.zone.tomorrow + 1) }

        it 'returns false' do
          expect(subject.started?).to eq false
        end
      end
    end

    describe 'ongoing?' do
      subject { build(:contest) }

      context 'with an ongoing contest' do
        before do
          expect(subject).to receive(:started?).and_return true
          expect(subject).to receive(:finished?).and_return false
        end

        it 'returns true' do
          expect(subject.ongoing?).to eq true
        end
      end

      context "when the contest hasn't started yet" do
        before do
          expect(subject).to receive(:started?).and_return false
        end

        it 'returns false' do
          expect(subject.ongoing?).to eq false
        end
      end

      context 'when the contest is already finished' do
        before do
          expect(subject).to receive(:started?).and_return true
          expect(subject).to receive(:finished?).and_return true
        end

        it 'returns false' do
          expect(subject.ongoing?).to eq false
        end
      end
    end

    describe 'finished_contents' do
      let(:contents_double) { double('Content') }
      let(:evaluated_contents_double) { double('Content') }
      let(:finished_contents_double) { double('Content') }

      before do
        expect(subject).to receive(:contents).and_return contents_double
        expect(contents_double).to receive(:evaluated).and_return evaluated_contents_double
        expect(evaluated_contents_double).to receive(:finished).and_return finished_contents_double
      end

      it 'returns the evaluated contents with finished encoded contents' do
        subject.finished_contents
      end
    end

    describe 'contents_to_show' do
      let(:finished_contents_double) { double('Content') }
      let(:ordered_double) { double('Content') }

      before do
        expect(subject).to receive(:finished_contents).and_return finished_contents_double
        expect(finished_contents_double).to receive(:order).with('judge_evaluations.judge_rating DESC', created_at: :desc).and_return ordered_double
      end

      it 'returns the finished contents ordered by their judge ratings and creation date' do
        subject.contents_to_show
      end
    end
  end
end
