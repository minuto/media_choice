require 'unit_helper'

RSpec.describe HomeHighlight do
  describe 'validations' do
    %i(visible link).each do |field|
      it { is_expected.to validate_presence_of field }
    end
  end

  describe 'paperclip attachments' do
    it { is_expected.to have_attached_file(:banner) }
    it { is_expected.to validate_attachment_presence(:banner) }
    it {
      is_expected.to validate_attachment_content_type(:banner)
        .allowing('image/*')
        .rejecting('text/*', 'video/*')
    }
  end
end
