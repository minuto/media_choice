require 'unit_helper'
require 'validators/content_contest_validator'

describe ContentContestValidator do
  describe 'methods' do
    describe 'validate' do
      let(:user) { create(:user) }
      let(:profile) { build(:profile) }
      let(:contest) { build(:contest) }
      subject(:content) { build(:video, user_id: user.id, contests: [contest]) }

      before do
        expect(User).to receive(:find).with(user.id).and_return user
        expect(user).to receive(:profile).and_return profile
      end

      context 'with an ongoing contest' do
        context 'and a contest that is not currently ongoing' do
          let(:another_contest) { build(:contest) }
          subject(:content) { build(:video, user_id: user.id, contests: [contest, another_contest]) }

          before do
            expect(another_contest).to receive(:ongoing?).and_return false
          end

          it 'is expected to add errors' do
            subject.save
            expect(subject.errors[:contests]).to include I18n.t 'no_ongoing_contest_associated'
          end
        end

        before do
          expect(contest).to receive(:ongoing?).and_return true
          Content.skip_callback(:save, :after, :update_coordinates)
        end

        it 'is expected to not add errors' do
          subject.save
          expect(subject.errors).to be_empty
        end

        after do
          Content.set_callback(:save, :after, :update_coordinates)
        end
      end

      context 'without an ongoing contest' do
        before do
          expect(contest).to receive(:ongoing?).and_return false
        end

        it 'is expected to no add errors' do
          subject.save
          expect(subject.errors[:contests]).to include I18n.t 'no_ongoing_contest_associated'
        end
      end
    end
  end
end
