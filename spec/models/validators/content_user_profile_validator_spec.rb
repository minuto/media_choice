require 'unit_helper'
require 'validators/content_user_profile_validator'

describe ContestTimeValidator do
  describe 'methods' do
    describe 'validate' do
      let(:user) { create(:user) }
      subject(:content) { build(:video, user_id: user.id) }

      before do
        expect(User).to receive(:find).with(user.id).and_return user
      end

      context 'valid content' do
        let(:profile) { build(:profile) }

        before do
          expect(user).to receive(:profile).and_return profile
          Content.skip_callback(:save, :after, :update_coordinates)
        end

        it 'is expected to no add errors' do
          subject.save
          expect(subject.errors).to be_empty
        end

        after do
          Content.set_callback(:save, :after, :update_coordinates)
        end
      end

      context 'invalid content' do
        before do
          expect(user).to receive(:profile).and_return nil
        end

        it 'is expected to no add errors' do
          subject.save
          expect(subject.errors[:user]).to include I18n.t 'user_needs_profile'
        end
      end
    end
  end
end
