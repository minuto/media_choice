require 'unit_helper'
require 'validators/numeric_code_maps_to_name_validator'

describe NumericCodeMapsToNameValidator do
  describe 'methods' do
    describe 'validate' do
      subject(:profile) { build(:profile) }

      context 'valid content' do
        it 'is expected to no add errors' do
          country = double('carmen country')
          expect(Carmen::Country).to receive(:numeric_coded).at_least(2).times.with(subject.country_numeric_code).and_return country
          expect(country).to receive(:name).at_least(2).times.and_return instance_of(String)
          subject.save
          expect(subject.errors).to be_empty
        end
      end

      context 'invalid content' do
        context 'numeric code does not map to anything' do
          it 'is expected to no add errors' do
            expect(Carmen::Country).to receive(:numeric_coded).with(subject.country_numeric_code).and_return nil
            subject.save
            expect(subject.errors[:country]).to include I18n.t 'no_numeric_code_mapping'
          end
        end

        context 'numeric code does maps to a Carmen object that has no name' do
          it 'is expected to no add errors' do
            country = double('carmen country')
            expect(Carmen::Country).to receive(:numeric_coded).with(subject.country_numeric_code).and_return country
            expect(country).to receive(:name).and_return nil
            subject.save
            expect(subject.errors[:country]).to include I18n.t 'no_numeric_code_mapping'
          end
        end
      end
    end
  end
end
