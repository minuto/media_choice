require 'unit_helper'
require 'validators/contest_time_validator'

describe ContestTimeValidator do
  describe 'methods' do
    describe 'validate' do
      context 'valid contest' do
        subject(:contest) { build(:contest) }

        it 'is expected to no add errors' do
          subject.save
          expect(subject.errors).to be_empty
        end
      end

      context 'invalid contest' do
        subject(:contest) { build(:contest, start_date: Time.zone.tomorrow, end_date: Time.zone.yesterday) }

        it 'is expected to no add errors' do
          subject.save
          expect(subject.errors[:date]).to include I18n.t 'invalid_contest_date'
        end
      end
    end
  end
end
