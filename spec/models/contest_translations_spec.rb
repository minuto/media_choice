require 'unit_helper'

RSpec.describe ContestTranslation, type: :model do
  describe 'validations' do
    %i(locale name).each do |field|
      it { is_expected.to validate_presence_of field }
    end
    it { is_expected.to validate_inclusion_of(:locale).in_array(I18n.available_locales).with_message(I18n.t('contest_translation_locale_not_available')) }

    context 'contest_id' do
      context 'when found' do
        let(:contest) { build(:contest) }
        before do
          expect(Contest).to receive(:find_by_id).and_return contest
        end

        it { is_expected.to validate_presence_of :contest_id }
      end

      context 'when not found' do
        before do
          expect(Contest).to receive(:find_by_id).and_return nil
        end

        it { is_expected.not_to validate_presence_of :contest_id }
      end
    end
  end

  describe 'paperclip attachments' do
    it { is_expected.to have_attached_file(:logo) }
    it { is_expected.to validate_attachment_presence(:logo) }
    it {
      is_expected.to validate_attachment_content_type(:logo).allowing('image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png').rejecting(/video/)
    }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:contest) }
  end
end
