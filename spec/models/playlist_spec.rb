require 'unit_helper'

describe Playlist do
  describe 'relations' do
    it { is_expected.to have_and_belong_to_many(:contents) }
    it { is_expected.to have_many(:playlist_translations) }
  end

  describe 'methods' do
    subject { build(:playlist) }

    describe 'name' do
      let(:locale) { I18n.locale }
      let(:translation_missing_error_message) { I18n.t('missing_playlist_translation', field: I18n.t('activerecord.attributes.playlist_translation.name'), _locale: locale, id: subject.id) }

      context 'when the playlist has no translations' do
        it 'returns a translation missing message' do
          expect(subject.name).to eq(translation_missing_error_message)
        end
      end

      context 'when the playlist has translations' do
        let(:locale) { :en }
        let(:translations) { instance_double('ActiveRecord::Relation') }

        before do
          expect(subject).to receive(:playlist_translations).and_return translations
        end

        context 'and the playlist has no translation with the specified locale' do
          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return nil
          end

          it 'returns nil' do
            expect(subject.name(locale)).to eq translation_missing_error_message
          end
        end

        context 'when the playlist has a translation with the specified locale' do
          let(:translation) { build(:playlist_translation) }

          before do
            expect(translations).to receive(:find_by).with(locale: locale).and_return translation
          end

          it 'returns the name attribute from the translation' do
            expect(subject.name(locale)).to eq translation.name
          end
        end
      end
    end
  end
end
