require 'unit_helper'

describe MassMail do
  subject { build(:mass_mail) }

  describe 'validations' do
    %i(status locale subject body).each do |field|
      it { is_expected.to validate_presence_of field }
    end
  end

  describe 'state_machine' do
    context 'transitioning from pending to sending' do
      it 'is expected to call the create_jobs method' do
        expect(subject).to receive(:create_job)

        expect(subject.send_email).to be_truthy

        expect(subject.status).to eq('sending')
      end
    end
  end

  describe 'create_job' do
    let(:transition) { double('transition') }

    it 'is expected to create the job' do
      expect(MassMailJob).to receive(:perform_later).with(subject)

      subject.create_job(transition)
    end
  end
end
