require 'unit_helper'

RSpec.describe ViewCounter do
  describe 'validations' do
    it { is_expected.to validate_presence_of :viewed_model }
    it { is_expected.to validate_presence_of :model_id }
  end
end
