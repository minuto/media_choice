require 'unit_helper'

RSpec.describe JudgeEvaluation, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:content) }
  end

  describe 'validations' do
    let(:user) { build(:user, profile: build(:profile)) }
    let(:content) { build(:content_with_original_medias, user: user) }
    subject { build(:judge_evaluation, content: content) }

    it do
      expect_any_instance_of(ContentUserProfileValidator).to receive(:validate).at_least(:once)
      is_expected.to validate_uniqueness_of(:content)
    end

    it { is_expected.to validate_presence_of :content }
  end

  describe 'method' do
    describe 'bad_state?' do
      context 'when the content is not on a bad state' do
        before do
          expect(subject).to receive(:evaluation_state).and_return('pending', 'evaluating', 'evaluated')
        end

        it 'returns false' do
          3.times do
            expect(subject.bad_state?).to eq false
          end
        end
      end

      context 'when the content is on a bad state' do
        before do
          expect(subject).to receive(:evaluation_state).and_return('corrupted', 'piracy', 'repeated')
        end

        it 'returns true' do
          3.times do
            expect(subject.bad_state?).to eq true
          end
        end
      end
    end

    describe 'featured?' do
      context 'when rating is not present' do
        subject { build(:judge_evaluation) }

        it 'is expected to return false' do
          expect(subject.featured?).to eq(false)
        end
      end

      context 'when there is a judge rating' do
        context 'and the rating is less than 4' do
          subject { build(:judge_evaluation, judge_rating: 3) }

          it 'is expected to return false' do
            expect(subject.featured?).to eq(false)
          end
        end

        context 'and the rating is more than or equal to 4' do
          subject { build(:judge_evaluation, judge_rating: 4) }

          it 'is expected to return true' do
            expect(subject.featured?).to eq(true)
          end
        end
      end
    end

    describe 'evaluated?' do
      context 'with an evaluated contest' do
        before do
          expect(subject).to receive(:evaluation_state).and_return 'evaluated'

          it 'returns true' do
            expect(subject.evaluated?).to eq true
          end
        end
      end

      context 'with any other state of the contest' do
        before do
          expect(subject).to receive(:evaluation_state).and_return 'pending'
        end

        it 'returns false' do
          expect(subject.evaluated?).to eq false
        end
      end
    end
  end
end
