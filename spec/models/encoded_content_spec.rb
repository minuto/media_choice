require 'unit_helper'

describe EncodedContent do
  describe 'validations' do
    it { is_expected.to validate_presence_of :content_id }
    it { is_expected.to validate_presence_of :job_id }
  end

  describe 'paperclip attachments' do
    it { is_expected.to have_attached_file(:video) }
    it {
      is_expected.to validate_attachment_content_type(:video)
        .allowing('video/mp4', 'video/x-flv', 'video/MP2T', 'video/3gpp', 'video/quicktime', 'video/x-msvideo', 'video/x-ms-wmv')
        .rejecting('text/plain', 'text/xml', 'image/png', 'image/gif')
    }
  end

  describe 'relations' do
    it { is_expected.to belong_to(:content) }
    it { is_expected.to have_one(:thumbnail) }
  end
end
