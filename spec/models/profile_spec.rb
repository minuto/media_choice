require 'unit_helper'

describe Profile do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    subject { build(:profile) }

    describe 'fields that are always required' do
      %i(first_name last_name country_numeric_code state city zip_code gender occupation).each do |field|
        it { is_expected.to validate_presence_of field }
      end
      it { is_expected.to validate_acceptance_of :terms_of_service }
    end

    describe 'fields that are conditionally required' do
      before do
        expect(subject).to receive(:underaged?).and_return true
      end

      it { is_expected.to validate_presence_of :responsible_name }
    end

    describe 'fields with specific format' do
      it { is_expected.to allow_value('076').for(:country_numeric_code) }
      it { is_expected.to_not allow_value('1445').for(:country_numeric_code) }
      it { is_expected.to_not allow_value('BR').for(:country_numeric_code) }
      it { is_expected.to allow_value(1).for(:gender) }
      it { is_expected.to_not allow_value(12).for(:gender) }
      it { is_expected.to allow_value(1).for(:occupation) }
      it { is_expected.to allow_value(13).for(:occupation) }
      it { is_expected.to_not allow_value(0).for(:occupation) }
      it { is_expected.to_not allow_value(18).for(:occupation) }
      it { is_expected.to allow_value(1).for(:school_type) }
      it { is_expected.to allow_value(nil).for(:school_type) }
      it { is_expected.to_not allow_value(12).for(:school_type) }
      it { is_expected.to_not allow_value(0).for(:school_type) }
    end
  end

  describe 'callbacks' do
    describe 'before validating a profile' do
      context 'if no birthdate was provided' do
        let(:profile_with_no_birthdate) { build(:profile, birthdate: nil) }

        it 'is expected to enforce a profile with 12 years' do
          profile_with_no_birthdate.run_callbacks :validation
          enforced_birthdate = ((Time.zone.today - profile_with_no_birthdate.birthdate) / 365).to_i
          expect(enforced_birthdate).to eq(12)
        end
      end

      context 'if birthdate was provided' do
        let(:birthdate) { (Time.zone.now - 25.years).to_date }
        let(:profile_with_birthdate) { build(:profile, birthdate: birthdate) }

        it 'is expected to keep the provided birthdate' do
          profile_with_birthdate.run_callbacks :validation
          expect(profile_with_birthdate.birthdate).to eq(birthdate)
        end
      end
    end
  end

  describe 'instance methods' do
    describe 'age verification' do
      let(:adult_profile) { build(:profile, birthdate: Time.zone.now - 25.years) }
      let(:underaged_profile) { build(:profile, birthdate: Time.zone.now - 15.years) }

      describe 'adult?' do
        context 'with an adult profile' do
          let(:profile) { adult_profile }

          it 'returns true' do
            expect(profile.adult?).to eq true
          end
        end

        context 'with an underaged profile' do
          let(:profile) { underaged_profile }

          it 'returns false' do
            expect(profile.adult?).to eq false
          end
        end
      end

      describe 'underaged?' do
        context 'with an adult profile' do
          let(:profile) { adult_profile }

          it 'returns false' do
            expect(profile.underaged?).to eq false
          end
        end

        context 'with an underaged profile' do
          let(:profile) { underaged_profile }

          it 'returns true' do
            expect(profile.underaged?).to eq true
          end
        end
      end
    end

    describe 'full_name' do
      let(:profile) { build(:profile) }

      it 'returns the first_name and last_name separated by whitespace' do
        expect(profile.full_name).to eq("#{profile.first_name} #{profile.last_name}")
      end
    end
  end
end
