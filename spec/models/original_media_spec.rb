require 'unit_helper'

describe OriginalMedia do
  describe 'paperclip attachments' do
    it { is_expected.to have_attached_file(:media) }
    it { is_expected.to validate_attachment_presence(:media) }
  end
end
