require 'unit_helper'

RSpec.describe Thumbnail, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:encoded_content) }
  end

  describe 'paperclip attachments' do
    it { is_expected.to have_attached_file(:image) }
    it {
      is_expected.to validate_attachment_content_type(:image)
        .allowing('image/png', 'image/gif', 'image/jpg')
        .rejecting('text/plain', 'text/xml', 'video/mp4', 'video/x-flv')
    }
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:width).to :encoded_content }
    it { is_expected.to delegate_method(:height).to :encoded_content }
  end

  describe 'method' do
    describe 'url' do
      let(:image) { double('image') }
      let(:style) { :small }

      it "is expected to call the image's method" do
        expect(image).to receive(:url).with(style)
        expect(subject).to receive(:image).and_return(image)

        subject.url(style)
      end
    end
  end
end
