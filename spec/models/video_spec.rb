require 'unit_helper'

describe Video do
  describe 'paperclip attachments' do
    it {
      is_expected.to validate_attachment_content_type(:media)
        .allowing('video/mp4', 'video/x-flv', 'video/MP2T', 'video/3gpp', 'video/quicktime', 'video/x-msvideo', 'video/x-ms-wmv')
        .rejecting('text/plain', 'text/xml', 'image/png', 'image/gif')
    }
  end
end
