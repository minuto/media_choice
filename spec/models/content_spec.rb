require 'unit_helper'
require 'models/concerns/coordinate_searcher'
require 'models/concerns/support_for_view_counting'

describe Content do
  context 'Included modules' do
    subject { build(:video) }
    it_behaves_like 'Coordinate Searcher'
    it_behaves_like 'It Has Support For View Counting'
  end

  describe 'validations' do
    %i(title user_id soundtrack director).each do |field|
      it { is_expected.to validate_presence_of field }
    end
    it { is_expected.to validate_acceptance_of :terms_of_service }
  end

  describe 'relations' do
    it { is_expected.to have_many(:encoded_contents) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to have_many(:public_ratings) }
    it { is_expected.to have_and_belong_to_many(:contests) }
    it { is_expected.to have_and_belong_to_many(:playlists) }
    it { is_expected.to have_one(:coordinate) }
    it { is_expected.to have_one(:judge_evaluation) }
    it { is_expected.to have_many(:thumbnails).through(:encoded_contents) }
  end

  describe 'hooks' do
    context 'after create' do
      subject { build(:video, id: nil) }
      it 'creates an Upload to Zencoder Job' do
        expect(RequestZencoderJob).to receive(:perform_later).with(subject)
        subject.run_callbacks :create
      end
    end

    context 'after save' do
      subject { build(:video) }
      it 'updates the coordinates for the content' do
        expect(subject).to receive(:update_coordinates)
        subject.run_callbacks :save
      end
    end
  end

  describe 'delegations' do
    it { is_expected.to delegate_method(:evaluation_state).to :judge_evaluation }
    it { is_expected.to delegate_method(:label_evaluated).to :judge_evaluation }
    it { is_expected.to delegate_method(:label_corrupted).to :judge_evaluation }
    it { is_expected.to delegate_method(:label_piracy).to :judge_evaluation }
    it { is_expected.to delegate_method(:label_repeated).to :judge_evaluation }
    it { is_expected.to delegate_method(:rollback_bad_state).to :judge_evaluation }
    it { is_expected.to delegate_method(:bad_state?).to :judge_evaluation }
    it { is_expected.to delegate_method(:featured?).to :judge_evaluation }
    it { is_expected.to delegate_method(:evaluated?).to :judge_evaluation }
    it { is_expected.to delegate_method(:judge_rating).to :judge_evaluation }
  end

  describe 'nested attributes' do
    it { is_expected.to accept_nested_attributes_for(:original_medias) }
  end

  describe 'methods' do
    describe 'average_grade' do
      let(:content) { build(:video) }

      context 'without public ratings' do
        it 'returns nil' do
          expect(content.average_grade).to be_nil
        end
      end

      context 'with any nonzero positive number of public ratings' do
        let(:public_ratings) { double('public_ratings') }

        before do
          expect(content).to receive(:public_ratings).at_least(1).times.and_return public_ratings
        end

        it 'is expected to call the average method' do
          expect(public_ratings).to receive(:average).with(:grade)
          content.average_grade
        end
      end
    end

    describe 'finished_encoded_contents?' do
      subject { build :video }

      context 'with finished encoded contents' do
        let(:encoded_content) { build :encoded_video }

        before do
          expect(subject).to receive(:finished_encoded_contents).and_return [encoded_content]
        end

        it 'returns true' do
          expect(subject.finished_encoded_contents?).to eq true
        end
      end

      context 'without finished encoded contents' do
        before do
          expect(subject).to receive(:finished_encoded_contents).and_return []
        end

        it 'returns false' do
          expect(subject.finished_encoded_contents?).to eq false
        end
      end
    end

    describe 'send_to_evaluation' do
      let(:content) { build(:video) }

      it 'is expected to request an evaluation from the evaluation system' do
        expect(EvaluationSystem).to receive(:request_evaluation).with(content)
        expect(content.judge_evaluation).to receive(:send_to_evaluation)

        content.send_to_evaluation
      end
    end

    describe 'finished_encoded_contents' do
      let(:encoded_content_double) { double('EncodedContent') }

      before do
        expect(subject).to receive(:encoded_contents).and_return encoded_content_double
      end

      it 'returns the finished encoded contents' do
        expect(encoded_content_double).to receive(:where).with(state: :finished)
        subject.finished_encoded_contents
      end
    end

    describe 'description' do
      it 'returns nil' do
        expect(subject.description).to be_nil
      end
    end

    describe 'update_coordinates' do
      let(:user) { build(:user) }
      let(:profile) { build(:profile, user: user) }
      subject { build(:video, user: user) }

      context 'when the video already has a coordinate' do
        let(:coordinate) { build(:coordinate) }

        before do
          expect(subject).to receive(:coordinate).at_least(1).times.and_return coordinate
        end

        context 'address found' do
          let(:lat_lng) { { lat: -23.5328814, lng: -46.7920029 } }
          let(:parameters) { { latitude: lat_lng[:lat], longitude: lat_lng[:lng], found: true, content: subject } }

          context 'using zip_code' do
            before do
              expect(subject).to receive(:search_coordinates).with(subject.zip_code, :zip_code).and_return lat_lng
            end

            it 'updates the coordinate with the new found values' do
              expect(coordinate).to receive(:update).with(parameters)
              subject.update_coordinates
            end
          end

          context "using owner's city" do
            before do
              expect(user).to receive(:profile).at_least(1).times.and_return profile
              expect(subject).to receive(:zip_code).and_return nil
              expect(subject).to receive(:search_coordinates).with(subject.user.city, :city).and_return lat_lng
            end

            it 'updates the coordinate with the new found values' do
              expect(coordinate).to receive(:update).with(parameters)
              subject.update_coordinates
            end
          end
        end

        context 'address not found' do
          let(:parameters) { { latitude: nil, longitude: nil, found: false, content: subject } }

          before do
            expect(subject).to receive(:search_coordinates).and_return({})
          end

          it 'sets the new coordinate to not found' do
            expect(coordinate).to receive(:update).with(parameters)
            subject.update_coordinates
          end
        end
      end

      context 'when the video does not have a coordinate' do
        context 'address found' do
          let(:new_coordinate) { build(:coordinate, found: true) }
          let(:lat_lng) { { lat: -23.5328814, lng: -46.7920029 } }
          let(:parameters) { { latitude: lat_lng[:lat], longitude: lat_lng[:lng], found: true, content: subject } }

          before do
            expect(subject).to receive(:search_coordinates).and_return(lat_lng)
          end

          it 'creates a new found coordinate' do
            expect(Coordinate).to receive(:create).with(parameters).and_return(new_coordinate)
            subject.update_coordinates
          end
        end

        context 'address not found' do
          let(:new_coordinate) { build(:coordinate, found: false) }
          let(:parameters) { { latitude: nil, longitude: nil, found: false, content: subject } }

          before do
            expect(subject).to receive(:search_coordinates).and_return({})
          end

          it 'creates a new not found coordinate' do
            expect(Coordinate).to receive(:create).with(parameters).and_return new_coordinate
            subject.update_coordinates
          end
        end
      end
    end

    describe 'user_finished_contents' do
      let(:user) { build(:user) }
      let(:user_finished_content) { build(:video, user: user) }
      let(:finished_contents) { double('ActiveRecord::Relation') }

      before do
        expect(Content).to receive(:show_on_map).and_return finished_contents
        expect(finished_contents).to receive(:where).with(user: user).and_return [user_finished_content]
      end

      it "returns the user's videos that were successfully encoded" do
        expect(described_class.user_finished_contents(user)).to eq Set.new([user_finished_content])
      end
    end

    describe 'generic_contents_for_map' do
      let(:active_record_relation) { double('ActiveRecord::Relation') }

      it 'is expected to apply filters for a generic map list' do
        expect(Content).to receive(:show_on_map).and_return active_record_relation
        expect(active_record_relation).to receive(:featured).and_return active_record_relation
        expect(active_record_relation).to receive(:evaluated).and_return active_record_relation
        expect(active_record_relation).to receive(:where).with(adult: false)

        expect(described_class.generic_contents_for_map).to be_a(Set)
      end
    end

    describe 'contents_for_map_with_logged_in_user' do
      context 'when the user is an adult' do
        let(:user) { build(:user, :adult_user) }
        it 'is expected to apply filters for a logged in user map list' do
          expect(described_class).to receive(:user_finished_contents).with(user).and_return Set.new
          expect(described_class).to receive(:generic_contents_for_map).with(true).and_return Set.new

          described_class.contents_for_map_with_logged_in_user(user)
        end
      end

      context 'when the user is not an adult' do
        let(:user) { build(:user) }
        it 'is expected to apply filters for a logged in user map list' do
          expect(described_class).to receive(:user_finished_contents).with(user).and_return Set.new
          expect(described_class).to receive(:generic_contents_for_map).with(false).and_return Set.new

          described_class.contents_for_map_with_logged_in_user(user)
        end
      end
    end

    describe 'view_counter_attribute' do
      it 'is expected to return the model attribute that count views' do
        subject = build(:video)
        expect(subject.view_counter_attribute).to eq('view_count')
      end
    end

    describe 'related_contents' do
      let(:contest_ids) { [1, 2] }
      let(:contests) { double('contests') }
      let(:query_mock) { double('query_mocks') }

      before do
        Rails.cache.clear
        # Mocks cannot be serialized, which makes Rails.cache.fetch fail with singleton can't be dumped
        expect(Rails.cache).to receive(:write).and_return query_mock
        expect(subject).to receive(:contests).and_return contests
        expect(contests).to receive(:pluck).with(:id).and_return contest_ids

        expect(Content).to receive(:joins).with(:contents_contests).and_return query_mock
        expect(query_mock).to receive(:where).with('contents_contests.contest_id' => contest_ids).and_return query_mock
        expect(query_mock).to receive(:where).with(no_args).and_return query_mock
        expect(query_mock).to receive(:not).with(id: subject.id).and_return query_mock
        expect(query_mock).to receive(:distinct).and_return query_mock
        expect(query_mock).to receive(:featured).and_return query_mock
        expect(query_mock).to receive(:shuffle).and_return query_mock
      end

      after do
        Rails.cache.clear
      end

      context "when the list is filled (has #{Content::RELATED_CONTENTS_LIST_SIZE} elements)" do
        it 'is expected to build a list with those related contents' do
          expect(query_mock).to receive(:size).and_return Content::RELATED_CONTENTS_LIST_SIZE
          expect(subject).to_not receive(:fill_with_additional_featured_contents)

          expect(subject.related_contents).to eq(query_mock)
        end
      end

      context "when the list is not filled (has less than #{Content::RELATED_CONTENTS_LIST_SIZE} elements)" do
        let(:list_size) { 20 }

        it 'is expected to complete the list with distinct featured contents' do
          expect(query_mock).to receive(:size).and_return list_size
          expect(subject).to receive(:fill_with_additional_featured_contents).with(query_mock).and_return query_mock

          expect(subject.related_contents).to eq(query_mock)
        end
      end
    end

    describe 'fill_with_additional_featured_contents' do
      let(:list_size) { 20 }
      let(:content_ids) { (1..20).to_a }
      let(:list) { double('active record relation') }
      let(:additional_contents) { double('additional related contents') }

      it 'is expected to fill the list up to 30 elements' do
        expect(list).to receive(:pluck).with(:id).and_return content_ids
        expect(Content).to receive(:where).with(no_args).and_return additional_contents
        expect(additional_contents).to receive(:not).with(id: content_ids).and_return additional_contents
        expect(additional_contents).to receive(:featured).and_return additional_contents
        expect(list).to receive(:size).and_return list_size
        expect(additional_contents).to receive(:sample).with(Content::RELATED_CONTENTS_LIST_SIZE - list_size).and_return additional_contents
        expect(list).to receive(:+).with(additional_contents).and_return additional_contents

        subject.fill_with_additional_featured_contents(list)
      end
    end
  end
end
