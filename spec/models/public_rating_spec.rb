require 'unit_helper'

RSpec.describe PublicRating do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'validations' do
    %i(user_id content_id grade).each do |field|
      it { is_expected.to validate_presence_of field }
    end
    it { is_expected.to validate_numericality_of(:grade).is_greater_than_or_equal_to 0 }
    it { is_expected.to validate_numericality_of(:grade).is_less_than_or_equal_to 5 }

    context 'scoped uniqueness' do
      subject { build(:public_rating, user_id: 1, content_id: 1) } # This example needs a subject with valid attributes from the other validations
      it { is_expected.to validate_uniqueness_of(:user_id).scoped_to(:content_id).with_message(I18n.t('can_only_vote_once')) }
    end
  end
end
