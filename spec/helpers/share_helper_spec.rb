require 'unit_helper'

describe ShareHelper do
  describe 'facebook_share' do
    let(:url) { 'http://www.example.com' }
    let(:title) { 'Title' }

    it 'is expected to create a link for sharing contents on Facebook' do
      link = helper.facebook_share(url, title)
      expect(link).to match(/facebook/)
      expect(link).to match(/u=#{url}/)
      expect(link).to match(/t=#{title}/)
      expect(link).to match(/fa-facebook/)
    end
  end

  describe 'twitter_share' do
    let(:url) { 'http://www.example.com' }
    let(:title) { 'Title' }

    it 'is expected to create a link for sharing contents on Facebook' do
      link = helper.twitter_share(url, title)
      expect(link).to match(/twitter/)
      expect(link).to match(/url=#{url}/)
      expect(link).to match(/text=#{title}/)
      expect(link).to match(/fa-twitter/)
    end
  end

  describe 'google_plus_share' do
    let(:url) { 'http://www.example.com' }

    it 'is expected to create a link for sharing contents on Facebook' do
      link = helper.google_plus_share(url)
      expect(link).to match(/plus\.google/)
      expect(link).to match(/url=#{url}/)
      expect(link).to match(/fa-google-plus/)
    end
  end
end
