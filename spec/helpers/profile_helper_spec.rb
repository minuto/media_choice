require 'unit_helper'

describe ProfileHelper do
  describe 'country_select' do
    let(:countries) { Carmen::Country.all }
    let(:result) { helper.country_select(profile) }

    context 'without a selected country' do
      let!(:profile) { build(:profile, country_numeric_code: nil) }
      it 'creates options for a select tag with the countries' do
        countries.each do |country|
          expect(result).to include CGI.escapeHTML country.name
        end
      end

      it 'sets Brazil as the default selected country' do
        expect(result).to match(/<option\s+selected="selected"\s+value="#{Carmen::Country.coded('br').numeric_code}"\s*>/)
      end
    end

    context 'with a selected country' do
      let!(:profile) { build(:profile, country_numeric_code: '840') } # US numeric code
      it 'creates options for a select tag with the countries' do
        countries.each do |country|
          expect(result).to include CGI.escapeHTML country.name
        end
      end

      it 'sets United States as the default selected country' do
        expect(result).to match(/<option\s+selected="selected"\s+value="#{Carmen::Country.coded('us').numeric_code}"\s*>/)
      end
    end
  end

  describe 'state_select' do
    let!(:states) { Carmen::Country.coded('br').subregions }
    let(:stored_state) { 'São Paulo' }
    let(:result) { helper.state_select(states, stored_state) }

    it 'creates options for a select tag with the states' do
      states.each do |state|
        expect(result).to include CGI.escapeHTML state.name
      end
    end

    it 'sets Brazil as the default selected country' do
      expect(result).to match(/<option\s+selected="selected"\s+value="#{stored_state}"\s*>/)
    end
  end

  describe 'occupation_select' do
    let!(:occupations) { I18n.t('occupations').values }
    selected_occupation = (1..15).to_a.sample
    let(:result) { helper.occupation_select(selected_occupation) }

    it 'creates options for a select tag with the occupations' do
      occupations.each do |occupation|
        expect(result).to include CGI.escapeHTML occupation
      end
    end

    it "sets #{I18n.t('occupations')[selected_occupation]} as the default selected occupation" do
      expect(result).to match(/<option\s+selected="selected"\s+value="#{selected_occupation}"\s*>/)
    end
  end

  describe 'school_type_select' do
    let!(:school_types) { I18n.t('school_types').values }
    selected_school_type = (1..5).to_a.sample
    let(:result) { helper.school_type_select(selected_school_type) }

    it 'creates options for a select tag with the school types' do
      school_types.each do |school_type|
        expect(result).to include CGI.escapeHTML school_type
      end
    end

    it "sets #{I18n.t('school_types')[selected_school_type]} as the default selected occupation" do
      expect(result).to match(/<option\s+selected="selected"\s+value="#{selected_school_type}"\s*>/)
    end
  end

  describe 'user_owns_profile?' do
    let(:profile) { build(:profile) }

    context 'with a user logged in' do
      let(:user) { build(:user) }
      let(:another_profile) { build(:profile) }

      before do
        expect(helper).to receive(:current_user).and_return user
      end

      context 'when the user owns the profile' do
        before do
          expect(user).to receive(:profile).and_return profile
        end

        it 'returns true' do
          expect(helper.user_owns_profile?(profile)).to eq true
        end
      end

      context 'when the user doesn\'t own the profile' do
        before do
          expect(user).to receive(:profile).and_return another_profile
        end

        it 'returns false' do
          expect(helper.user_owns_profile?(profile)).to eq false
        end
      end
    end

    context 'with no user logged in' do
      before do
        expect(helper).to receive(:current_user).and_return nil
      end

      it 'returns false' do
        expect(helper.user_owns_profile?(profile)).to eq false
      end
    end
  end

  describe 'display_location_information' do
    let(:profile) { build(:profile) }

    before do
      expect(helper).to receive(:country_name).with(profile.country_numeric_code).and_return profile.country_numeric_code
    end

    context 'when there is location information missing' do
      it 'it is expected to display a not informed message' do
        profile.city = nil
        profile.state = nil
        response = helper.display_location_information(profile)
        expect(response).to match(/#{I18n.t('city')} #{I18n.t('not_informed')}/)
        expect(response).to match(/#{I18n.t('state')} #{I18n.t('not_informed')}/)
        expect(response).to match(/#{profile.country_numeric_code}/)
      end
    end

    context 'when all location information are there' do
      it 'it is expected to display the information' do
        expect(helper.display_location_information(profile)).to match(/#{profile.city}.*#{profile.state}.*#{profile.country_numeric_code}/)
      end
    end
  end

  describe 'country_name' do
    let(:profile) { build(:profile, country_numeric_code: '076') } # Brazil's numeric code
    let(:default_carmen_locale) { Carmen.i18n_backend.locale }

    context 'in english' do
      before do
        Carmen.i18n_backend.locale = :en
      end

      it 'is expected to return the country name correctly translated' do
        expect(helper.country_name(profile.country_numeric_code)).to eq('Brazil')
      end

      after do
        Carmen.i18n_backend.locale = default_carmen_locale
      end
    end

    context 'in portuguese' do
      before do
        Carmen.i18n_backend.locale = :pt
      end

      it 'is expected to return the country name correctly translated' do
        expect(helper.country_name(profile.country_numeric_code)).to eq('Brasil')
      end

      after do
        Carmen.i18n_backend.locale = default_carmen_locale
      end
    end
  end

  describe 'gender_radios' do
    let(:form) { double('mock') }

    before do
      @expected = ''

      I18n.t('genders').each do |id, gender|
        expect(form).to receive(:radio_button).with(:gender, id).and_return 'radio_button'
        @expected << 'radio_button'
        expect(form).to receive(:label).with(gender).and_return 'label'
        @expected << 'label'
      end
    end

    it 'is expected to iterate over the locales and generate radio buttons' do
      expect(helper.gender_radios(form)).to eq(@expected)
    end
  end

  describe 'student_radios' do
    context 'when the user is a student' do
      let(:profile) { build(:profile) }

      it 'is expected generate radio buttons with the affirmative checked' do
        expect(helper.student_radios(profile)).to match(/<input\stype="radio".+value="#{I18n.t('affirmative')}"\s+checked="checked"/)
        expect(helper.student_radios(profile)).to_not match(/<input\stype="radio".+value="#{I18n.t('negative')}"\s+checked="checked"/)
      end
    end

    context 'when the user is not a student' do
      let(:profile) { build(:profile, school_type: nil) }

      it 'is expected generate radio buttons with the negative checked' do
        expect(helper.student_radios(profile)).to match(/<input\stype="radio".+value="#{I18n.t('negative')}"\s+checked="checked"/)
        expect(helper.student_radios(profile)).to_not match(/<input\stype="radio".+value="#{I18n.t('affirmative')}"\s+checked="checked"/)
      end
    end
  end
end
