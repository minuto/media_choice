require 'unit_helper'

describe ApplicationHelper do
  describe 'i18n_logo' do
    context 'en locale' do
      let(:i18n_logo_name) { 'logo-en.png' }

      it 'is expected to return the english logo' do
        expect(I18n).to receive(:locale).twice.and_return(:en)

        expect(helper.i18n_logo).to eq(i18n_logo_name)
      end
    end

    context 'pt-BR locale' do
      let(:i18n_logo_name) { 'logo-pt-BR.png' }

      it 'is expected to return the pt-BR logo' do
        expect(I18n).to receive(:locale).twice.and_return(:'pt-BR')

        expect(helper.i18n_logo).to eq(i18n_logo_name)
      end
    end
  end

  describe 'i18n_top_banner' do
    context 'en locale' do
      let(:i18n_top_banner_name) { 'top-banner-en.png' }

      it 'is expected to return the english top banner' do
        expect(I18n).to receive(:locale).twice.and_return(:en)

        expect(helper.i18n_top_banner).to eq(i18n_top_banner_name)
      end
    end

    context 'pt-BR locale' do
      let(:i18n_top_banner_name) { 'top-banner-pt-BR.png' }

      it 'is expected to return the pt-BR top banner' do
        expect(I18n).to receive(:locale).twice.and_return(:'pt-BR')

        expect(helper.i18n_top_banner).to eq(i18n_top_banner_name)
      end
    end
  end

  describe 'flash_to_callout' do
    context 'with key notice' do
      let(:key) { 'notice' }

      it 'is expected to convert to the success key' do
        expect(helper.flash_to_callout(key)).to eq('success')
      end
    end

    context 'with any other key' do
      ['success', 'alert', 'warning'].each do |key|
        it 'is expected to not modify the flash message type' do
          expect(helper.flash_to_callout(key)).to eq(key)
        end
      end
    end
  end

  describe 'sponsor_banner' do
    let(:asset_name) { sponsor_banner_name + '.png' }

    before do
      # Mocking the method that loads all assets, used inside 'image_tag'. Speeds up the unit test.
      expect(helper).to receive(:path_to_image).with(asset_name).and_return "/assets/images/#{asset_name}"
    end

    context 'en locale' do
      let(:sponsor_banner_name) { 'sponsor-banner-en' }

      it 'is expected to return the english sponsor banner' do
        expect(I18n).to receive(:locale).and_return(:en)

        expect(helper.sponsor_banner).to match(/<img.*src=".*#{sponsor_banner_name}/)
      end
    end

    context 'pt-BR locale' do
      let(:sponsor_banner_name) { 'sponsor-banner-pt-BR' }

      it 'is expected to return the pt-BR sponsor banner' do
        expect(I18n).to receive(:locale).and_return(:'pt-BR')

        expect(helper.sponsor_banner).to match(/<img.*src=".*#{sponsor_banner_name}/)
      end
    end
  end
end
