require 'unit_helper'

describe ContentsHelper do
  describe 'contest_list_for' do
    let(:content) { build(:video) }

    context 'when there is no contest' do
      before do
        expect(content).to receive(:contests).and_return []
      end

      it 'is expected to return a message saying the content is not on any contest' do
        expect(helper.contest_list_for(content)).to eq(I18n.t('contents.content_details.none'))
      end
    end

    context 'when there is contests' do
      let(:contest) { build(:contest) }
      let(:contest_translation) { build(:contest_translation, name: 'Contest') }
      let(:another_contest) { build(:contest) }
      let(:another_contest_translation) { build(:contest_translation, name: ' Another Contest') }

      before do
        expect(content).to receive(:contests).and_return [contest, another_contest]
        expect(contest).to receive(:name).and_return contest_translation.name
        expect(another_contest).to receive(:name).and_return another_contest_translation.name
      end

      it 'is expected to return a list of links separated by comas' do
        expect(helper.contest_list_for(content)).to match(/#{contest_translation.name}.+#{another_contest_translation.name}/)
      end
    end
  end
end
