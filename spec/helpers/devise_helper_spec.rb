require 'unit_helper'

describe DeviseHelper do
  before do
    # The original DeviseHelper would generate a method missing error, which would be caught by the failure_app and
    # search for it in the warden environment.
    # In this test, the only method that would require this is resource, which returns a user
    module DeviseHelper
      def resource; end
    end
  end

  describe 'devise_error_messages!' do
    let!(:resource) { double('User') }

    context 'when the resource has no errors' do
      before do
        expect(helper).to receive(:devise_error_messages?).and_return false
      end

      it 'returns an empty string' do
        expect(helper.devise_error_messages!).to eq ''
      end
    end

    context 'when the resource has errors' do
      let(:resource_errors) { ActiveModel::Errors.new(build(:user)) }

      before do
        expect(helper).to receive(:devise_error_messages?).and_return true
        expect(resource).to receive(:class).and_return User
        expect(resource).to receive(:errors).at_least(:once).and_return resource_errors
        expect(helper).to receive(:resource).at_least(2).times.and_return resource
      end

      it 'returns the html content for the error message' do
        expect(helper.devise_error_messages!).to match(/<div.*#{I18n.t('errors.messages.not_saved', resource: User.model_name.human.downcase, count: 0)}/)
      end
    end
  end

  describe 'devise_error_messages?' do
    let!(:resource) { double('User') }
    let(:errors) { double('Array') }

    before do
      expect(helper).to receive(:resource).and_return resource
      expect(resource).to receive(:errors).and_return errors
    end

    context 'when the resource has no errors' do
      before do
        expect(errors).to receive(:empty?).and_return true
      end

      it 'returns false' do
        expect(helper.devise_error_messages?).to eq false
      end
    end

    context 'when the resource has errors' do
      before do
        expect(errors).to receive(:empty?).and_return false
      end

      it 'returns true' do
        expect(helper.devise_error_messages?).to eq true
      end
    end
  end
end
