require 'unit_helper'

describe MassMailJob do
  let(:mass_mail) { build(:mass_mail, status: 'sending') }
  let(:mail_sender) { double('mail_sender') }

  describe 'perform' do
    before do
      expect(MailSender).to receive(:new).with(mass_mail).and_return(mail_sender)
    end

    context 'successfully' do
      it 'is expected to call the MailSender and  and transition using successful_send' do
        expect(mail_sender).to receive(:send)
        expect(mass_mail).to receive(:successful_send)

        described_class.perform_now(mass_mail)
      end
    end

    context 'with send error' do
      it 'is expected to transition to the error state' do
        expect(mail_sender).to receive(:send).and_raise StandardError.new
        expect(mass_mail).to receive(:error_sending)

        expect { described_class.perform_now(mass_mail) }.to raise_error StandardError
      end
    end

    context 'retrying' do
      let(:mass_mail) { build(:mass_mail, status: 'sent_with_error') }

      it 'is expected to transition back to sending and send the message' do
        expect(mail_sender).to receive(:send)
        expect(mass_mail).to receive(:retry)
        expect(mass_mail).to receive(:successful_send)

        described_class.perform_now(mass_mail)
      end
    end
  end
end
