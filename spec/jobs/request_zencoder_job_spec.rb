require 'unit_helper'

describe RequestZencoderJob do
  describe 'perform' do
    let(:video) { build(:video) }
    let(:original_medias) { [build(:original_media)] }
    let(:response_body) { { 'id' => 12 } }
    let(:response) { instance_double('response') }

    it 'creates a zencoder job with the video' do
      expect(video).to receive(:original_medias).and_return original_medias
      expect(Zencoder::Job).to receive(:create).and_return response
      expect(response).to receive(:body).and_return response_body
      expect(EncodedVideo).to receive(:create).with(job_id: response_body['id'], content_id: video.id)
      subject.perform video
    end
  end
end
